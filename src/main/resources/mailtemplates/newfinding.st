newfinding(findingNo, findingDescription, findingPriority, findingStatus) ::= <<

Dear Process Manager,

Due to findings of prior audit reports, we need your updated action plan for following audit finding. Please enter the system your updated “management action plan”, “completion date” and “action owner” to the system within one week. If you don’t enter your updated action plans to the system within one week, you will regularly take reminder e-mails. This status follow up e-mail will be quarterly sent you until the finding status is marked as “Completed” by yourself. If you have any questions, please contact to Çimsa Internal Audit Department or Internal Systems and Actuary Group Management according to the audit type and report.

Finding No: $findingNo$
Finding Title: $findingDescription$
Findings Priority: $findingPriority$
Finding Status: $findingStatus$

Best regards,

>>