package com.tr.cimsa.denetimtaleptakip.services;

import java.util.List;

import com.tr.cimsa.denetimtaleptakip.model.Role;
import com.tr.cimsa.denetimtaleptakip.model.User;

/**
 * User: mhazer
 * Date: 8/19/12
 * Time: 5:04 PM
 */
public interface UserService extends BaseService
{
    public User findUser(String username);

    public List<User> retrieveActiveUsers();

    public User mergeUser(User user);

    public List<User> retrieveAllUsers();

    public List<Role> retrieveAllRoles();
}
