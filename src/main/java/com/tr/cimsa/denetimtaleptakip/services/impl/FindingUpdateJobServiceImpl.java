package com.tr.cimsa.denetimtaleptakip.services.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.tr.cimsa.denetimtaleptakip.controller.admin.CommanFunctionsService;
import com.tr.cimsa.denetimtaleptakip.model.Finding;
import com.tr.cimsa.denetimtaleptakip.services.FindingService;
import com.tr.cimsa.denetimtaleptakip.services.FindingUpdateJobService;

@Service("findingUpdateJobService")
public class FindingUpdateJobServiceImpl implements FindingUpdateJobService {
	
	private final static Logger LOGGER = LoggerFactory.getLogger(FindingUpdateJobServiceImpl.class);

	private FindingService findingService;

	@Resource(name = "findingService")
	public void setFindingService(FindingService findingService) {
		this.findingService = findingService;
	}

	@Async
	@Override
	public void updateFindingStatus() {

		CommanFunctionsService commanFunctionsService = new CommanFunctionsService();

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		LOGGER.debug(sdf.format(new Date()) + " :  updateFindingStatus  start  ");

		List<Finding> findings = findingService.retrieveNotCompletedFindings();

		for (Finding f : findings) {
			LOGGER.debug("*******************************************************************************************");
			LOGGER.debug(f.getFindingNumber() + " status: " + f.getStatus() + " comp date: " + f.getCompletionDate() + " rev comp " + f.getRevisedCompletionDate());
			f = commanFunctionsService.updateStatus(f);

			f = commanFunctionsService.updatePeriod(f);

			findingService.mergeFinding(f);
			
			LOGGER.debug(f.getFindingNumber() + " status: " + f.getStatus() + " comp date: " + f.getCompletionDate() + " rev comp " + f.getRevisedCompletionDate());
			LOGGER.debug("*******************************************************************************************");

		}

		LOGGER.debug(sdf.format(new Date()) + " :  updateFindingStatus end ");

	}

}
