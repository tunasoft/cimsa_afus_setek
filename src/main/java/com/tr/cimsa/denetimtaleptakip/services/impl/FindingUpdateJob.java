package com.tr.cimsa.denetimtaleptakip.services.impl;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.tr.cimsa.denetimtaleptakip.services.FindingUpdateJobService;

public class FindingUpdateJob  extends QuartzJobBean {
	
	private FindingUpdateJobService findingUpdateJobService;

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		
		findingUpdateJobService.updateFindingStatus();
	}

	public void setFindingUpdateJobService(FindingUpdateJobService findingUpdateJobService) {
		this.findingUpdateJobService = findingUpdateJobService;
	}
	
	

}
