package com.tr.cimsa.denetimtaleptakip.services.util;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * spring managed bean'lere ulaşmak için kullanılır
 *
 * @author mhazer
 */
@Component("springApplicationContextUtil")
public class SpringApplicationContextUtil implements ApplicationContextAware
{

    private static ApplicationContext APPLICATION_CONTEXT;

    private List<String> devWorkModes;

    private String workMode;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException
    {
        APPLICATION_CONTEXT = applicationContext;
    }

    public static Object getBean(String beanName)
    {
        Object bean = APPLICATION_CONTEXT.getBean(beanName);
        return bean;
    }

    @SuppressWarnings("unchecked")
    public static String getBeanNameFromType(Class clazz)
    {
        String[] names = APPLICATION_CONTEXT.getBeanNamesForType(clazz);
        if (names != null && names.length > 0)
        {
            return names[0];
        }
        else
        {
            return StringUtils.EMPTY;
        }
    }
}
