package com.tr.cimsa.denetimtaleptakip.services.impl;

import com.tr.cimsa.denetimtaleptakip.dao.GenericDAO;
import com.tr.cimsa.denetimtaleptakip.dao.UserDAO;
import com.tr.cimsa.denetimtaleptakip.model.Role;
import com.tr.cimsa.denetimtaleptakip.model.User;
import com.tr.cimsa.denetimtaleptakip.services.UserService;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * User: mhazer
 * Date: 8/19/12
 * Time: 5:04 PM
 */
@Service("userService")
public class UserServiceImpl extends BaseServiceImpl implements UserService
{
    private UserDAO userDAO;

    @Override
    public User findUser(String username)
    {
        List<User> users = this.userDAO.findByUsername(username);//findAllByProperty(User.class, "username", username);
        if(CollectionUtils.isNotEmpty(users))
        {
            User user = users.get(0);
            user.getResponsibleBusinessSubTypes().size();      
            user.getResponsibleBusinessTypes().size();    
            return user;
        }
        else
        {
            return null;
        }
    }

    @Override
    public List<User> retrieveActiveUsers()
    {
        return this.userDAO.retrieveActiveUsers();
    }

    @Override
    @Transactional
    public User mergeUser(User user)
    {
        return this.userDAO.merge(user);
    }

    @Override
    public List<User> retrieveAllUsers()
    {
        return this.userDAO.retrieveAllUsers();
    }

    @Override
    public List<Role> retrieveAllRoles()
    {
        return this.userDAO.findAll(Role.class);
    }

    @Override
    protected GenericDAO getDAO()
    {
        return userDAO;
    }

    @Resource(name = "userDAO")
    public void setUserDAO(UserDAO userDAO)
    {
        this.userDAO = userDAO;
    }
}
