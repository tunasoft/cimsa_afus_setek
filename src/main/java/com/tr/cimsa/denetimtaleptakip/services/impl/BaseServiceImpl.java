package com.tr.cimsa.denetimtaleptakip.services.impl;

import com.tr.cimsa.denetimtaleptakip.dao.GenericDAO;

public abstract class BaseServiceImpl
{
    protected abstract GenericDAO getDAO();

    public <T> void reattachUsingLock(T detachedEntity)
    {
        getDAO().reattachUsingLock(detachedEntity);
    }

    public <T> void reattachReadOnly(T detachedEntity)
    {
        getDAO().reattachUsingLock(detachedEntity);
        getDAO().getSession().setReadOnly(detachedEntity, true);
    }
}
