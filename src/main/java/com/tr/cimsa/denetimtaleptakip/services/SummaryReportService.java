package com.tr.cimsa.denetimtaleptakip.services;

import java.math.BigDecimal;
import java.util.List;

import org.javatuples.Quartet;
import org.javatuples.Quintet;
import org.javatuples.Triplet;

import com.tr.cimsa.denetimtaleptakip.model.AuditType;
import com.tr.cimsa.denetimtaleptakip.model.Finding;
import com.tr.cimsa.denetimtaleptakip.model.User;

/**
 * User: mhazer
 * Date: 8/19/12
 * Time: 5:04 PM
 */
public interface SummaryReportService extends BaseService
{
    public Quintet<List<Finding>, List<Finding>, List<Finding>, List<Finding>, List<Finding>> getFindingForSummaryReport(AuditType auditType, User user);
    public List<Finding> findSelectedFindings(AuditType auditType, User user, String priority, String status);
    //public String findKpiRateFromTable(BigDecimal higherOverdueKPIRate, BigDecimal lowerOverdueKPIRate);
}