package com.tr.cimsa.denetimtaleptakip.services;

import java.util.List;

import org.springframework.scheduling.annotation.Async;
import org.springframework.transaction.annotation.Transactional;

import com.tr.cimsa.denetimtaleptakip.model.Finding;
import com.tr.cimsa.denetimtaleptakip.model.FindingAction;

/**
 * User: mhazer
 * Date: 8/22/12
 * Time: 5:01 PM
 */
public interface MailService
{
    public void sendNotificationEmails();

    void sendEmailToFindingActionOwnersAndResponsibleHeads(Finding finding, String templateName);

    public void sendInformingEmails();
    
    @Async
    @Transactional(readOnly = true)
    void sendFindingActionUpdateEmail(FindingAction findingAction);
    
    @Async
    @Transactional(readOnly = true)
    public void sendUpdateEmailForEditetFinding(Finding finding);

    @Async
    @Transactional(readOnly = true)
    public void sendOpeningEmailToFindingActionOwnersAndResponsibleHeads(Finding finding, String templateName);
    
    @Async
    @Transactional(readOnly = true)
    public void executeEnerjisaEmailStrategy(Finding notCompletedFinding);
    
    @Async
    @Transactional(readOnly = true)
    public void executeEnerjisaEmailStrategyForStartMailService(Finding notCompletedFinding);
    
    @Async
    @Transactional(readOnly = true)
    public void executeEnerjisaEmailStrategyForInformationMail(List<Finding> openFinding);
}
