package com.tr.cimsa.denetimtaleptakip.services;

import org.apache.poi.ss.usermodel.Workbook;

import com.tr.cimsa.denetimtaleptakip.model.Finding;
import com.tr.cimsa.denetimtaleptakip.model.User;

import java.util.List;

/**
 * User: mhazer
 * Date: 8/19/12
 * Time: 5:04 PM
 */
public interface FindingExcelService extends BaseService
{
    public Workbook prepareExcelReportOfTheFindings(List<Finding> findings, User user);

    public Workbook prepareExcelTemplate();
    
    public Workbook prepareExcelReportOfTheDepartmentHeads(List<Finding> findings);


}
