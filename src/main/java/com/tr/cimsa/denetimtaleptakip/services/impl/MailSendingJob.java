package com.tr.cimsa.denetimtaleptakip.services.impl;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.tr.cimsa.denetimtaleptakip.services.MailService;

/**
 * User: mhazer
 * Date: 11/24/12
 * Time: 10:15 PM
 */
public class MailSendingJob extends QuartzJobBean
{
    private MailService mailService;

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException
    {
        mailService.sendNotificationEmails();
    }

    public void setMailService(MailService mailService)
    {
        this.mailService = mailService;
    }
}
