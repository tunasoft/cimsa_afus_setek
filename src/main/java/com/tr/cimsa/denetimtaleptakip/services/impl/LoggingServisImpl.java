package com.tr.cimsa.denetimtaleptakip.services.impl;

import com.tr.cimsa.denetimtaleptakip.dao.FindingDAO;
import com.tr.cimsa.denetimtaleptakip.dao.GenericDAO;
import com.tr.cimsa.denetimtaleptakip.services.LoggingServis;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * User: mhazer
 * Date: 7/29/13
 * Time: 3:59 PM
 */
@Service("loggingServis")
public class LoggingServisImpl extends BaseServiceImpl implements LoggingServis
{
    private FindingDAO findingDAO;

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public <T> T mergeLog(T detachedEntity)
    {
        return this.findingDAO.merge(detachedEntity);
    }

    @Override
    protected GenericDAO getDAO()
    {
        return findingDAO;
    }

    @Resource(name = "findingDAO")
    public void setFindingDAO(FindingDAO findingDAO)
    {
        this.findingDAO = findingDAO;
    }
}
