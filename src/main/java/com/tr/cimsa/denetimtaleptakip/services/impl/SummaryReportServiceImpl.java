package com.tr.cimsa.denetimtaleptakip.services.impl;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import com.google.common.collect.Lists;

import org.apache.commons.collections.CollectionUtils;
import org.javatuples.Quartet;
import org.javatuples.Quintet;
import org.javatuples.Triplet;
import org.springframework.stereotype.Service;

import com.tr.cimsa.denetimtaleptakip.dao.GenericDAO;
import com.tr.cimsa.denetimtaleptakip.dao.SummaryReportDAO;
import com.tr.cimsa.denetimtaleptakip.model.*;
import com.tr.cimsa.denetimtaleptakip.services.SummaryReportService;


@Service("summaryReportService")
public class SummaryReportServiceImpl extends BaseServiceImpl implements SummaryReportService
{
    private SummaryReportDAO summaryReportDAO;

    public Quintet<List<Finding>, List<Finding>, List<Finding>,List<Finding>,List<Finding>> getFindingForSummaryReport(AuditType auditType, User user)
    {
        List<Finding> notStartedFinding = Lists.newArrayList(), inProgressFinding = Lists.newArrayList(), overDueFinding = Lists.newArrayList(), confirmedCompletedFinding = Lists.newArrayList(), unconfirmedCompletedFinding = Lists.newArrayList();
        List<Finding> summaryResults =  this.summaryReportDAO.getFindingForSummaryReport(auditType, user);
        this.fillStatusListsOfFinding(summaryResults, notStartedFinding, inProgressFinding, overDueFinding, confirmedCompletedFinding, unconfirmedCompletedFinding);
        return Quintet.with(notStartedFinding, inProgressFinding, overDueFinding, confirmedCompletedFinding,unconfirmedCompletedFinding);
    }


    @Override
    public List<Finding> findSelectedFindings(AuditType auditType, User user, String priority, String status)
    {
        return this.summaryReportDAO.getFindingForSummaryReport(auditType, user, priority, status);
    }

    private void fillStatusListsOfFinding(List<Finding> summaryResults, List<Finding> notStartedFinding, List<Finding> inProgressFinding, List<Finding> overDueFinding, List<Finding> confirmedCompletedFinding, List<Finding> unconfirmedCompletedFinding)
    {
        for (Finding finding : summaryResults)
        {
        	if (Status.NOT_STARTED.equals(finding.getStatus()))
            {
                notStartedFinding.add(finding);
            }
        	else if (Status.IN_PROGRESS.equals(finding.getStatus()))
            {
                inProgressFinding.add(finding);
            }
            else if (Status.DELAYED.equals(finding.getStatus()))
            {
                overDueFinding.add(finding);
            }
            else if (Status.CONFIRMED_COMPLETED.equals(finding.getStatus()))
            {
                confirmedCompletedFinding.add(finding);
            }
            else if(Status.UNCONFIRMED_COMPLETED.equals(finding.getStatus()))
            {
                unconfirmedCompletedFinding.add(finding);
            }
        }
    }

    /**
    @Override
    public String findKpiRateFromTable(BigDecimal higherOverdueKPIRate, BigDecimal lowerOverdueKPIRate)
    {
        BigDecimal kpiRate = this.summaryReportDAO.findKpiRateFromTable(higherOverdueKPIRate, lowerOverdueKPIRate);
        return kpiRate != null ? String.valueOf(kpiRate.multiply(new BigDecimal("100")).setScale(0)) : "N/A";
    }**/


    @Resource(name = "summaryReportDAO")
    public void setSummaryReportDAO(SummaryReportDAO summaryReportDAO)
    {
        this.summaryReportDAO = summaryReportDAO;
    }

    @Override
    protected GenericDAO getDAO() {
        return summaryReportDAO;
    }
}
