package com.tr.cimsa.denetimtaleptakip.services.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.tr.cimsa.denetimtaleptakip.dao.FindingDAO;
import com.tr.cimsa.denetimtaleptakip.dao.GenericDAO;
import com.tr.cimsa.denetimtaleptakip.dao.SummaryReportDAO;
import com.tr.cimsa.denetimtaleptakip.model.BusinessProcessType;
import com.tr.cimsa.denetimtaleptakip.model.BusinessSubProcessType;
import com.tr.cimsa.denetimtaleptakip.model.Finding;
import com.tr.cimsa.denetimtaleptakip.model.FindingAction;
import com.tr.cimsa.denetimtaleptakip.services.FindingExcelService;
import com.tr.cimsa.denetimtaleptakip.services.SummaryReportExcelService;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.joda.time.LocalDate;
import org.springframework.stereotype.Service;


@Service("summaryReportExcelService")
public class SummaryReportExcelServiceImpl extends BaseServiceImpl implements SummaryReportExcelService
{
    private SummaryReportDAO summaryReportDAO;

    @Override
    public Workbook prepareExcelReportOfTheFindings(List<Finding> findings)
    {
        Workbook workbook = new HSSFWorkbook();
        Map<String, CellStyle> styles = createStyles(workbook);
        Sheet sheet = workbook.createSheet("Findings");
        int baslikSayisi = baslikOlustur(sheet, styles);
        int i = 1;
        for (Finding finding : findings)
        {
            Row row = sheet.createRow(i);

            Cell cell = row.createCell(0);
            cell.setCellValue(finding.getFindingNumber());
            cell.setCellStyle(styles.get("normalStyle"));

            cell = row.createCell(1);
            cell.setCellValue(finding.getBusinessSubProcessType().getDescription());
            cell.setCellStyle(styles.get("normalStyle"));

            cell = row.createCell(2);
            cell.setCellValue(finding.getDescription());
            cell.setCellStyle(styles.get("normalStyle"));

            cell = row.createCell(3);
            cell.setCellValue(finding.getPriority().getDescription());
            cell.setCellStyle(styles.get("normalStyle"));

            cell = row.createCell(4);
            cell.setCellValue(finding.getCompletionDate());
            cell.setCellStyle(styles.get("dateStyle"));

            cell = row.createCell(5);
            cell.setCellValue(String.valueOf(finding.getCountOfDelayedDay()));
            cell.setCellStyle(styles.get("normalStyle"));

            cell = row.createCell(6);
            cell.setCellValue(finding.getStatus().getDescription());
            cell.setCellStyle(styles.get("normalStyle"));
            i++;
        }
        for (i = baslikSayisi; i >= 0; i--)
            sheet.autoSizeColumn(i);
        return workbook;
    }

    private Map<String, CellStyle> createStyles(Workbook workbook)
    {
        Map<String, CellStyle> styles = new HashMap<String, CellStyle>();
        Font font = workbook.createFont();
        font.setFontHeightInPoints((short) 10);
        font.setFontName("Arial");
        font.setColor(IndexedColors.BLACK.getIndex());

        DataFormat dateFormat = workbook.createDataFormat();

        CellStyle style = workbook.createCellStyle();
        style.setFont(font);
        style.setWrapText(true);
        styles.put("normalStyle", style);

        style = workbook.createCellStyle();
        style.setFont(font);
        style.setDataFormat(dateFormat.getFormat("dd/MM/yyyy"));
        styles.put("dateStyle", style);

        CellStyle headerStyle = workbook.createCellStyle();

        headerStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);

        font = workbook.createFont();
        font.setFontHeightInPoints((short) 10);
        font.setFontName("Arial");
        font.setColor(IndexedColors.BLACK.getIndex());
        headerStyle.setFont(font);
        styles.put("criteriaHeaderStyle", headerStyle);
        headerStyle.setFillForegroundColor(HSSFColor.LIGHT_YELLOW.index);
        styles.put("headerStyle", headerStyle);
        return styles;
    }

    private int baslikOlustur(Sheet sheet, Map<String, CellStyle> styles)
    {
        int baslikSayisi = 0;
        Row row = sheet.createRow(0);

        Cell cell = row.createCell(0);
        cell.setCellValue("Finding Number");
        cell.setCellStyle(styles.get("headerStyle"));

        cell = row.createCell(1);
        cell.setCellValue("Sub Process");
        cell.setCellStyle(styles.get("headerStyle"));

        cell = row.createCell(2);
        cell.setCellValue("Finding Title");
        cell.setCellStyle(styles.get("headerStyle"));

        cell = row.createCell(3);
        cell.setCellValue("Priority");
        cell.setCellStyle(styles.get("headerStyle"));

        cell = row.createCell(4);
        cell.setCellValue("Target Date");
        cell.setCellStyle(styles.get("headerStyle"));

        cell = row.createCell(5);
        cell.setCellValue("Delay Time (days)");
        cell.setCellStyle(styles.get("headerStyle"));

        cell = row.createCell(6);
        cell.setCellValue("Status");
        cell.setCellStyle(styles.get("headerStyle"));

        baslikSayisi = (int) row.getLastCellNum();
        return baslikSayisi;
    }

    @Override
    protected GenericDAO getDAO()
    {
        return summaryReportDAO;
    }

    @Resource(name = "summaryReportDAO")
    public void setFindingDAO(SummaryReportDAO summaryReportDAO)
    {
        this.summaryReportDAO = summaryReportDAO;
    }
}
