package com.tr.cimsa.denetimtaleptakip.services.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.mail.internet.MimeMessage;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.joda.time.DateTimeZone;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.Period;
import org.joda.time.tz.DateTimeZoneBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StopWatch;
import org.stringtemplate.v4.ST;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.tr.cimsa.denetimtaleptakip.model.Department;
import com.tr.cimsa.denetimtaleptakip.model.EmailContent;
import com.tr.cimsa.denetimtaleptakip.model.EmailSendingStrategyEnum;
import com.tr.cimsa.denetimtaleptakip.model.Finding;
import com.tr.cimsa.denetimtaleptakip.model.FindingAction;
import com.tr.cimsa.denetimtaleptakip.model.Priority;
import com.tr.cimsa.denetimtaleptakip.model.Status;
import com.tr.cimsa.denetimtaleptakip.model.User;
import com.tr.cimsa.denetimtaleptakip.services.FindingExcelService;
import com.tr.cimsa.denetimtaleptakip.services.FindingService;
import com.tr.cimsa.denetimtaleptakip.services.MailService;

/**
 * User: mhazer Date: 8/22/12 Time: 5:02 PM
 */
@Service("mailService")
public class MailServiceImpl implements MailService {
	private final Logger logger = LoggerFactory.getLogger(MailServiceImpl.class);

	private FindingService findingService;

	private JavaMailSender javaMailSender;

	private SimpleMailMessage templateMailMessage;

	private String auditSystemURL;
	private Boolean enableEmails;
	private Boolean batchJobEnabled;

	@Resource(name = "mailSender")
	public void setJavaMailSender(JavaMailSender javaMailSender) {
		this.javaMailSender = javaMailSender;
	}

	@Resource(name = "templateMessage")
	public void setTemplateMailMessage(SimpleMailMessage templateMailMessage) {
		this.templateMailMessage = templateMailMessage;
	}

	@Resource(name = "findingService")
	public void setFindingService(FindingService findingService) {
		this.findingService = findingService;
	}

	@Resource(name = "auditSystemURL")
	public void setAuditSystemURL(String auditSystemURL) {
		this.auditSystemURL = auditSystemURL;
	}

	@Resource(name = "enableEmails")
	public void setEnableEmails(Boolean enableEmails) {
		this.enableEmails = enableEmails;
	}

	@Resource(name = "batchJobEnabled")
	public void setBatchJobEnabled(Boolean batchJobEnabled) {
		this.batchJobEnabled = batchJobEnabled;
	}

	@Autowired
	private FindingExcelService findingExcelService;

	@Override
	@Async
	@Transactional
	public void sendNotificationEmails() {
		if (batchJobEnabled) {
			StopWatch stopWatch = new StopWatch("sending finding emails");
			stopWatch.start();

			// 3 ayda bir departman maili at
			sendInformingEmails();

			logger.debug("scanning findings and sending mails");
			List<Finding> notCompletedFindings = findingService.retrieveNotCompletedFindings();
			logger.debug("there are {} not completed findings", notCompletedFindings.size());
			for (Finding notCompletedFinding : notCompletedFindings) {
				try {
					Integer findingId = notCompletedFinding.getAuditType().getEmailSendingStrategy().getId();
					if (EmailSendingStrategyEnum.THREE_MONTH_STRATEGY.getCode().equals(findingId)) {
						this.executeThreeMonthsEmailStrategy(notCompletedFinding);
					} else if (EmailSendingStrategyEnum.ONE_DAY_BEFORE_STRATEGY.getCode().equals(findingId)) {
						this.executeOneDayBeforeEmailStrategy(notCompletedFinding);
					} else if (EmailSendingStrategyEnum.ENERJISA_STRATEGY.getCode().equals(findingId)) {
						this.executeEnerjisaEmailStrategy(notCompletedFinding);
					}
				} catch (Exception exception) {
					exception.printStackTrace();
					logger.error("mail sending job failed {}", exception.getMessage());
				}
			}
			stopWatch.stop();
			logger.info("total elapsed time {}", stopWatch.prettyPrint());
		}
	}

	private void executeThreeMonthsEmailStrategy(Finding notCompletedFinding) {
		LocalDate today = new LocalDate();
		LocalDate findingOpeningDate = new LocalDate(notCompletedFinding.getAuditDate());
		Period period = new Period(findingOpeningDate, today);
		if (!findingOpeningDate.equals(today) && period.getMonths() % 3 == 0) {
			if (period.getWeeks() == 0 && period.getDays() == 0) {
				logger.debug("3 months passed after opening a finding, so sending email");
				sendThreeMonthEmail(notCompletedFinding,
						notCompletedFinding.getAuditType().getEmailStrategyContentOne().getMailContent());
			} else if (period.getMonths() > 0 && period.getWeeks() == 1 && period.getDays() == 0) {
				boolean sendSecondEmail = false;
				CHECK_SENDING_SECOND_EMAIL_REQUIREMENTS: for (FindingAction findingAction : notCompletedFinding
						.getFindingActions()) {
					LocalDate userActionDate = new LocalDate(findingAction.getChangeDate());
					LocalDate aWeekBefore = today.minusWeeks(1);
					if (findingAction.getChangeDate() == null || userActionDate.isBefore(aWeekBefore)) {
						sendSecondEmail = true;
						break CHECK_SENDING_SECOND_EMAIL_REQUIREMENTS;
					}
				}
				if (sendSecondEmail) {
					logger.debug("3 months + 1 week passed after opening a finding, so sending second email");
					sendThreeMonthPlusOneWeekEmail(notCompletedFinding,
							notCompletedFinding.getAuditType().getEmailStrategyContentTwo().getMailContent());
				}
			}
		}
	}

	@Override
	public void executeEnerjisaEmailStrategy(Finding notCompletedFinding) {

		logger.debug("MailServiceImpl.executeEnerjisaEmailStrategy()");

		LocalDate today = new LocalDate();

		LocalDate findingOpeningDate = new LocalDate(notCompletedFinding.getAuditDate());

		Period period = new Period(findingOpeningDate, today);

		LocalDate findingCompletionDate = prepareCompletionDate(notCompletedFinding);
		;

		int fark = Math.abs(Days.daysBetween(today, findingCompletionDate).getDays());

		if (!findingOpeningDate.equals(today) && period.getMonths() % 3 == 0 && period.getWeeks() == 0
				&& period.getDays() == 0) {

			sendThreeMonthEmail(notCompletedFinding,
					notCompletedFinding.getAuditType().getEmailStrategyContentThree().getMailContent());

		} else if (today.isAfter(findingCompletionDate) && fark % 7 == 0
				&& notCompletedFinding.getAuditType().getSendWeeklyReminderMail() != null) {

			sendEmailForWeeklyReminderMail(notCompletedFinding,
					notCompletedFinding.getAuditType().getSendWeeklyReminderMail());

		} else {
			findingCompletionDate = prepareCompletionDate(notCompletedFinding);

			if (today.equals(findingCompletionDate.minusWeeks(1))) {

				sendOneWeekBeforeDeadlineEmail(notCompletedFinding,
						notCompletedFinding.getAuditType().getEmailStrategyContentOne());

			} else if (today.equals(findingCompletionDate.plusDays(1))) {

				sendOneDayAfterDeadlineEmail(notCompletedFinding,
						notCompletedFinding.getAuditType().getEmailStrategyContentTwo());
			}
		}
	}

	@Override
	public void executeEnerjisaEmailStrategyForStartMailService(Finding notCompletedFinding) {
		notCompletedFinding = this.findingService.retrieveFindingByTransactional(notCompletedFinding.getId());
		executeEnerjisaEmailStrategy(notCompletedFinding);
	}

	private LocalDate prepareCompletionDate(Finding notCompletedFinding) {
		LocalDate findingCompletionDate;
		if (notCompletedFinding.getRevised() && notCompletedFinding.getRevisedCompletionDate() != null) {
			findingCompletionDate = new LocalDate(notCompletedFinding.getRevisedCompletionDate());
		} else {
			findingCompletionDate = new LocalDate(notCompletedFinding.getCompletionDate());
		}
		return findingCompletionDate;
	}

	private void sendOneDayAfterDeadlineEmail(Finding notCompletedFinding, EmailContent emailStrategyContent) {
		logger.debug("trying to send one day after deadline mail");
		Preconditions.checkNotNull(emailStrategyContent, "can not find mail content template");
		String mailTemplate = emailStrategyContent.getMailContent();
		ST stringTemplate = new ST(mailTemplate, '$', '$');
		logger.info("ST is {}", stringTemplate);
		stringTemplate = editMailTemplateForFinding(notCompletedFinding, stringTemplate);
		String mailBody = stringTemplate.render(new Locale("tr", "TR"));

		List<String> toAdresses = User.usersEmailAddresses(notCompletedFinding.usersOfFindingActionsHasAction());
		HashSet<String> ccAddresses = new HashSet<String>(
				User.usersEmailAddresses(notCompletedFinding.getAuditType().getAuditTypeUpdateFollowers()));

		if (notCompletedFinding.getPriority() == Priority.VERY_HIGH
				|| notCompletedFinding.getPriority() == Priority.HIGH
				|| notCompletedFinding.getStatus() == Status.DELAYED) {

			List<String> ccAddressesForFunctionHeads = User
					.usersEmailAddresses(notCompletedFinding.getBusinessSubProcessType().getFunctionHeads());
			ccAddressesForFunctionHeads
					.addAll(User.usersEmailAddresses(notCompletedFinding.getBusinessProcessType().getFunctionHeads()));
			ccAddresses.addAll(ccAddressesForFunctionHeads);
		}
		ccAddresses.removeAll(toAdresses);
		this.sendEmail(mailBody, toAdresses, ccAddresses, notCompletedFinding.getAuditType().getEmailSubject());
	}

	private void sendThreeMonthEmail(Finding finding, String mailContent) {
		sendEmailToFindingActionOwnersAndResponsibleHeads(finding, mailContent);
	}

	private void sendThreeMonthPlusOneWeekEmail(Finding finding, String mailContent) {
		sendEmailToFindingActionOwnersAndResponsibleHeads(finding, mailContent);
	}

	private void executeOneDayBeforeEmailStrategy(Finding notCompletedFinding) {
		LocalDate today = new LocalDate();
		LocalDate findingCompletionDate = new LocalDate(notCompletedFinding.getCompletionDate());
		if (!today.equals(findingCompletionDate.minusDays(1))) {
			logger.debug("trying to send one day before mail strategy");
			Preconditions.checkNotNull(notCompletedFinding.getAuditType().getEmailStrategyContentOne(),
					"can not find mail content template");
			String mailTemplate = notCompletedFinding.getAuditType().getEmailStrategyContentOne().getMailContent();
			ST stringTemplate = new ST(mailTemplate, '$', '$');
			logger.info("ST is {}", stringTemplate);
			stringTemplate = editMailTemplateForFinding(notCompletedFinding, stringTemplate);
			String mailBody = stringTemplate.render(new Locale("tr", "TR"));
			List<String> toAdresses = User.usersEmailAddresses(notCompletedFinding.usersOfFindingActionsHasAction());
			HashSet<String> ccAddresses = new HashSet<String>(
					User.usersEmailAddresses(notCompletedFinding.getAuditType().getAuditTypeUpdateFollowers()));
			ccAddresses.removeAll(toAdresses);

			this.sendEmail(mailBody, toAdresses, ccAddresses, notCompletedFinding.getAuditType().getEmailSubject());
		}
	}

	private void sendOneWeekBeforeDeadlineEmail(Finding notCompletedFinding, EmailContent emailStrategyContent) {
		System.out.println("MailServiceImpl.sendOneWeekBeforeDeadlineEmail()");
		logger.debug("trying to send one week before mail");
		Preconditions.checkNotNull(emailStrategyContent, "can not find mail content template");
		String mailTemplate = emailStrategyContent.getMailContent();
		ST stringTemplate = new ST(mailTemplate, '$', '$');
		logger.info("ST is {}", stringTemplate);
		stringTemplate = editMailTemplateForFinding(notCompletedFinding, stringTemplate);
		String mailBody = stringTemplate.render(new Locale("tr", "TR"));
		List<String> toAdresses = User.usersEmailAddresses(notCompletedFinding.usersOfFindingActionsHasAction());
		HashSet<String> ccAddresses = new HashSet<String>(
				User.usersEmailAddresses(notCompletedFinding.getAuditType().getAuditTypeUpdateFollowers()));
		ccAddresses.removeAll(toAdresses);
		this.sendEmail(mailBody, toAdresses, ccAddresses, notCompletedFinding.getAuditType().getEmailSubject());
	}

	@Override
	@Async
	@Transactional(readOnly = true)
	public void sendEmailToFindingActionOwnersAndResponsibleHeads(Finding finding, String templateName) {
		if (finding.getSendEmail()) {
			sendEmail(finding, templateName);
		}
	}

	@Override
	@Async
	@Transactional(readOnly = true)
	public void sendOpeningEmailToFindingActionOwnersAndResponsibleHeads(Finding finding, String templateName) {
		if (finding.getAuditType().isSendOpeningEmail() && finding.getVersion().equals(NumberUtils.INTEGER_ZERO)) {
			sendEmailWithoutToCC(finding, templateName);
		}
	}

	public void sendEmail(Finding finding, String templateName) {
		try {

			logger.debug("trying to send mail");
			finding = this.findingService.retrieveFinding(finding.getId());
			ST stringTemplate = new ST(templateName, '$', '$');
			logger.info("ST is {}", stringTemplate);
			stringTemplate = editMailTemplateForFinding(finding, stringTemplate);
			String mailBody = stringTemplate.render(new Locale("tr", "TR"));
			List<String> toAdresses = User.usersEmailAddresses(finding.usersOfFindingActions());
			HashSet<String> ccAddresses = prepareCCAddressesOfFinding(finding);
			ccAddresses.removeAll(toAdresses);
			this.sendEmail(mailBody, toAdresses, ccAddresses, finding.getAuditType().getEmailSubject());
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error("can not send email {} {}", ex.getMessage(), finding);
		}
	}

	private void sendEmailWithoutToCC(Finding finding, String templateName) {
		try {

			logger.debug("trying to send mail");
			finding = this.findingService.retrieveFinding(finding.getId());
			ST stringTemplate = new ST(templateName, '$', '$');
			logger.info("ST is {}", stringTemplate);
			stringTemplate = editMailTemplateForFinding(finding, stringTemplate);
			String mailBody = stringTemplate.render(new Locale("tr", "TR"));
			List<String> toAdresses = User.usersEmailAddresses(finding.usersOfFindingActions());
			this.sendEmail(mailBody, toAdresses, null, finding.getAuditType().getEmailSubject());
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error("can not send email {} {}", ex.getMessage(), finding);
		}
	}

	private HashSet<String> prepareCCAddressesOfFinding(Finding finding) {

		HashSet<String> ccAddresses = new HashSet<String>(
				User.usersEmailAddresses(finding.getBusinessSubProcessType().getResponsibleHeads()));
		List<String> ccAddressesForFuncHeads = User
				.usersEmailAddresses(finding.getBusinessSubProcessType().getFunctionHeads());

		if (finding.getPriority() == Priority.VERY_HIGH || finding.getPriority() == Priority.HIGH
				|| finding.getStatus() == Status.DELAYED) {
			ccAddresses.addAll(User.usersEmailAddresses(finding.getBusinessProcessType().getResponsibleHeads()));
			ccAddressesForFuncHeads
					.addAll(User.usersEmailAddresses(finding.getBusinessProcessType().getFunctionHeads()));
			ccAddresses.addAll(ccAddressesForFuncHeads);
		}
		ccAddresses.addAll(ccAddressesForFuncHeads);

		return ccAddresses;
	}

	private void sendEmail(String mailBody, List<String> toAddress, HashSet<String> ccAddresses, String mailSubject) {
		try {
			if (!enableEmails) {
				logger.debug("emails are disabled in configuration");
				return;
			} else {
				logger.debug("trying to send mail");
				MimeMessage mimeMessage = this.javaMailSender.createMimeMessage();
				MimeMessageHelper message = new MimeMessageHelper(mimeMessage, "ISO-8859-9");
				mimeMessage.setContent(mailBody, "text/html; charset=UTF-8");
//                message.setFrom("afus@cimsa.com.tr");
				message.setFrom("techselenity@gmail.com");

				message.setTo(toAddress.toArray(new String[0]));
				if (ccAddresses != null) {
					message.setCc(ccAddresses.toArray(new String[0]));
				}
//                message.setBcc("d.pinar@cimsa.com.tr");
				message.setSubject(mailSubject);

				this.javaMailSender.send(mimeMessage);
				logger.debug("mail sent successfully");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error("can not send email {}", ex.getMessage());
		}
	}

	public void sendEmailForWeeklyReminderMail(Finding notCompletedFinding, EmailContent emailStrategyContent) {

		System.out.println("MailServiceImpl.sendEmailForWeeklyReminderMail()");
		try {
			logger.debug("trying to send Weekly Reminder mail");
			Preconditions.checkNotNull(emailStrategyContent, "can not find mail content template");
			String mailTemplate = emailStrategyContent.getMailContent();
			ST stringTemplate = new ST(mailTemplate, '$', '$');
			logger.info("ST is {}", stringTemplate);
			stringTemplate = editMailTemplateForFinding(notCompletedFinding, stringTemplate);
			String mailBody = stringTemplate.render(new Locale("tr", "TR"));

			List<String> toAdresses = User.usersEmailAddresses(notCompletedFinding.usersOfFindingActionsHasAction());

			HashSet<String> ccAddresses = new HashSet<String>(
					User.usersEmailAddresses(notCompletedFinding.getAuditType().getAuditTypeUpdateFollowers()));

			if (notCompletedFinding.getPriority() == Priority.VERY_HIGH
					|| notCompletedFinding.getPriority() == Priority.HIGH
					|| notCompletedFinding.getStatus() == Status.DELAYED) {

				List<String> ccAddressesForFunctionHeads = User
						.usersEmailAddresses(notCompletedFinding.getBusinessSubProcessType().getFunctionHeads());
				ccAddressesForFunctionHeads.addAll(
						User.usersEmailAddresses(notCompletedFinding.getBusinessProcessType().getFunctionHeads()));
				ccAddresses.addAll(ccAddressesForFunctionHeads);
			}
			ccAddresses.removeAll(toAdresses);

			this.sendEmail(mailBody, toAdresses, ccAddresses, notCompletedFinding.getAuditType().getEmailSubject());
		} catch (Exception ex) {
			logger.error("can not send email {} {}", ex.getMessage());
		}

	}

	public String editMailContentForFindingAction(FindingAction findingAction, String mailContent) {

		Finding finding = this.findingService.retrieveFinding(findingAction.getFinding().getId());

		String mailTemplate = finding.getAuditType().getUpdateEmailContent().getMailContent();
		ST stringTemplate = new ST(mailTemplate, '$', '$');
		logger.info("ST is {}", stringTemplate);

		stringTemplate = editMailTemplateForFinding(finding, stringTemplate);
		stringTemplate.add("userName", findingAction.getActionOwner().getNameSurname());

		String mailBody = stringTemplate.render(new Locale("tr", "TR"));

		return mailBody;
	}

	@Override
	@Async
	@Transactional(readOnly = true)
	public void sendUpdateEmailForEditetFinding(Finding finding) {
		try {

			logger.debug("MailServiceImpl.sendUpdateEmailForEditetFinding()");

			finding = this.findingService.retrieveFinding(finding.getId());
			if (CollectionUtils.isNotEmpty(finding.getAuditType().getAuditTypeUpdateFollowers())
					&& finding.getAuditType().getUpdateEmailContent() != null) {

				logger.debug("trying to send finding action update mail");
				List<String> toAdresses = User
						.usersEmailAddresses(finding.getAuditType().getAuditTypeUpdateFollowers());
				String mailTemplate = finding.getAuditType().getUpdateEmailContent().getMailContent();
				ST stringTemplate = new ST(mailTemplate, '$', '$');
				logger.info("ST is {}", stringTemplate);

				stringTemplate = editMailTemplateForFinding(finding, stringTemplate);

//				stringTemplate = editMailTemplateForFindingAction(findingAction, stringTemplate);

				String mailBody = stringTemplate.render(new Locale("tr", "TR"));
				this.sendEmail(mailBody, toAdresses, new HashSet<String>(), finding.getAuditType().getEmailSubject());
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error("can not send finding action update email {}", ex.getMessage());
		}
	}

	@Override
	@Async
	@Transactional(readOnly = true)
	public void sendFindingActionUpdateEmail(FindingAction findingAction) {
		try {
			logger.debug("MailServiceImpl.sendFindingActionUpdateEmail()");
			Finding finding = this.findingService.retrieveFinding(findingAction.getFinding().getId());
			if (CollectionUtils.isNotEmpty(finding.getAuditType().getAuditTypeUpdateFollowers())
					&& finding.getAuditType().getUpdateEmailContent() != null) {

				logger.debug("trying to send finding action update mail");
				List<String> toAdresses = User
						.usersEmailAddresses(finding.getAuditType().getAuditTypeUpdateFollowers());
				String mailTemplate = finding.getAuditType().getUpdateEmailContent().getMailContent();
				ST stringTemplate = new ST(mailTemplate, '$', '$');
				logger.info("ST is {}", stringTemplate);

				stringTemplate = editMailTemplateForFinding(finding, stringTemplate);

				stringTemplate = editMailTemplateForFindingAction(findingAction, stringTemplate);

				String mailBody = stringTemplate.render(new Locale("tr", "TR"));
				this.sendEmail(mailBody, toAdresses, new HashSet<String>(), finding.getAuditType().getEmailSubject());
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error("can not send finding action update email {}", ex.getMessage());
		}
	}

	public String[] getOwnerNameWithActionPercentage(Finding finding) {
		List<String> ownersInfo = Lists.newLinkedList();

		for (FindingAction findingAction : finding.getFindingActions()) {
			StringBuilder sb = new StringBuilder();
			sb.append(StringUtils.defaultString(findingAction.getActionOwner().getNameSurname(), StringUtils.EMPTY));
			sb.append(" - ");

			if (findingAction.isSpoc() && !findingAction.isActionOwnerFlag()) {
				sb.append("[SPOC]");

			} else if (findingAction.isReadOnly()) {

				sb.append("[READ ONLY]");
			} else {
				sb.append("% " + findingAction.getCompletionPercentage());
			}

			ownersInfo.add(sb.toString());
		}
		return ownersInfo.toArray(new String[ownersInfo.size()]);
	}

	public String getLastActionTakenForFinding(Finding finding) {
		List<String> ownersInfo = Lists.newLinkedList();

		Date maxDate = null;
		FindingAction lastFindingAction = null;

		for (FindingAction findingAction : finding.getFindingActions()) {
			if (maxDate == null) {
				maxDate = findingAction.getChangeDate();
				lastFindingAction = findingAction;
			}

			if (maxDate.before(findingAction.getChangeDate())) {
				maxDate = findingAction.getChangeDate();
				lastFindingAction = findingAction;
			}

		}

		return lastFindingAction.getActionTaken();

	}

	private ST editMailTemplateForFinding(Finding finding, ST stringTemplate) {

		try {
			stringTemplate.add("findingNo", finding.getFindingNumber());
			stringTemplate.add("findingDescription", finding.getDescription());
			stringTemplate.add("findingPriority", finding.getPriority().getDescription());
			stringTemplate.add("findingStatus", finding.getStatus().getDescription());
			stringTemplate.add("businessProcess", finding.getBusinessSubProcessType().getDescription());
			stringTemplate.add("findingDetail", finding.getDetail());
			stringTemplate.add("findingRisk", finding.getRisk());
			stringTemplate.add("findingRecommandation", finding.getRecommendation());
			stringTemplate.add("managementActionPlan", finding.getManagementActionPlan());
			stringTemplate.add("completionDate",
					new SimpleDateFormat("dd/MM/yyyy").format(finding.getCompletionDate()));
			stringTemplate.add("completionPercentage", finding.getCompPercentage());
			stringTemplate.add("findingSummary", finding.getFindingSummary());
			stringTemplate.add("actionTaken", getLastActionTakenForFinding(finding));

			if (finding.getRevised() && finding.getRevisedCompletionDate() != null) {
				stringTemplate.add("revisedCompletionDate",
						new SimpleDateFormat("dd/MM/yyyy").format(finding.getRevisedCompletionDate()));
			}

		} catch (Exception e) {
			logger.debug("MailServiceImpl.editMailTemplateForFinding() e -> " + e);
		}

		stringTemplate.add("URL", auditSystemURL);
		stringTemplate.add("ownersInfo", getOwnerNameWithActionPercentage(finding));
		return stringTemplate;
	}

	private ST editMailTemplateForFindingAction(FindingAction findingAction, ST stringTemplate) {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			stringTemplate.add("userName", findingAction.getActionOwner().getNameSurname());
			stringTemplate.add("actionTaken", findingAction.getActionTaken());
			if (findingAction.isSpoc() && !findingAction.isActionOwnerFlag()) {
				stringTemplate.add("completionPercentageForActionOwner", "SPOC");

			} else if (findingAction.isReadOnly()) {
				stringTemplate.add("completionPercentageForActionOwner", "READ ONLY");
			} else {
				stringTemplate.add("completionPercentageForActionOwner", findingAction.getCompletionPercentage());

			}
			stringTemplate.add("explanationForDelay", findingAction.getExplanationForDelay());

			stringTemplate.add("updateTime",
					findingAction.getChangeDate() != null ? sdf.format(findingAction.getChangeDate()) : "");
		} catch (Exception e) {
			logger.debug("MailServiceImpl.editMailTemplateForFindingAction() e -> " + e);
		}

		return stringTemplate;
	}

	@Override
	public void sendInformingEmails() {

		logger.debug("com.tr.cimsa.denetimtaleptakip.services.impl.MailServiceImpl.sendInformingEmails()");
		
		logger.debug("scanning findings and sending mails to department's heads");
		
		List<Finding> findingOfDepartmentList = new ArrayList<Finding>();

		Calendar cal = Calendar.getInstance();

		int lastDayOfCurrentMonth = cal.getActualMaximum(Calendar.DATE);

//		if ((cal.get(Calendar.MONTH) ) % 3 == 0 && cal.get(Calendar.DAY_OF_MONTH) == lastDayOfCurrentMonth) {
		if ((cal.get(Calendar.MONTH) + 1) % 3 == 0 && cal.get(Calendar.DAY_OF_MONTH) == lastDayOfCurrentMonth) { // doğrusu
			List<Finding> openFindings = findingService.retrieveAllFindings();
			logger.debug("there are {} not completed findings", openFindings.size());

			for (Finding openFinding : openFindings) {

				if (openFinding.getStatus() != null
						&& ((openFinding.getStatus().getDescription().contains("In Progress"))
								|| (openFinding.getStatus().getDescription().contains("Not Started"))
								|| (openFinding.getStatus().getDescription().contains("Overdue")))) {

					findingOfDepartmentList.add(openFinding);
				}
			}
			executeEnerjisaEmailStrategyForInformationMail(findingOfDepartmentList);
		}
	}

	@Override
	public void executeEnerjisaEmailStrategyForInformationMail(List<Finding> openFinding) {
		
		logger.debug("MailServiceImpl.executeEnerjisaEmailStrategyForInformationMail()");

		List<Department> departments = new ArrayList<Department>();

		departments = findingService.retrieveDepartments();
		if (openFinding != null) {
			HashMap<Integer, List<Finding>> map = new HashMap<Integer, List<Finding>>();
			for (Department department : departments) {
				for (Finding finding : openFinding) {
					try {
						if (finding.getDepartment() != null) {
							if (department.getId() == finding.getDepartment().getId()) {
								if (map.containsKey(department.getId())) {
									List<Finding> list = map.get(department.getId());
									list.add(finding);
								} else {
									List<Finding> list = new ArrayList<>();
									list.add(finding);
									map.put(department.getId(), list);
								}
							}
						}
					} catch (Exception e) {
						logger.error("MailServiceImpl.executeEnerjisaEmailStrategyForInformationMail() -> "
								+ e.getMessage());
					}
				}
			}

			for (Map.Entry<Integer, List<Finding>> entry : map.entrySet()) {
				Integer key = entry.getKey();
				List<Finding> value = entry.getValue();

				Department department = value.get(0).getDepartment();

				Set<User> users = department.getDepartmentUpdateHeaders();
				Set<String> toAddress = new HashSet<String>();

				for (User user : users) {
					toAddress.add(user.getEmail());
				}

				// String mailBody = department.getDepartmentEmailContent().getMailContent();
				String mailBody = "";

				if (department.getDepartmentEmailContent() != null) {
					mailBody = department.getDepartmentEmailContent().getMailContent();

				}
				String mailSubject = "department report";
				if (department.getEmailSubject() != null) {
					mailSubject = department.getEmailSubject();
				}

				sendMailAddAttachment(findingExcelService.prepareExcelReportOfTheDepartmentHeads(value), mailSubject,
						mailBody, toAddress);
			}
		}

	}

	private void sendMailAddAttachment(Workbook workbook, String mailSubject, String mailBody, Set<String> toAddress) {

		try {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			workbook.write(bos);
			InputStreamSource iss = new InputStreamSource() {
				@Override
				public InputStream getInputStream() throws IOException {
					return new ByteArrayInputStream(bos.toByteArray());
				}
			};

			MimeMessage mimeMessage = this.javaMailSender.createMimeMessage();
			MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, "ISO-8859-9");

			//                message.setFrom("afus@cimsa.com.tr");

			message.setFrom("techselenity@gmail.com");
			message.setTo(toAddress.toArray(new String[0]));
//                if (ccAddresses != null) {
//                    message.setCc(ccAddresses.toArray(new String[0]));
//                }
////                message.setBcc("d.pinar@cimsa.com.tr");
//                message.setSubject(mailSubject);
			
			message.setSubject(mailSubject);
			message.setText(mailBody, true);
			
//                mimeMessage.setContent(mailBody, "text/html; charset=UTF-8");
			message.addAttachment("DepartmentReport.xls", iss);

			this.javaMailSender.send(mimeMessage);
			logger.debug("mail sent successfully");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
