package com.tr.cimsa.denetimtaleptakip.services;

import com.tr.cimsa.denetimtaleptakip.controller.admin.SearchParameters;
import com.tr.cimsa.denetimtaleptakip.dao.FindingDAO;
import com.tr.cimsa.denetimtaleptakip.model.*;

import lombok.Getter;

import com.tr.cimsa.denetimtaleptakip.controller.admin.SearchParameters;
import com.tr.cimsa.denetimtaleptakip.dao.FindingDAO;
import com.tr.cimsa.denetimtaleptakip.model.*;
import org.primefaces.model.SortOrder;

import java.util.List;
import java.util.Map;

/**
 * User: mhazer Date: 8/19/12 Time: 5:04 PM
 */
public interface FindingService extends BaseService {

	public FindingDAO getFindingDAO();

	public List<AuditType> retrieveAuditTypes();

	public List<Company> retrieveCompanies();

	public AuditType retrieveAuditType(String description);

	public Company retrieveCompany(String description);
	
	public Department retrieveDepartment(String description);
	
	public List<BusinessProcessType> retrieveBusinessProcesses();

	public Finding mergeFinding(Finding finding);

	public List<Finding> retrieveAllFindings();

	public List<FindingAction> retrieveUserFindingActions(User user);

	public void mergeFindingAction(FindingAction selectedFindingAction);

	public void saveFindingAction(FindingAction selectedFindingAction);

	public void mergeFindingResponsibleAction(FindingResponsibleAction selectedFindingResponsibleAction);

	public Finding retrieveFinding(Integer findingIdToUpdate);

    public AuditType mergeAuditType(AuditType auditType, List<BusinessProcessType> businessProcessTypes, List<User> target);
    
	public Finding retrieveFinding(String findingNumber);

    public BusinessProcessType mergeBusinessType(BusinessProcessType businessProcessType, List<User> responsibleHeads, List<User> functionHeads);
	public List<Department> retrieveDepartments();

	public List<BusinessSubProcessType> retrieveBusinessSubProcesses();

	public List<Finding> searchFindings(SearchParameters searchParameters, User user);

	public List<Finding> searchFindings(SearchParameters searchParameters);

	public List<Finding> retrieveNotCompletedFindings();
	
	public Finding retrieveFindingByTransactional(Integer findingIdToUpdate);

	public FindingAction retrieveFindingActionWithHistory(Integer findingActionId);

	public List<FindingAction> retrieveHistoryOfFindingAction(Integer findingActionId);

	public List<FindingResponsibleAction> retrieveHistoryOfFindingResponsibleAction(Integer findingResponsibleActionId);

	public List<Finding> retrieveUserResponsibleDepartmentsFindingActions(User user);

	List<AuditType> retrieveAllActiveAuditTypes();

	List<Company> retrieveAllActiveCompanies();

	List<Department> retrieveAllActiveDepartments();

	public List<EmailSendingStrategy> retrieveAllEmailStrategies();

	public List<EmailContent> retrieveAllEmailContents();

	public void reassignFinding(Finding finding, List<User> oldActionOwners, List<User> selectedOldActionOwners,
			User selectedNewActionOwner);

	public BusinessProcessType retrieveBusinessProcessType(String description);

	List<Finding> retrieveAllFindings(int first, int pageSize, String sortField, SortOrder sortOrder);

	Long retrieveAllFindingsCount();

	Long retrieveUserResponsibleDepartmentsFindingActionsCount(User user);

	List<Finding> retrieveUserResponsibleDepartmentsFindingActions(int first, int pageSize, String sortField,
			SortOrder sortOrder, User user);

	public Long retrieveUserResponsibleDepartmentsFindingActionsCountByParam(Map<String, Object> paramterMaps,
			User user);

	public List<Finding> retrieveUserResponsibleDepartmentsFindingActionsByParam(Map<String, Object> paramterMaps,
			int first, int pageSize, String sortField, SortOrder sortOrder, User user);

	Long retrieveUserFindingActionsCount(User user);
	
	public Long retrieveUserFindingActionsCountByParam(Map<String, Object> paramterMaps, User user);

	List<FindingAction> retrieveUserFindingActions(int first, int pageSize, String sortField, SortOrder sortOrder,
			User user);
	
	public List<FindingAction> retrieveUserFindingActionsByParam(Map<String, Object> paramterMaps ,int first, int pageSize, String sortField, SortOrder sortOrder, User user);
	
	public List<Finding> retrieveFindingsByParam(Map<String,Object> paramterMaps,int first, int pageSize, String sortField, SortOrder sortOrder);
	
	public Long retrieveFindingsCountByParam(Map<String, Object> paramterMaps);
	
	public List<String> getActionOwnersOfFindings();
	
	public List<String> getResponsibleOwnersOfFindings();
	
	public List<String> getActionOwnersOfMyDepartmentFindingsForSubBusinesProc(String userId);
	
	public List<String> getActionOwnersOfMyDepartmentFindingsForBusinessProc(String userId);
	
	public AuditType findByProperty(Class<?> clazz, String propertyName, String value);

	public void deleteFinding(Finding finding);

	public void deleteAuditType(AuditType auditType);

	public void deleteBusinessProcessType(BusinessProcessType businessProcessType);

	public void deleteBusinessSubProcessType(BusinessSubProcessType businessSubProcessType);

	public void saveOrUpdateEmailContent(EmailContent emailContent);

	public void deleteEmailContent(EmailContent emailContent);

	void deleteDepartment(Department department);

	public void removeFindingFile(List<FindingFile> findingFiles, Finding finding);

	Department mergeDepartmentType(Department departmentType, List<User> target);
	
	
	public Long retrieveFindingsStatusNullCount();
	
	public List<Finding> retrieveFindingsStatusNull();
	
	public FindingResponsibleAction getFindingResponsibleOfLastChange(Integer findingId);

	public FindingAction getFindingActionOfLastChange(Integer findingId);
}
