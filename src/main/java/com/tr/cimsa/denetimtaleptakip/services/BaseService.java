package com.tr.cimsa.denetimtaleptakip.services;

public interface BaseService
{
	public <T> void reattachUsingLock(T detachedEntity);
	
	public <T> void reattachReadOnly(T detachedEntity);
}
