package com.tr.cimsa.denetimtaleptakip.controller.admin;

import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;

import org.primefaces.event.ItemSelectEvent;
import org.primefaces.model.chart.PieChartModel;

import com.google.common.collect.Lists;
import com.tr.cimsa.denetimtaleptakip.controller.util.FacesUtils;
import com.tr.cimsa.denetimtaleptakip.model.AuditType;
import com.tr.cimsa.denetimtaleptakip.model.Finding;

import lombok.Getter;
import lombok.Setter;

/**
 * User: mhazer Date: 8/30/12 Time: 11:21 PM
 */
@ManagedBean(name = "numberOfFindingsPerYearGraphicalReportController")
@ViewScoped
public class NumberOfFindingsPerYearGraphicalReportController extends BaseAdminController implements Serializable {

	@Getter
	@Setter
	private List<AuditType> selectedAuditTypes = Lists.newArrayList();

	private List<Finding> selectedFindings;

	@Getter
	private PieChartModel pieChartModel;

	@PostConstruct
	public void init() {
		try {
			List<AuditType> myAuditTypes = commonDefinitionsController.getAuditTypes();

			int minId = this.findMinId(findingService.retrieveAuditTypes());

			List<Finding> mySelectedFindings = new LinkedList<Finding>();

			for (AuditType auditType : myAuditTypes) {

				if (auditType.getId() == minId) {
					SearchParameters searchParameters = new SearchParameters();
					List<AuditType> atlist = new ArrayList<>();
					atlist.add(auditType);
					searchParameters.setAuditTypes(atlist);
					mySelectedFindings.addAll(findingService.searchFindings(searchParameters));
					preparePieChartOfThis(mySelectedFindings);
					selectedAuditTypes.add(auditType);
					break;
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("init Exception " + e.getMessage());
		}
	}

	private PieChartModel initPieChartModel() {
		pieChartModel = new PieChartModel();
		pieChartModel.setResetAxesOnResize(true);
		pieChartModel.setLegendPosition("ne");
		return pieChartModel;
	}

	public void changeAuditType() {
		try {
			SearchParameters searchParameters = new SearchParameters();
			searchParameters.setAuditTypes(selectedAuditTypes);
			selectedFindings = findingService.searchFindings(searchParameters);
			preparePieChartOfThis(selectedFindings);

		} catch (Exception e) {
			System.err.println(e);
			FacesUtils.addMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), null);
		}
	}

	private PieChartModel preparePieChartOfThis(List<Finding> findings) {

		DateFormat df = new SimpleDateFormat("yyyy");

		PieChartModel _pieChartModel = initPieChartModel();
		for (Finding finding : findings) {
			String year = df.format(finding.getFindingYear());
			if (_pieChartModel.getData().get(year) == null) {
				_pieChartModel.set(year, 1);
			} else {
				Number oldValue = _pieChartModel.getData().get(year);
				Number newValue = (oldValue.intValue()) + 1;
				_pieChartModel.set(year, newValue);
				_pieChartModel.setShowDatatip(true);
			     
				_pieChartModel.setShowDataLabels(true);
				     
				_pieChartModel.setDataFormat("value"); 
			}

		}
		return _pieChartModel;
	}

	public static PieChartModel preparePieChart(List<Finding> findings) {
		DateFormat df = new SimpleDateFormat("yyyy");

		PieChartModel _pieChartModel = new PieChartModel();
		for (Finding finding : findings) {
			String year = df.format(finding.getFindingYear());
			if (_pieChartModel.getData().get(year) == null) {
				_pieChartModel.set(year, 1);
			} else {
				Number oldValue = _pieChartModel.getData().get(year);
				Number newValue = (oldValue.intValue()) + 1;
				_pieChartModel.set(year, newValue);
			}

		}
		return _pieChartModel;
	}

	public void selectPieChart(ItemSelectEvent event) throws IOException {
		String yearOfSelectedChartItem = Lists.newLinkedList(pieChartModel.getData().entrySet())
				.get(event.getItemIndex()).getKey();

		Flash flash = FacesContext.getCurrentInstance().getExternalContext().getFlash();
		flash.put("findingYear", yearOfSelectedChartItem);
		flash.put("selectedAuditTypes", selectedAuditTypes);

		FacesContext.getCurrentInstance().getExternalContext()
				.redirect(FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath()
						+ "/admin/searchFinding.xhtml?faces-redirect=true");
	}

	@Override
	protected Class getClazz() {
		return NumberOfFindingsPerYearGraphicalReportController.class;
	}
	
	public Integer findMinId(List<AuditType> list) { 
		List<Integer> idList = new ArrayList<Integer>();
		
		if (list != null || list.size() != 0) {
			for (AuditType at : list)
				idList.add(at.getId());
			return Collections.min(idList);
        }
	
		return null;
    }
}
