package com.tr.cimsa.denetimtaleptakip.controller.admin;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedProperty;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tr.cimsa.denetimtaleptakip.model.Finding;
import com.tr.cimsa.denetimtaleptakip.model.FindingAction;
import com.tr.cimsa.denetimtaleptakip.model.FindingResponsibleAction;
import com.tr.cimsa.denetimtaleptakip.model.Status;
import com.tr.cimsa.denetimtaleptakip.services.FindingService;

import lombok.Getter;
import lombok.Setter;

public class FindingAdminLazyDataModel extends LazyDataModel<Finding> {

	private static final long serialVersionUID = 1L;

	private final static Logger LOGGER = LoggerFactory.getLogger(FindingAdminLazyDataModel.class);

	private FindingService findingService;

	@Getter
	private List<String> actionOwnersOfFindings;

	@Getter
	private List<String> responsibleOwnersOfFindings;


	private CommanFunctionsService commanFunctionsService;

	public FindingAdminLazyDataModel(FindingService findingService ,CommanFunctionsService commanFunctionsService) {
		this.findingService = findingService;
		this.commanFunctionsService = commanFunctionsService;
	}

	@Override
	public List<Finding> load(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) {

		int totalDataSize = 0;

		totalDataSize = findingService.retrieveAllFindingsCount().intValue();

		int statusCount = findingService.retrieveFindingsStatusNullCount().intValue();

		if (statusCount > 0) {

			List<Finding> statusNullFindings = findingService.retrieveFindingsStatusNull();

			initActiveActionOwnersForTable(statusNullFindings);

		}

		actionOwnersOfFindings = findingService.getActionOwnersOfFindings();

		responsibleOwnersOfFindings = findingService.getResponsibleOwnersOfFindings();

		if (filters != null && !filters.isEmpty()) {

			Map<String, Object> paramMaps = new LinkedHashMap<>();

			for (String key : filters.keySet()) {
				Object value = filters.get(key);
				key = key.equals("auditDate") ? "AUDIT_DATE" : key;
				key = key.equals("findingYear") ? "FINDING_YEAR" : key;
				key = key.equals("revisedCompletionDate") ? "REVISED_COMP_DATE" : key;
				key = key.equals("completionDate") ? "COMPLETION_DATE" : key;

				if (key.equals("getStatus().getDescription()")) {

					value = value.equals("Overdue") ? "DELAYED" : value;
					value = value.equals("Unconfirmed-Completed") ? "UNCONFIRMED_COMPLETED" : value;
					value = value.equals("Confirmed-Completed") ? "CONFIRMED_COMPLETED" : value;
					value = value.equals("Not Started") ? "NOT_STARTED" : value;
					value = value.equals("In Progress") ? "IN_PROGRESS" : value;

					paramMaps.put("status", value);
				} else {
					paramMaps.put(key, value);
				}

			}

			List<Finding> filteredList = new ArrayList<Finding>();
			filteredList = findingService.retrieveFindingsByParam(paramMaps, first, pageSize, sortField, sortOrder);

			totalDataSize = findingService.retrieveFindingsCountByParam(paramMaps).intValue();
			this.setRowCount(totalDataSize);

			return filteredList;

		}

		this.setRowCount(totalDataSize);

		List<Finding> list = findingService.retrieveAllFindings(first, pageSize, sortField, sortOrder);

		return list;
	}

	public void initActiveActionOwnersForTable(List<Finding> findings) {

		int numberOfRealActOwners, sumOfFindingActionCompletionPercentages;

		for (Finding f : findings) {

			try {

				numberOfRealActOwners = 0;
				sumOfFindingActionCompletionPercentages = 0;

				for (FindingAction findingAction : f.findingActionsAsList()) {

					if (findingAction.isActionOwnerFlag()) { // Action owner lara göre comp perc hesaplanıyor
						numberOfRealActOwners++;
						sumOfFindingActionCompletionPercentages = sumOfFindingActionCompletionPercentages
								+ findingAction.getCompletionPercentage();
					}

					// -----------------------------------------------------------------------------------------------------------------------------

					// string action owners üretiyor gösterim için
					try {
						if (findingAction.isReadOnly()) {
							if (f.getActionOwners() != null
									&& !f.getActionOwners().contains(findingAction.getActionOwner().getNameSurname())) {
								f.setActionOwners(f.getActionOwners() + " • "
										+ findingAction.getActionOwner().getNameSurname() + " [Read Only]\n");
							} else {
								f.setActionOwners(
										" • " + findingAction.getActionOwner().getNameSurname() + " [Read Only]\n");
							}
						} else if (findingAction.isSpoc()) {
							if (f.getActionOwners() != null
									&& !f.getActionOwners().contains(findingAction.getActionOwner().getNameSurname())) {

								f.setActionOwners(f.getActionOwners() + " • "
										+ findingAction.getActionOwner().getNameSurname() + " [SPOC]\n");

							} else {
								f.setActionOwners(
										" • " + findingAction.getActionOwner().getNameSurname() + " [SPOC]\n");
							}

						} else {
							if (f.getActionOwners() != null
									&& !f.getActionOwners().contains(findingAction.getActionOwner().getNameSurname())) {
								f.setActionOwners(f.getActionOwners() + " • "
										+ findingAction.getActionOwner().getNameSurname() + "\n");
							} else {
								f.setActionOwners(" • " + findingAction.getActionOwner().getNameSurname() + "\n");
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}

					// -----------------------------------------------------------------------------------------------------------------------------
					// responsible ownersd generate ediliyor
					f.setResponsibleOwners(null);

					for (FindingResponsibleAction findingResponsibleAction : f.findingResponsibleActionsAsList()) {
						if (f.getResponsibleOwners() != null) {
							f.setResponsibleOwners(f.getResponsibleOwners() + " • "
									+ findingResponsibleAction.getActionOwner().getNameSurname() + "\n");
						} else {
							f.setResponsibleOwners(
									" • " + findingResponsibleAction.getActionOwner().getNameSurname() + "\n");
						}
					}

					f.setGeneralInfo(
							f.getAuditType().getDescription() + "\n" + f.getBusinessProcessType().getDescription()
									+ "\n" + f.getBusinessSubProcessType().getDescription() + "\n" + f.getReportName());

					// -----------------------------------------------------------------------------------------------------------------------------

				}

				// -----------------------------------------------------------------------------------------------------------------------------
				// hesaplanan comp perc set ediliyor

				try {
					if (f.getConfirmed() == true) {
						FindingResponsibleAction fra = findingService.getFindingResponsibleOfLastChange(f.getId());
						FindingAction fa = findingService.getFindingActionOfLastChange(f.getId());
						if ((fra != null)
								&& ((fa.getChangeDate() == null) || (fra.getChangeDate().after(fa.getChangeDate())))) {
							f.setCompPercentage(Integer.toString(fra.getCompletionPercentage()));
						} else {
							f.setCompPercentage(
									Integer.toString(sumOfFindingActionCompletionPercentages / numberOfRealActOwners));
						}
					} else {
						f.setCompPercentage(
								Integer.toString(sumOfFindingActionCompletionPercentages / numberOfRealActOwners));
					}
				} catch (ArithmeticException e) {
					f.setCompPercentage("0");
				}

				f = commanFunctionsService.updateStatus(f);

				f = commanFunctionsService.updatePeriod(f);

				findingService.mergeFinding(f);
				// -----------------------------------------------------------------------------------------------------------------------------

			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	}

}
