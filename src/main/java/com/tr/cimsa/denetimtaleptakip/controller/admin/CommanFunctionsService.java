package com.tr.cimsa.denetimtaleptakip.controller.admin;

import java.util.Date;

import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.tr.cimsa.denetimtaleptakip.model.Finding;
import com.tr.cimsa.denetimtaleptakip.model.FindingAction;
import com.tr.cimsa.denetimtaleptakip.model.Status;

@Service("commanFunctionsService")
public class CommanFunctionsService {

	private final static Logger LOGGER = LoggerFactory.getLogger(CommanFunctionsService.class);

	public Finding updatePeriod(Finding f) {

		try {
			Date  ldRevisedOrCompDate  =   f.getCompletionDate();
			LocalDate ldRevisedOrComp = new LocalDate(f.getCompletionDate());

			if (f.getRevisedCompletionDate() != null && f.getRevised()) {
				ldRevisedOrComp = new LocalDate(f.getRevisedCompletionDate());
				ldRevisedOrCompDate = f.getRevisedCompletionDate();
			}

			int total = 0;

			Date today = new Date();

			LocalDate ldToday = new LocalDate();

			Days totalDay = Days.daysBetween(ldRevisedOrComp, ldToday);

			total = Math.abs(totalDay.getDays());

			if (today.before(ldRevisedOrCompDate)) {

				f.setIsAfter(false);

			} else {
				f.setIsAfter(true);

			}

			if (total >= 0 && total <= 90)
				f.setCompareDates("0-3");
			else if (total > 90 && total <= 180)
				f.setCompareDates("3-6");
			else if (total > 180 && total <= 270)
				f.setCompareDates("6-9");
			else if (total > 270 && total <= 365)
				f.setCompareDates("9-12");
			else
				f.setCompareDates("+12");

		} catch (Exception e) {
			LOGGER.error("CommanFunctionsService.updatePeriod() -> " + e.getMessage());
		}

		return f;

	}

	public Finding updatePercentageOfFinding(Finding finding) {

		try {

			int numberOfRealActOwners = 0;
			int sumOfFindingActionCompletionPercentages = 0;

			for (FindingAction findingAction : finding.findingActionsAsList()) {

				if (findingAction.isActionOwnerFlag()) {
					numberOfRealActOwners++;
					sumOfFindingActionCompletionPercentages = sumOfFindingActionCompletionPercentages
							+ findingAction.getCompletionPercentage();
				}

			}

			try {
				finding.setCompPercentage(
						Integer.toString(sumOfFindingActionCompletionPercentages / numberOfRealActOwners));
			} catch (ArithmeticException e) {
				finding.setCompPercentage("0");
			}

		} catch (Exception e) {
			LOGGER.error("CommanFunctionsService.updatePercentageOfFinding() -> " + e.getMessage());
		}

		return finding;
	}

	public Finding updateStatus(Finding f) {

		try {
			Date today = new Date();

			Date compOrRevisedDate = prepareCompletionDate(f);

			if (Integer.parseInt(f.getCompPercentage()) == 100) {
				
				if (f.getConfirmed())
					f.setStatus(Status.CONFIRMED_COMPLETED);
				else
					f.setStatus(Status.UNCONFIRMED_COMPLETED);
			} else if (Integer.parseInt(f.getCompPercentage()) > 0 && Integer.parseInt(f.getCompPercentage()) < 100) {

				if (today.before(compOrRevisedDate))
					// today.before(f.getRevisedCompletionDate())
					f.setStatus(Status.IN_PROGRESS);
				else
					f.setStatus(Status.DELAYED);
			} else if (Integer.parseInt(f.getCompPercentage()) == 0) {

				if (today.before(compOrRevisedDate)) {
					f.setStatus(Status.NOT_STARTED);

				} else
					f.setStatus(Status.DELAYED);
			}
		} catch (Exception e) {
			LOGGER.error("CommanFunctionsService.updateStatus() -> " + e.getMessage());
		}

		return f;
	}

	private Date prepareCompletionDate(Finding finding) {
		
		Date findingCompletionDate;
		if (finding.getRevised() && finding.getRevisedCompletionDate() != null) {
			findingCompletionDate =finding.getRevisedCompletionDate();
		} else {
			findingCompletionDate = finding.getCompletionDate();
		}
		return findingCompletionDate;
	}

}
