package com.tr.cimsa.denetimtaleptakip.controller.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.RandomAccess;

public class AsList {

	public static <E> List<E> asList(Collection<E> c, int size) {
		if (c instanceof List && c instanceof RandomAccess)
			return (List<E>) c;

		if (c.isEmpty())
			return Collections.emptyList();

		return new ArrayList<E>(c);
	}

	public static <E> List<E> asList(Collection<E> c) {
		return asList(c, c.size());
	}

}
