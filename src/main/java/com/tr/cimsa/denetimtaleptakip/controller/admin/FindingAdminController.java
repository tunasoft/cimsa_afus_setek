package com.tr.cimsa.denetimtaleptakip.controller.admin;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.chart.PieChartModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;
import com.tr.cimsa.denetimtaleptakip.controller.user.DepartmentFindingsController;
import com.tr.cimsa.denetimtaleptakip.controller.util.FacesUtils;
import com.tr.cimsa.denetimtaleptakip.model.BusinessProcessType;
import com.tr.cimsa.denetimtaleptakip.model.Finding;
import com.tr.cimsa.denetimtaleptakip.model.FindingAction;
import com.tr.cimsa.denetimtaleptakip.model.FindingFile;
import com.tr.cimsa.denetimtaleptakip.model.FindingResponsibleAction;
import com.tr.cimsa.denetimtaleptakip.model.Status;
import com.tr.cimsa.denetimtaleptakip.model.User;
import com.tr.cimsa.denetimtaleptakip.services.FindingExcelService;
import com.tr.cimsa.denetimtaleptakip.services.FindingService;
import com.tr.cimsa.denetimtaleptakip.services.MailService;

import lombok.Getter;
import lombok.Setter;

/**
 * User: mhazer Date: 8/20/12 Time: 2:28 PM
 */
@ManagedBean(name = "findingAdminController")
@ViewScoped
public class FindingAdminController implements Serializable {
	private final static Logger logger = LoggerFactory.getLogger(FindingAdminController.class);

	@ManagedProperty("#{findingService}")
	private FindingService findingService;

	@ManagedProperty("#{findingExcelService}")
	private FindingExcelService findingExcelService;

	@ManagedProperty("#{mailService}")
	@Setter
	private MailService mailService;

	@ManagedProperty("#{mailService}")
	@Setter
	private MailService mailServiceTest;

	@Getter
	@Setter
	private Finding selectedFinding;

	@Getter
	@Setter
	private FindingAction selectedFindingAction;

	@Getter
	@Setter
	private List<FindingAction> selectedFindingActions;

	@Getter
	@Setter
	private FindingResponsibleAction selectedFindingResponsibleAction;

	@Getter
	private PieChartModel pieChartModel;

	@Getter
	private List<Finding> findings;

	@Getter
	@Setter
	private List<User> currentActionOwners;

	@ManagedProperty("#{visit.user}")
	@Setter
	private User user;

	@Getter
	@Setter
	private String[] selectedActionOwners;

	@Getter
	@Setter
	private List<Finding> filteredFindings;

	@ManagedProperty("#{departmentFindingsController}")
	@Setter
	private DepartmentFindingsController departmentFindingController;

	@Getter
	private FindingAdminLazyDataModel findingLazyDataModel;

	@ManagedProperty("#{commanFunctionsService}")
	@Setter
	private CommanFunctionsService commanFunctionsService;

	@PostConstruct
	public void init() {
		findingLazyDataModel = new FindingAdminLazyDataModel(findingService,commanFunctionsService);
	}

	public void startMailServis(Finding finding) {
		mailService.executeEnerjisaEmailStrategyForStartMailService(finding);
	}

	@ManagedProperty("#{findingFileController}")
	@Setter
	private FindingFileController findingFileController;

	public void selectFinding(Finding finding) {
		this.selectedFinding = finding;
		this.selectedFindingAction = new FindingAction();
		this.selectedFindingAction.setActionTaken("");
		this.selectedFindingAction.setFinding(selectedFinding);
		this.selectedFindingResponsibleAction = null;
	}

	public void selectFindingForConfirmation(Finding finding) {

		this.selectedFinding = finding;
		this.selectedFindingResponsibleAction = new FindingResponsibleAction();
	}

	public void selectFindingForEditUserActions(Finding finding) { // seçim yapılmış
		this.selectedFinding = finding;
		this.selectedFindingAction = new FindingAction();
		this.selectedFindingAction.setActionTaken("");
		this.selectedFindingAction.setFinding(selectedFinding);
		this.selectedFindingActions = finding.findingActionsAsList();

		List<FindingAction> list = finding.getFindingActions();
		currentActionOwners = new ArrayList<>();

		for (FindingAction findingAction : list) {
			if (findingAction.isActionOwnerFlag()) {
				currentActionOwners.add(findingAction.getActionOwner());
			}
		}

	}

	public void action() throws IOException {
		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		ec.redirect(((HttpServletRequest) ec.getRequest()).getRequestURI());
	}

	public void selectFindingAction(FindingAction findingAction) {
		findingAction.setHistoryOfFindingAction(this.findingService.retrieveHistoryOfFindingAction(findingAction.getId()));
		this.selectedFindingAction = findingAction;
		List<FindingResponsibleAction> fr = findingAction.getFinding().findingResponsibleActionsAsList();
//		this.selectedFindingAction = this.findingService.retrieveFindingActionWithHistory(findingAction.getId());
//		this.selectedFinding = this.selectedFindingAction.getFinding();
	}

	public void selectFindingResponsibleAction(FindingResponsibleAction findingResponsibleAction) {
		findingResponsibleAction.setHistoryOfFindingResponsibleAction(this.findingService.retrieveHistoryOfFindingResponsibleAction(findingResponsibleAction.getId()));
		this.selectedFindingResponsibleAction = findingResponsibleAction;
	}

	private boolean isFindingResponsibleActionExistsForUser(User _user) {
		final List<FindingResponsibleAction> findingResponsibleActions = this.getSelectedFinding().findingResponsibleActionsAsList();
		for (FindingResponsibleAction findingResponsibleAction : findingResponsibleActions) {
			if (findingResponsibleAction.getActionOwner().getId().equals(_user.getId())) {
				return true;
			}
		}
		return false;
	}

	public void updateFindingResponsibleAction() {
		boolean addNewFindingResponsibleUserFlag = false;
		try {
			if (isUserResponsibleForFinding(this.selectedFinding, user)) {

				if (this.selectedFindingResponsibleAction.getRevisedCompletionDate() != null && selectedFinding.getRevisedCompletionDate() != null
						&& this.selectedFindingResponsibleAction.getRevisedCompletionDate().getDate() == selectedFinding.getRevisedCompletionDate().getDate()) {

					FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, "You set revisedDate with the same Revised Date as before", null);
				}
				checkResponsibleFindingActionValues();
				if (this.selectedFinding.isFindingResponsibleActionExistsForUser(this.user)) {
					editFindingResponsibleAction();
				} else {
					addNewFindingResponsibleUser();
					addNewFindingResponsibleUserFlag = true;
				}

				selectedFinding = findingService.retrieveFinding(selectedFinding.getId());

				if (addNewFindingResponsibleUserFlag) {
					String responsibleOwners = selectedFinding.getResponsibleOwners();

					responsibleOwners = responsibleOwners != null ? responsibleOwners += "\n" + user.getNameSurname() : user.getNameSurname();

					selectedFinding.setResponsibleOwners(responsibleOwners);
				}

				this.selectedFinding.setRevised(false);

				if (this.selectedFindingResponsibleAction.getRevisedCompletionDate() != null) {

					this.selectedFinding.setRevised(true);

//					this.selectedFinding
//							.setCompletionDate(this.selectedFindingResponsibleAction.getRevisedCompletionDate());

					this.selectedFinding.setRevisedCompletionDate(this.selectedFindingResponsibleAction.getRevisedCompletionDate());

				} else if (this.selectedFinding.getRevisedCompletionDate() != null && this.selectedFindingResponsibleAction.getRevisedCompletionDate() == null) {

					this.selectedFinding.setRevised(true);

				} else {
					this.selectedFinding.setRevised(false);
				}

				for (FindingAction findingAction : this.selectedFinding.findingActionsAsList()) {

					if (findingAction.isActionOwnerFlag()) {

						findingAction.setCompletionPercentage(

								this.selectedFindingResponsibleAction.getCompletionPercentage());

						findingAction.setActionTaken(user.getNameSurname() + " tarafından confirm edilmiştir.");

						findingAction.setChangeDate(new Date());
					}
				}

				selectedFinding = commanFunctionsService.updatePercentageOfFinding(this.selectedFinding);

				if (selectedFinding.getCompletionPercentage() == 100) {

					selectedFinding.setConfirmed(true);
				}

				selectedFinding = commanFunctionsService.updateStatus(this.selectedFinding);

				selectedFinding = commanFunctionsService.updatePeriod(this.selectedFinding);

				selectedFinding = this.findingService.mergeFinding(this.selectedFinding);

				// this.sendUpdateEmails(this.selectedFindingAction);

			} else {
				FacesUtils.addMessage(FacesMessage.SEVERITY_ERROR, "You are not responsible owner for this finding", null);
			}

		} catch (Exception exception) {
			FacesUtils.addMessage(FacesMessage.SEVERITY_ERROR, "Finding status update failed: (" + exception.getMessage() + ")", null);
		} finally {
			this.selectedFindingResponsibleAction = null;
			this.init();
		}
	}

	private void addNewFindingResponsibleUser() {
		if (isUserResponsibleForFinding(this.selectedFinding, user)) {
			FindingResponsibleAction findingResponsibleAction = new FindingResponsibleAction();
			findingResponsibleAction.setChangeDate(new Date());
			findingResponsibleAction.setActionTaken(this.selectedFindingResponsibleAction.getActionTaken());
			findingResponsibleAction.setCompletionPercentage(this.selectedFindingResponsibleAction.getCompletionPercentage());
			findingResponsibleAction.setExplanationForDelay(this.selectedFindingResponsibleAction.getExplanationForDelay());
			findingResponsibleAction.setRevisedCompletionDate(this.selectedFindingResponsibleAction.getRevisedCompletionDate());
			findingResponsibleAction.setFinding(this.selectedFinding);
			findingResponsibleAction.setActionOwner(this.user);

			this.findingService.mergeFindingResponsibleAction(findingResponsibleAction);

			FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, "Finding status updated successfully", null);
		} else {
			FacesUtils.addMessage(FacesMessage.SEVERITY_ERROR, "You are not responsible owner for this finding", null);
		}
	}

	private void editFindingResponsibleAction() {
		if (isUserResponsibleForFinding(this.selectedFinding, user)) {

			FindingResponsibleAction findingResponsibleAction = this.selectedFinding.findingResponsibleActionOfUser(this.user);
			findingResponsibleAction.setChangeDate(new Date());

			findingResponsibleAction.setActionTaken(this.selectedFindingResponsibleAction.getActionTaken());
			findingResponsibleAction.setCompletionPercentage(this.selectedFindingResponsibleAction.getCompletionPercentage());
			findingResponsibleAction.setExplanationForDelay(this.selectedFindingResponsibleAction.getExplanationForDelay());
			findingResponsibleAction.setRevisedCompletionDate(this.selectedFindingResponsibleAction.getRevisedCompletionDate());
			findingResponsibleAction.setFinding(this.selectedFinding);
			findingResponsibleAction.setActionOwner(this.user);
			this.findingService.mergeFindingResponsibleAction(findingResponsibleAction);

			FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, "Finding status updated successfully", null);
		} else {
			FacesUtils.addMessage(FacesMessage.SEVERITY_ERROR, "You are not responsible owner for this finding anymore", null);
		}
	}

	private boolean isUserResponsibleForFinding(Finding selectedFinding, User user) {
		for (BusinessProcessType respt : user.getResponsibleBusinessTypes()) {
			String x = "descrps:" + respt.getDescription();
		}
		String y = "selected" + selectedFinding.getBusinessProcessType().getDescription();
		return user.getResponsibleBusinessTypes().contains(selectedFinding.getBusinessProcessType()) || user.getResponsibleBusinessSubTypes().contains(selectedFinding.getBusinessSubProcessType());
	}

	private void checkResponsibleFindingActionValues() {
		if (this.selectedFindingResponsibleAction.getCompletionPercentage() != 100) {
			if (this.selectedFindingResponsibleAction.getRevisedCompletionDate() != null && this.selectedFindingResponsibleAction.getRevisedCompletionDate().before(new Date())) {
				// throw new IllegalArgumentException("You need to choose revised date after
				// today");
			}
		}
	}

	public String updateFinding(Finding finding) {
		return "/admin/editFinding.xhtml?faces-redirect=true&findingId=" + finding.getId();
	}

	public void reviseFinding(Finding finding) {
		if (isUserResponsibleForFinding(finding, user)) {
			finding.setRevised(true);
			finding = commanFunctionsService.updateStatus(finding);
			finding = commanFunctionsService.updatePeriod(finding);
			this.findingService.mergeFinding(finding);
			FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, "Revised completion date is approved successfully", null);
		} else {
			FacesUtils.addMessage(FacesMessage.SEVERITY_ERROR, "You are not responsible owner for this finding", null);

		}
	}

	public void rejectFinding(Finding finding) {
		if (isUserResponsibleForFinding(finding, user)) {
			finding.setRevised(true);
			finding.setRevisedCompletionDate(null);
			this.findingService.mergeFinding(finding);
			FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, "Revised completion date is rejected", null);
		} else {
			FacesUtils.addMessage(FacesMessage.SEVERITY_ERROR, "You are not responsible owner for this finding", null);

		}
	}

	public String reassignFinding(Finding finding) {
		return "/admin/reassignFinding.xhtml?faces-redirect=true&findingId=" + finding.getId();
	}

	public void exportFindingsToExcel(List<Finding> findings) {

		logger.debug("started to prepare excel {}", findings.size());
		Workbook workbook = this.findingExcelService.prepareExcelReportOfTheFindings(findings, user);
		logger.debug("workbook size is {}", workbook.getSheet("Findings").getLastRowNum());
		sendFileToBrowser(workbook);
	}

	public String sendFileToBrowser(Workbook workbook) {
		try {
			String filename = "findings.xls";

			FacesContext facesContext = FacesContext.getCurrentInstance();
			ExternalContext externalContext = facesContext.getExternalContext();
			externalContext.setResponseContentType("application/vnd.ms-excel");

			// http://stackoverflow.com/questions/3592058/how-to-send-byte-as-pdf-to-browser-in-java-web-application
			// attachment (pops up a "Save As" dialogue) or inline (let the web browser
			// handle the display itself)
			externalContext.setResponseHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");

			workbook.write(externalContext.getResponseOutputStream()); // get workbook
			facesContext.responseComplete(); // if I don't call responseComplete() => IllegalStateException

			return null; // remain on same page
		} catch (Exception e) {
			// handle exception...
			return null; // remain on same page
		}
	}

	public void preparePieChart(List<Finding> findings) {

		logger.debug("preparing pie chart");
		try {
			pieChartModel = NumberOfFindingsPerStatusGraphicalReportController.preparePieChart(findings);
			pieChartModel.setShowDatatip(true);
			pieChartModel.setShowDataLabels(true);
			pieChartModel.setDataFormat("value");
			pieChartModel.setResetAxesOnResize(true);
			pieChartModel.setLegendPosition("ne");
		} catch (Exception exception) {
			exception.printStackTrace();
			FacesUtils.addMessage(FacesMessage.SEVERITY_ERROR, "We couldn't prepare the graph of findings" + exception.getMessage(), null);
		}

	}

	public void setFindingService(FindingService findingService) {
		this.findingService = findingService;
	}

	public void setFindingExcelService(FindingExcelService findingExcelService) {
		this.findingExcelService = findingExcelService;
	}

	public void deleteFinding(Finding finding) {
		logger.debug("preparing to delete finding");
		try {
			findingService.deleteFinding(finding);
			FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, "Finding deleted successfully", null);
		} catch (Exception exception) {
			exception.printStackTrace();
			FacesUtils.addMessage(FacesMessage.SEVERITY_ERROR, "Error occured while deleting finding" + exception.getMessage(), null);
		}
	}

	public void updateFindingActions() throws Exception { // edit as user ile buraya geliniyor
		boolean atLeastOneActionOwnerFilled = !isAtLeastOneActionOwnerSelected();
		try {
			Preconditions.checkArgument(atLeastOneActionOwnerFilled, "In order to perform update you should select at least one action owner!");
		} catch (IllegalArgumentException e) {
			System.err.println(e.getMessage());
			FacesUtils.addMessage(FacesMessage.SEVERITY_ERROR, "In order to perform update you should select at least one action owner!", null);
			e.printStackTrace();
			return;
		}
		try {
			List<FindingAction> editedFindingActions = new ArrayList<>();

			for (FindingAction action : this.selectedFindingActions) {
				for (String myUser : selectedActionOwners) {

					if ((!action.isReadOnly()) && (myUser.equalsIgnoreCase(action.getActionOwner().getNameSurname()))) {
						action.setActionTaken("[" + user.getNameSurname() + " tarafından] " + this.selectedFindingAction.getActionTaken());
						action.setCompletionPercentage(this.selectedFindingAction.getCompletionPercentage());
						action.setExplanationForDelay(this.selectedFindingAction.getExplanationForDelay());
						action.setRevisedCompletionDate(this.selectedFindingAction.getRevisedCompletionDate());
						action.setChangeDate(new Date());
						editedFindingActions.add(action);
					}

				}

			}

			if (!findingFileController.getFindingFiles().isEmpty() && findingFileController.getFinding() != null) {
				findingService.removeFindingFile(findingFileController.getFindingFiles(), findingFileController.getFinding());
			}

			if (this.selectedFindingAction.getRevisedCompletionDate() != null) {
				this.selectedFinding.setRevisedCompletionDate(this.selectedFindingAction.getRevisedCompletionDate());
			}

			if (this.selectedFinding.getRevisedCompletionDate() != null && this.selectedFindingAction.getRevisedCompletionDate() != null
					&& this.selectedFindingAction.getRevisedCompletionDate().getTime() != this.selectedFinding.getRevisedCompletionDate().getTime()) {

				this.selectedFinding.setRevised(false);
			}

			if (this.selectedFinding.getRevised() == true && this.selectedFinding.getRevisedCompletionDate() == null && this.selectedFindingAction.getRevisedCompletionDate() != null) {
				this.selectedFinding.setRevised(false);
			}



			this.selectedFinding = commanFunctionsService.updatePercentageOfFinding(this.selectedFinding); // burada bir action güncellenince buraya
			this.selectedFinding = commanFunctionsService.updateStatus(this.selectedFinding);
			this.selectedFinding = commanFunctionsService.updatePeriod(selectedFinding);
			this.selectedFinding = this.findingService.mergeFinding(this.selectedFinding);



			sendUpdateEmailForEditedFindings(selectedFinding);

			FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, "Finding status is updated successfully", null);

		} catch (Exception exception) {
			exception.printStackTrace();
			FacesUtils.addMessage(FacesMessage.SEVERITY_ERROR, "Finding status update is failed: (" + exception.getMessage() + ")", null);
		} finally {
			this.selectedFindingAction = null;
			this.selectedFinding = null;
			this.init();
			this.departmentFindingController.init();
		}
	}

	public boolean isAtLeastOneActionOwnerSelected() {
		return CollectionUtils.sizeIsEmpty(selectedActionOwners);
	}



	private void sendUpdateEmailForEditedFindings(Finding finding) {
		this.mailService.sendUpdateEmailForEditetFinding(finding);
	}



	public void handleFileUpload(FileUploadEvent fileUploadEvent) {
		findingFileController.handleFileUpload(fileUploadEvent.getFile(), this.selectedFinding, this.user);
	}

	public void removeFile(FindingFile findingFile, Finding finding) {
		finding.getFindingFiles().remove(findingFile);
	}

	public boolean renderForEditAsUser() {

		return true;// change
	}

	public void startDepartmentMail() {

		System.out.println("startDepartmentMail started");
		mailServiceTest.sendInformingEmails();
	}

}
