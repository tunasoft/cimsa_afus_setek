package com.tr.cimsa.denetimtaleptakip.controller.admin;

import com.tr.cimsa.denetimtaleptakip.controller.Visit;
import com.tr.cimsa.denetimtaleptakip.controller.util.CommonDefinitionsController;
import com.tr.cimsa.denetimtaleptakip.services.FindingService;

import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.bean.ManagedProperty;

/**
 * User: mhazer
 * Date: 8/30/12
 * Time: 12:51 PM
 */
public abstract class BaseAdminController
{
    protected final Logger logger = LoggerFactory.getLogger(getClazz());

    @ManagedProperty("#{commonDefinitionsController}")
    @Setter
    protected CommonDefinitionsController commonDefinitionsController;

    @ManagedProperty("#{findingService}")
    @Setter
    protected FindingService findingService;

    @ManagedProperty("#{visit}")
    @Setter
    protected Visit visit;

    protected abstract Class getClazz();

}
