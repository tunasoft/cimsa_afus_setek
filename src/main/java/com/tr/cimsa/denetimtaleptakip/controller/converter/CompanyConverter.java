package com.tr.cimsa.denetimtaleptakip.controller.converter;

import org.apache.commons.lang.StringUtils;

import com.tr.cimsa.denetimtaleptakip.model.Company;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import java.util.List;

/**
 * Company: mhazer
 * Date: 8/20/12
 * Time: 8:43 AM
 */
@FacesConverter(value = "companyConverter")
public class CompanyConverter implements Converter
{
    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String id)
    {
        List<Company> companys = (List<Company>) uiComponent.getAttributes().get("companyEntities");
        if (companys != null)
        {
            for (Company company : companys)
            {
                if (company.getId().toString().equals(id))
                {
                    return company;
                }
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o)
    {
        Company company = (Company) o;
        return company != null ? company.getId().toString() : StringUtils.EMPTY;
    }
}
