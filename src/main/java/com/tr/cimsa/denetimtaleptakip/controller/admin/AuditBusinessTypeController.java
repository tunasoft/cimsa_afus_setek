package com.tr.cimsa.denetimtaleptakip.controller.admin;

import lombok.Getter;
import org.apache.commons.collections.CollectionUtils;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import com.tr.cimsa.denetimtaleptakip.model.AuditType;
import com.tr.cimsa.denetimtaleptakip.model.BusinessProcessType;
import com.tr.cimsa.denetimtaleptakip.model.BusinessSubProcessType;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;

/**
 * User: mhazer
 * Date: 8/29/12
 * Time: 4:38 PM
 */
@ManagedBean(name = "auditBusinessTypeController")
@ViewScoped
public class AuditBusinessTypeController extends BaseAdminController implements Serializable
{
    @Getter
    private TreeNode rootNode;

    @PostConstruct
    public void init()
    {
        rootNode = new DefaultTreeNode("Root", null);
        rootNode.setExpanded(true);
        for (AuditType auditType : this.findingService.retrieveAuditTypes())
        {
            TreeNode auditTypeNode = new DefaultTreeNode(auditType, rootNode);
            auditTypeNode.setExpanded(true);
            if (CollectionUtils.isNotEmpty(auditType.getBusinessProcessTypes()))
            {
                for (BusinessProcessType businessProcessType : auditType.getBusinessProcessTypes())
                {
                    TreeNode businessTypeNode = new DefaultTreeNode(businessProcessType, auditTypeNode);
                    businessTypeNode.setExpanded(true);
                    if (CollectionUtils.isNotEmpty(businessProcessType.getSubProcessTypes()))
                    {
                        for (BusinessSubProcessType businessSubProcessType : businessProcessType.getSubProcessTypes())
                        {
                            TreeNode businessSubTypeNode = new DefaultTreeNode(businessSubProcessType, businessTypeNode);
                        }
                    }
                }
            }
        }
    }

    @Override
    protected Class getClazz()
    {
        return AuditBusinessTypeController.class;
    }
}
