package com.tr.cimsa.denetimtaleptakip.controller.converter;

import com.google.common.collect.Lists;
import com.tr.cimsa.denetimtaleptakip.model.BaseDefinitionEntity;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Set;

/**
 * User: mhazer
 * Date: 8/19/12
 * Time: 6:15 PM
 */
@FacesConverter(value = "entityConverter")
public class EntityConverter implements Converter
{
	@Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String name)
    {
		
        List<BaseDefinitionEntity> definitionEntities = null;
        Object object = uiComponent.getAttributes().get("entityList");
        if(object instanceof List)
        {
            definitionEntities = (List)object;   
        }
        else if(object instanceof Set)
        {
            definitionEntities = Lists.newLinkedList((Set)object);
        }
        try {
        	 for(BaseDefinitionEntity baseDefinitionEntity : definitionEntities)
             {
        		 if(baseDefinitionEntity.getDescription().toString().equals(name))
                 {
                     return baseDefinitionEntity;
                 }
             }
		} catch (Exception e) {
			e.printStackTrace();
		}
       
        return null;
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object object)
    {
        try
        {
            Method method = object.getClass().getMethod("getDescription");
            return (String) method.invoke(object);
        }
        catch (Exception e)
        {
            return null;
        }
    }
}
