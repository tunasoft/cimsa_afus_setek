package com.tr.cimsa.denetimtaleptakip.controller.user;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tr.cimsa.denetimtaleptakip.controller.Visit;
import com.tr.cimsa.denetimtaleptakip.controller.admin.SearchParameters;
import com.tr.cimsa.denetimtaleptakip.model.Finding;
import com.tr.cimsa.denetimtaleptakip.model.FindingAction;
import com.tr.cimsa.denetimtaleptakip.model.User;
import com.tr.cimsa.denetimtaleptakip.services.FindingService;


public class FindingActionsLazyDataModel extends LazyDataModel<FindingAction> {
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4448766984600048348L;
	private final  Logger logger = LoggerFactory.getLogger(FindingActionsLazyDataModel.class);

	private FindingService findingService;
	
	private Visit visit;

	private List<FindingAction> findingActions ;
	
	
	private List<Finding> findings; 
	
	
	public FindingActionsLazyDataModel(FindingService findingService, Visit visit) {
		this.findingService = findingService;
		this.visit = visit;
	}


	@Override
	public List<FindingAction> load(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) {

		long start = System.currentTimeMillis();

		int totalDataSize = 0;
		
		
		if (filters != null && !filters.isEmpty()) {
			Map<String, Object> paramMaps = new LinkedHashMap<>();

			for (String key : filters.keySet()) {

				Object value = filters.get(key);
				
				key = key.equals("finding.getStatus().getDescription()") ? "finding.status" :key;
				key = key.equals("finding.findingYear") ? "FINDING_YEAR" : key;
				key = key.equals("finding.revisedCompletionDate") ? "REVISED_COMP_DATE" : key;

				if (key.equals("finding.status")) {

					value = value.equals("Overdue") ? "DELAYED" : value;
					value = value.equals("Unconfirmed-Completed") ? "UNCONFIRMED_COMPLETED" : value;
					value = value.equals("Confirmed-Completed") ? "CONFIRMED_COMPLETED" : value;
					value = value.equals("Not Started") ? "NOT_STARTED" : value;
					value = value.equals("In Progress") ? "IN_PROGRESS" : value;

					paramMaps.put(key, value);
				} else {
					paramMaps.put(key, value);
				}

			}

			List<FindingAction> filteredList = new ArrayList<>();
			
			filteredList = findingService.retrieveUserFindingActionsByParam(paramMaps, first, pageSize, sortField, sortOrder, visit.getUser());

			totalDataSize = findingService.retrieveUserFindingActionsCountByParam(paramMaps, visit.getUser()).intValue();
			this.setRowCount(totalDataSize);
			
			findingActions = filteredList;
			
//			findingActions = filterFindingActions(findingActions);
			
			return findingActions;

		}
		
		
		
		totalDataSize = findingService.retrieveUserFindingActionsCount(visit.getUser()).intValue();
				
	    setRowCount(totalDataSize);

		this.setRowCount(totalDataSize);		
		
		findingActions = findingService.retrieveUserFindingActions(first, pageSize, sortField, sortOrder, visit.getUser());
		
//		findingActions = filterFindingActions(findingActions);
		
		return findingActions;
	}
	
	
//	private List<FindingAction> filterFindingActions(List<FindingAction> findingActions) {
//
//		try {
//
//			// Select distinct(FINDING_ID) from DT_RT_FINDING_ACTIONS_TABLE where
//			// ACTION_OWNER_ID = 100;
//
//			for (int i = 0; i < findingActions.size(); i++) {
//				if (!findingActions.get(i).isSpoc() && !findingActions.get(i).isReadOnly()) {
//					findingActions.get(i).setActionOwnerFlag(Boolean.TRUE);
//				}
//				for (int j = 0; j < findingActions.size(); j++) {
//					if (i != j && findingActions.get(i).getFinding().getFindingNumber()
//							.equals(findingActions.get(j).getFinding().getFindingNumber())) {
//						if (findingActions.get(i).isSpoc() && findingActions.get(j).isReadOnly()) {
//							findingActions.get(i).setReadOnly(Boolean.TRUE);
//							findingActions.get(i).setActionTaken("Read Only User and SPOC");
//
//						}
//						if (findingActions.get(i).isSpoc() && findingActions.get(j).isActionOwnerFlag()) {
//							findingActions.get(i).setActionOwnerFlag(Boolean.TRUE);
//							findingActions.get(i).setActionTaken("Action Owner and SPOC");
//
//						}
//					}
//				}
//			}
//
//			// ----- LinkedHashMap<findingNumber, HashMap<UserId, FindingAction>>
//
//			LinkedHashMap<String, HashMap<Integer, FindingAction>> torba = new LinkedHashMap<>();
//			HashMap<Integer, FindingAction> showOneFindingAct;
//			for (FindingAction findingAction : findingActions) {
//
//				int userId = findingAction.getActionOwner().getId();
//				String findingNmber = findingAction.getFinding().getFindingNumber();
//
//				showOneFindingAct = torba.get(findingNmber) != null ? torba.get(findingNmber) : new HashMap<>();
//
//				if (findingAction.isSpoc()) {
//
//					showOneFindingAct.put(userId, findingAction);
//					torba.put(findingNmber, showOneFindingAct);
//					continue;
//				}
//
//				if (!showOneFindingAct.containsKey(userId) && findingAction.isActionOwnerFlag()) {
//
//					showOneFindingAct.put(userId, findingAction);
//					torba.put(findingNmber, showOneFindingAct);
//					continue;
//				}
//
//				if (!showOneFindingAct.containsKey(userId) && findingAction.isReadOnly()) {
//
//					showOneFindingAct.put(userId, findingAction);
//					torba.put(findingNmber, showOneFindingAct);
//					continue;
//				}
//
//			}
//
//			List<FindingAction> myFindActs = new ArrayList<>();
//
//			for (String key : torba.keySet()) {
//
//				myFindActs.addAll(torba.get(key).values());
//
//			}
//
//			findingActions = myFindActs; // hem spoc hem act own için liste düzenlendi
//
//			findings = new ArrayList<>();
//
//			for (FindingAction findingAction : myFindActs) {
//				findings.add(findingAction.getFinding());
//			}
//
//			return findingActions;
//
//		} catch (Exception e) {
//			logger.debug("FindingActionsLazyDataModel.filterFindingActions() e -> " + e);
//		}
//
//		return null;
//
//	}


	public List<FindingAction> getFindingActions() {
		logger.debug("FindingActionsLazyDataModel.getFindingActions()");
		return findingActions;
	}


	public List<Finding> getFindings() {
		
		List<User> users = new ArrayList<>();
		users.add(visit.getUser());
		
		SearchParameters searchParameters = new SearchParameters();
		searchParameters.setUsers(users);
		
		findings = findingService.searchFindings(searchParameters);
	
		
		for (Iterator<Finding> fin  = findings.iterator(); fin.hasNext();) {
		
			Finding finding = fin.next();
			boolean flag = false;
			
			for (FindingAction findingAction : findingActions) {

				if (finding.getId().equals(findingAction.getFinding().getId())) {
					flag = true;
					break;
				}
			}
			
			if (!flag) {
				fin.remove();
			}
		}
		
		return findings;
	}
	
}
