package com.tr.cimsa.denetimtaleptakip.controller.admin;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import lombok.Getter;
import lombok.Setter;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.joda.time.LocalDate;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;
import com.tr.cimsa.denetimtaleptakip.controller.util.FacesUtils;
import com.tr.cimsa.denetimtaleptakip.controller.util.FindingImportWrapper;
import com.tr.cimsa.denetimtaleptakip.model.AuditType;
import com.tr.cimsa.denetimtaleptakip.model.BusinessProcessType;
import com.tr.cimsa.denetimtaleptakip.model.BusinessSubProcessType;
import com.tr.cimsa.denetimtaleptakip.model.Company;
import com.tr.cimsa.denetimtaleptakip.model.Department;
import com.tr.cimsa.denetimtaleptakip.model.Finding;
import com.tr.cimsa.denetimtaleptakip.model.FindingAction;
import com.tr.cimsa.denetimtaleptakip.model.Priority;
import com.tr.cimsa.denetimtaleptakip.model.User;
import com.tr.cimsa.denetimtaleptakip.services.FindingExcelService;
import com.tr.cimsa.denetimtaleptakip.services.FindingService;
import com.tr.cimsa.denetimtaleptakip.services.MailService;

@ManagedBean(name = "importFindingController")
@ViewScoped
public class ImportFindingController implements Serializable {
	private final static Logger LOGGER = LoggerFactory.getLogger(ImportFindingController.class);

	private final static int MAXIMUM_ROW_SIZE = 51;

	@ManagedProperty("#{findingService}")
	@Setter
	private FindingService findingService;

	@ManagedProperty("#{visit.user}")
	@Setter
	private User user;
	
	@ManagedProperty("#{mailService}")
	@Setter
	private MailService mailService;

	@ManagedProperty("#{commonDefinitionsController.activeUsers}")
	@Setter
	private List<User> users;

	@ManagedProperty("#{findingExcelService}")
	@Setter
	private FindingExcelService findingExcelService;

	@Getter
	private List<FindingImportWrapper> findingImportWrapperList;
	
	@ManagedProperty("#{commanFunctionsService}")
	@Setter
	private CommanFunctionsService commanFunctionsService;

	@PostConstruct
	public void init() {
		findingImportWrapperList = new ArrayList<FindingImportWrapper>();
	}

	public void handleFileUpload(FileUploadEvent fileUploadEvent) {
		final UploadedFile uploadedFile = fileUploadEvent.getFile();

		try {
			Workbook workbook = WorkbookFactory.create(uploadedFile.getInputstream());
			Sheet sheet = workbook.getSheetAt(0);
			if (sheet != null) {
				if (sheet.getPhysicalNumberOfRows() <= MAXIMUM_ROW_SIZE) {
					parseSheet(sheet);
				} else {
					FacesUtils.addMessage(FacesMessage.SEVERITY_ERROR,
							"Total finding size is exceeded. (Maximum 50 rows are allowed per file)",
							"Total finding size is exceeded. (Maximum 50 rows are allowed per file)");
				}
			}
		} catch (IOException exception) {
			LOGGER.error("IOException is occured:", exception.getMessage());
			FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, "IOException is occured:" + exception.getMessage(), null);
		} catch (InvalidFormatException exception) {
			LOGGER.error("InvalidFormatException is occured:", exception.getMessage());
			FacesUtils.addMessage(FacesMessage.SEVERITY_INFO,
					"InvalidFormatException is occured:" + exception.getMessage(), null);
		}
	}

	private void parseSheet(Sheet sheet) {
		for (Row row : sheet) {
			final int rowNum = row.getRowNum();
			if (rowNum > 0) {
				parseRow(row);
			}
		}
	}

	private String getStringCellValue(Cell cell) {
		String cellValue = "";

		cell.setCellType(Cell.CELL_TYPE_STRING);
		cellValue = cell.getStringCellValue();

		return cellValue;
	}

	private void parseRow(Row row) {
		int i = 0;
		final String companyString = getStringCellValue(row.getCell(i++, Row.CREATE_NULL_AS_BLANK));
		final String departmentString = getStringCellValue(row.getCell(i++, Row.CREATE_NULL_AS_BLANK));
		final String auditTypeString = getStringCellValue(row.getCell(i++, Row.CREATE_NULL_AS_BLANK));
		final String mainProcessString = getStringCellValue(row.getCell(i++, Row.CREATE_NULL_AS_BLANK));
		final String subProcessString = getStringCellValue(row.getCell(i++, Row.CREATE_NULL_AS_BLANK));
		final String findingNoString = getStringCellValue(row.getCell(i++, Row.CREATE_NULL_AS_BLANK));
		final String findingSummary = getStringCellValue(row.getCell(i++, Row.CREATE_NULL_AS_BLANK));
		final String findingYearString = getStringCellValue(row.getCell(i++, Row.CREATE_NULL_AS_BLANK));
		final String reportNameString = getStringCellValue(row.getCell(i++, Row.CREATE_NULL_AS_BLANK));
		final String locationString = getStringCellValue(row.getCell(i++, Row.CREATE_NULL_AS_BLANK));
		final String findingTitleString = getStringCellValue(row.getCell(i++, Row.CREATE_NULL_AS_BLANK));
		final String priorityString = getStringCellValue(row.getCell(i++, Row.CREATE_NULL_AS_BLANK));
		final String detailString = getStringCellValue(row.getCell(i++, Row.CREATE_NULL_AS_BLANK));
		final String riskString = getStringCellValue(row.getCell(i++, Row.CREATE_NULL_AS_BLANK));
		final String recommendationString = getStringCellValue(row.getCell(i++, Row.CREATE_NULL_AS_BLANK));
		final String manActionPlanString = getStringCellValue(row.getCell(i++, Row.CREATE_NULL_AS_BLANK));
		final String actionOwnersString = getStringCellValue(row.getCell(i++, Row.CREATE_NULL_AS_BLANK));
		final Cell completionDateCell = row.getCell(i++, Row.CREATE_NULL_AS_BLANK);
		final String spocsString = getStringCellValue(row.getCell(i++, Row.CREATE_NULL_AS_BLANK));

		Boolean hasAnything = Boolean.FALSE;
		for (int j = 0; j < i; j++) {
			Cell cell = row.getCell(j, Row.CREATE_NULL_AS_BLANK);
			if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
				hasAnything |= StringUtils.isNotBlank(cell.getStringCellValue());
			}
		}
		if (!hasAnything)
			return;

		FindingImportWrapper findingImportWrapper = new FindingImportWrapper();
		Finding finding = new Finding();
		finding.setAuditUser(this.user);
		finding.setAuditDate(new Date());

		StringBuffer errorBuffer = new StringBuffer();

		parseAuditType(auditTypeString, finding, errorBuffer);

		parseCompany(companyString, finding, errorBuffer);

		parseDepartment(departmentString, finding, errorBuffer);

		parseBusinessProcess(mainProcessString, subProcessString, finding, errorBuffer);

		parseFindingNumber(findingNoString, finding, errorBuffer);

		parseFindingSummary(findingSummary, finding, errorBuffer);

		parseFindingYear(findingYearString, finding, errorBuffer);

		parseReportName(reportNameString, finding, errorBuffer);

		parseLocation(locationString, finding, errorBuffer);

		parseDescription(findingTitleString, finding, errorBuffer);

		parsePriority(priorityString, finding, errorBuffer);

		parseDetail(detailString, finding, errorBuffer);

		parseRisk(riskString, finding, errorBuffer);

		parseRecommendation(recommendationString, finding, errorBuffer);

		parseManActionPlan(manActionPlanString, finding, errorBuffer);

		List<User> actionOwners = parseActionOwners(actionOwnersString, finding, errorBuffer);

		parseCompletionDate(completionDateCell, finding, errorBuffer);

		List<User> spocUsers = parseSpocs(spocsString, finding, errorBuffer);

		adjustFindingActionsForUsers(finding, actionOwners, spocUsers);

		finding.getStatusOfFinding();

		findingImportWrapper.setId(row.getRowNum());
		findingImportWrapper.setFinding(finding);
		findingImportWrapper.setErrorDetail(errorBuffer.toString());
		findingImportWrapper.setValid(errorBuffer.length() == 0);
		findingImportWrapperList.add(findingImportWrapper);
	}

	private void adjustFindingActionsForUsers(Finding finding, List<User> actionOwners, List<User> spocUsers) {
		finding.adjustFindingActionsForUsers(actionOwners, new ArrayList<>(), spocUsers);
	}

	private void parseLocation(String locationString, Finding finding, StringBuffer errorBuffer) {
		if (StringUtils.isNotBlank(locationString)) {
			finding.setLocation(locationString);

		} else {
			errorBuffer.append("-Location is blank!<br/>");
		}

	}

	private void parseCompletionDate(Cell completionDateCell, Finding finding, StringBuffer errorBuffer) {
		try {
			if (StringUtils.isNotBlank(completionDateCell.toString())) {
				Date completionDate = completionDateCell.getDateCellValue();
				finding.setCompletionDate(completionDate);
			} else {
				finding.setCompletionDate(completionDateCell.getDateCellValue());
				errorBuffer.append("Completion date is blank!<br/>");

			}
		} catch (IllegalStateException e) {
			errorBuffer.append("-Invalid completion date!<br/>");
		}
	}

	private List<User> parseActionOwners(String actionOwnersString, Finding finding, StringBuffer errorBuffer) {
		if (StringUtils.isNotBlank(actionOwnersString)) {
			List<User> actionOwners = getActionOwners(actionOwnersString);
			if (CollectionUtils.isNotEmpty(actionOwners)) {
				return actionOwners;
			} else {
				errorBuffer.append("-Action Owner(s) is not valid!<br/>");
			}
		} else {
			errorBuffer.append("-Action Owner(s) is blank!<br/>");
		}
		return new ArrayList<>();
	}

	private List<User> parseSpocs(String spocsString, Finding finding, StringBuffer errorBuffer) {
		if (StringUtils.isNotBlank(spocsString)) {
			List<User> spocs = getSpocs(spocsString);
			if (CollectionUtils.isNotEmpty(spocs)) {
				return spocs;
			} else {
				errorBuffer.append("-Spoc(s) is to valid!<br/>");
			}
		}
		return new ArrayList<>();
	}

	private void parseManActionPlan(String manActionPlanString, Finding finding, StringBuffer errorBuffer) {
		if (StringUtils.isNotBlank(manActionPlanString)) {
			finding.setManagementActionPlan(manActionPlanString);
		} else {
			errorBuffer.append("-Man. Action Plan is blank!<br/>");
		}
	}

	private void parseRecommendation(String recommendationString, Finding finding, StringBuffer errorBuffer) {
		if (StringUtils.isNotBlank(recommendationString)) {
			finding.setRecommendation(recommendationString);
		} else {
			errorBuffer.append("-Recommendation is blank!<br/>");
		}
	}

	private void parseRisk(String riskString, Finding finding, StringBuffer errorBuffer) {
		if (StringUtils.isNotBlank(riskString)) {
			finding.setRisk(riskString);
		} else {
			errorBuffer.append("-Risk is blank!<br/>");
		}
	}

	private void parseDetail(String detailString, Finding finding, StringBuffer errorBuffer) {
		if (StringUtils.isNotBlank(detailString)) {
			finding.setDetail(detailString);
		} else {
			errorBuffer.append("-Detail is blank!<br/>");
		}
	}

	private void parsePriority(String priorityString, Finding finding, StringBuffer errorBuffer) {
		if (StringUtils.isNotBlank(priorityString)) {
			Priority priority = findPriority(priorityString);
			if (priority != null) {
				finding.setPriority(priority);
			} else {
				errorBuffer.append("-Priority is not valid!<br/>");
			}
		} else {
			errorBuffer.append("-Priority is blank!<br/>");
		}
	}

	private void parseDescription(String findingTitleString, Finding finding, StringBuffer errorBuffer) {
		if (StringUtils.isNotBlank(findingTitleString)) {
			finding.setDescription(findingTitleString);
		} else {
			errorBuffer.append("-Finding Description is blank!<br/>");
		}
	}

	private void parseFindingNumber(String findingNoString, Finding finding, StringBuffer errorBuffer) {
		if (StringUtils.isNotBlank(findingNoString)) {
			final Finding retrieveFinding = findingService.retrieveFinding(findingNoString);
			if (retrieveFinding != null) {
				finding.setFindingNumber(findingNoString);
				errorBuffer.append("-Finding Number is duplicated!<br/>");
			} else {
				finding.setFindingNumber(findingNoString);
			}
		} else {
			errorBuffer.append("-Finding Number is blank!<br/>");
		}
	}

	private void parseFindingYear(String findingYearString, Finding finding, StringBuffer errorBuffer) {
		if (StringUtils.isNotBlank(findingYearString)) {
			finding.setFindingYear(new LocalDate(Integer.parseInt(findingYearString), 1, 1).toDate());

		} else {
			errorBuffer.append("-Finding Year is blank!<br/>");
		}
	}

	private void parseReportName(String reportNameString, Finding finding, StringBuffer errorBuffer) {
		if (StringUtils.isNotBlank(reportNameString)) {
			finding.setReportName(reportNameString);

		} else {
			errorBuffer.append("-Report Name is blank!<br/>");
		}
	}

	private void parseBusinessSubProcess(String subProcessString, Finding finding, StringBuffer errorBuffer) {
		if (StringUtils.isNotBlank(subProcessString)) {
			final BusinessProcessType businessProcessType = finding.getBusinessProcessType();

			BusinessSubProcessType businessSubProcessType = findSubProcess(subProcessString,
					businessProcessType.subprocessTypesAsList());
			if (businessSubProcessType != null) {
				finding.setBusinessSubProcessType(businessSubProcessType);
			} else {
				errorBuffer.append("-Business Sub Process Type is not valid!<br/>");
			}
		} else {
			errorBuffer.append("-Business Sub Process Type is blank!<br/>");
		}
	}

	private void parseBusinessProcess(String mainProcessString, String subProcessString, Finding finding,
			StringBuffer errorBuffer) {
		if (StringUtils.isNotBlank(mainProcessString)) {
			BusinessProcessType businessProcessType = findMainProcess(mainProcessString);
			if (businessProcessType != null) {
				finding.setBusinessProcessType(businessProcessType);
				parseBusinessSubProcess(subProcessString, finding, errorBuffer);
			} else {
				errorBuffer.append("-Business Process Type is not valid!<br/>");
			}
		} else {
			errorBuffer.append("-Business Process Type is blank!<br/>");
		}
	}

	private void parseCompany(String companyString, Finding finding, StringBuffer errorBuffer) {
		if (StringUtils.isNotBlank(companyString)) {
			Company company = findCompany(companyString);
			if (company != null) {
				finding.setCompany(company);
			} else {
				errorBuffer.append("-Company is not valid!<br/>");
			}
		} else {
			AuditType auditType = finding.getAuditType();
			if (auditType != null && auditType.getCompanyRequired()) {
				errorBuffer.append("-Company is blank!<br/>");
			}
		}
	}

	private void parseDepartment(String departmentString, Finding finding, StringBuffer errorBuffer) {
		if (StringUtils.isNotBlank(departmentString)) {
			Department department = findDepartment(departmentString);
			if (department != null) {
				finding.setDepartment(department);
			} else {
				errorBuffer.append("-Department is not valid!<br/>");
			}
		} else {
			AuditType auditType = finding.getAuditType();
			if (auditType != null && auditType.getCompanyRequired()) {
				errorBuffer.append("-Department is blank!<br/>");
			}
		}
	}

	private void parseFindingSummary(String findingSummaryString, Finding finding, StringBuffer errorBuffer) {
		if (StringUtils.isNotBlank(findingSummaryString)) {
			finding.setFindingSummary(findingSummaryString);
		} else {
			errorBuffer.append("-finding Summary is blank!<br/>");
		}
	}

	private void parseAuditType(String auditTypeString, Finding finding, StringBuffer errorBuffer) {
		if (StringUtils.isNotBlank(auditTypeString)) {
			AuditType auditType = findAuditType(auditTypeString);
			if (auditType != null) {
				finding.setAuditType(auditType);
			} else {
				errorBuffer.append("-Audit Type is not valid!<br/>");
			}
		} else {
			errorBuffer.append("-Audit Type is blank!<br/>");
		}
	}

	private List<User> getActionOwners(String actionOwnersString) {
		List<User> possibleActionOwners = new ArrayList<User>();
		StringTokenizer stringTokenizer = new StringTokenizer(actionOwnersString, ",");
		while (stringTokenizer.hasMoreElements()) {
			final String nextToken = stringTokenizer.nextToken();
			for (User user : users) {
				if (user.getUsername().equalsIgnoreCase(nextToken)) {
					possibleActionOwners.add(user);
					break;
				}
			}
		}
		return possibleActionOwners;
	}

	private List<User> getSpocs(String spocsString) {
		List<User> possibleSpocs = new ArrayList<User>();
		StringTokenizer stringTokenizer = new StringTokenizer(spocsString, ",");
		while (stringTokenizer.hasMoreElements()) {
			final String nextToken = stringTokenizer.nextToken();
			for (User user : users) {
				if (user.getUsername().equalsIgnoreCase(nextToken)) {
					possibleSpocs.add(user);
					break;
				}
			}
		}
		return possibleSpocs;
	}

	private Priority findPriority(String priorityString) {
		final Priority[] values = Priority.values();
		for (Priority value : values) {
			if (value.getDescription().equals(priorityString)) {
				return value;
			}
		}
		return null;
	}

	private BusinessSubProcessType findSubProcess(String subProcessString,
			List<BusinessSubProcessType> businessSubProcessTypes) {
		BusinessSubProcessType businessSubProcessType = null;
		for (BusinessSubProcessType currentBusinessSubProcessType : businessSubProcessTypes) {
			if (currentBusinessSubProcessType.getDescription().equals(subProcessString)) {
				businessSubProcessType = currentBusinessSubProcessType;
				break;
			}
		}
		return businessSubProcessType;
	}

	private BusinessProcessType findMainProcess(String mainProcessString) {
		return findingService.retrieveBusinessProcessType(mainProcessString);
	}

	private Company findCompany(String companyString) {
		return findingService.retrieveCompany(companyString);
	}

	private Department findDepartment(String departmentString) {
		return findingService.retrieveDepartment(departmentString);
	}

	private AuditType findAuditType(String auditTypeString) {
		return findingService.retrieveAuditType(auditTypeString);
	}

	public void exportTemplate() {
		final Workbook workbook = this.findingExcelService.prepareExcelTemplate();
		sendFileToBrowser(workbook);
	}

	public void importFindings() {
		try {
			Preconditions.checkArgument(CollectionUtils.isNotEmpty(this.findingImportWrapperList), "There is not any findings suitable to import");
			int count = this.getMatchedCount();

			if (count > 0) {
				for (FindingImportWrapper findingImportWrapper : this.findingImportWrapperList) {
					if (findingImportWrapper.isValid()) {
						final Finding f = this.findingService.mergeFinding(findingImportWrapper.getFinding());
						this.generateActionOwnersandGeneralInfo(f);
						this.findingService.mergeFinding(f);
						this.sendOpeningEmails(f);
					}
				}
				FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, "Findings are imported successfully", null);
				this.findingImportWrapperList = new ArrayList<FindingImportWrapper>();
			} else {
				FacesUtils.addMessage(FacesMessage.SEVERITY_WARN, "There are not any valid findings to import.", null);
			}
		} catch (Exception exception) {
			exception.printStackTrace();
			FacesUtils.addMessage(FacesMessage.SEVERITY_ERROR, exception.getMessage(), null);
		}
	}
	
	public void generateActionOwnersandGeneralInfo(Finding f ) {
		for (FindingAction findingAction : f.findingActionsAsList()) {
			
			try {
				if (findingAction.isReadOnly()) {
					if (f.getActionOwners() != null && !f.getActionOwners().contains(findingAction.getActionOwner().getNameSurname())) {
						f.setActionOwners(f.getActionOwners() + " • " + findingAction.getActionOwner().getNameSurname()
								+ " [Read Only]\n");
					} else {
						f.setActionOwners(" • " + findingAction.getActionOwner().getNameSurname() + " [Read Only]\n");
					}
				} else if (findingAction.isSpoc() ) {
					if (f.getActionOwners() != null && !f.getActionOwners().contains(findingAction.getActionOwner().getNameSurname())) {
						
							f.setActionOwners(f.getActionOwners() + " • " + findingAction.getActionOwner().getNameSurname()
									+ " [SPOC]\n");	
					
					} else {
						f.setActionOwners(" • " + findingAction.getActionOwner().getNameSurname() + " [SPOC]\n");
					}
					
				} else {
					if (f.getActionOwners() != null && !f.getActionOwners().contains(findingAction.getActionOwner().getNameSurname())) {
						f.setActionOwners(
								f.getActionOwners() + " • " + findingAction.getActionOwner().getNameSurname() + "\n");
					} else {
						f.setActionOwners(" • " + findingAction.getActionOwner().getNameSurname() + "\n");
					}
				}				
			} catch (Exception e) {
				
				e.printStackTrace();
			}
		}
		
		f.setGeneralInfo(f.getAuditType().getDescription() + "\n" + f.getBusinessProcessType().getDescription()
				+ "\n" + f.getBusinessSubProcessType().getDescription() + "\n" + f.getReportName());
		
		 f = commanFunctionsService.updatePercentageOfFinding(f);
		 f = commanFunctionsService.updateStatus(f);
		 f = commanFunctionsService.updatePeriod(f);
	}

	private void sendOpeningEmails(Finding finding) {
		if (finding.getAuditType().getOpeningEmailContent() != null) {
			this.mailService.sendOpeningEmailToFindingActionOwnersAndResponsibleHeads(finding,
					finding.getAuditType().getOpeningEmailContent().getMailContent());
		}
	}
	

	
	private int getMatchedCount() {
		return CollectionUtils.countMatches(this.findingImportWrapperList, new Predicate() {
			@Override
			public boolean evaluate(Object object) {
				return ((FindingImportWrapper) object).isValid();
			}
		});
	}

	public String sendFileToBrowser(Workbook workbook) {
		try {
			String filename = "findingsTemplate.xls";

			FacesContext facesContext = FacesContext.getCurrentInstance();
			ExternalContext externalContext = facesContext.getExternalContext();
			externalContext.setResponseContentType("application/vnd.ms-excel");

			externalContext.setResponseHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");

			workbook.write(externalContext.getResponseOutputStream());
			facesContext.responseComplete();

			return null;
		} catch (Exception e) {
			return null;
		}
	}

}
