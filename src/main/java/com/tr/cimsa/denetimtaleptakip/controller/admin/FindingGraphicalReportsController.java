package com.tr.cimsa.denetimtaleptakip.controller.admin;

import com.google.common.collect.Lists;
import com.tr.cimsa.denetimtaleptakip.model.Finding;
import com.tr.cimsa.denetimtaleptakip.model.Status;

import lombok.Getter;
import org.primefaces.event.ItemSelectEvent;
import org.primefaces.model.chart.PieChartModel;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

/**
 * User: mhazer
 * Date: 8/30/12
 * Time: 11:21 PM
 */
@ManagedBean(name = "findingGraphicalReportsController")
@RequestScoped
public class FindingGraphicalReportsController extends BaseAdminController implements Serializable
{

    @Getter
    private PieChartModel pieChartModel;
    
    @PostConstruct
    public void init()
    {
    	List<Finding> findings = this.findingService.retrieveAllFindings();
        pieChartModel = preparePieChart(findings);
    }

    public static PieChartModel preparePieChart(List<Finding> findings)
    {
        PieChartModel _pieChartModel = new PieChartModel();
        for (Finding finding : findings)
        {
            Status findingStatus = finding.getStatus();
            if (_pieChartModel.getData().get(findingStatus.getDescription()) == null)
            {
                _pieChartModel.set(findingStatus.getDescription(), 1);
            }
            else
            {
                Number oldValue = _pieChartModel.getData().get(findingStatus.getDescription());
                Number newValue = (oldValue.intValue()) + 1;
                _pieChartModel.set(findingStatus.getDescription(), newValue);
            }
        }
        return _pieChartModel;
    }

    public void selectPieChart(ItemSelectEvent event) throws IOException
    {
        String statusOfSelectedChartItem = Lists.newLinkedList(pieChartModel.getData().entrySet()).get(event.getItemIndex()).getKey();
        logger.debug("clicked chart item is {}", statusOfSelectedChartItem);
        FacesContext.getCurrentInstance().getExternalContext().redirect("/" + FacesContext.getCurrentInstance().getExternalContext().getContextName() + "/admin/searchFinding.xhtml?status=" + Status.findStatusFromDescription(statusOfSelectedChartItem));
    }

    @Override
    protected Class getClazz()
    {
        return FindingGraphicalReportsController.class;
    }
}
