package com.tr.cimsa.denetimtaleptakip.controller.admin;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.DualListModel;

import com.google.common.collect.Lists;
import com.tr.cimsa.denetimtaleptakip.controller.util.FacesUtils;
import com.tr.cimsa.denetimtaleptakip.model.AuditType;
import com.tr.cimsa.denetimtaleptakip.model.BusinessProcessType;
import com.tr.cimsa.denetimtaleptakip.model.Finding;
import com.tr.cimsa.denetimtaleptakip.model.User;

import lombok.Getter;
import lombok.Setter;

/**
 * User: mhazer Date: 8/30/12 Time: 12:50 PM
 */
@ManagedBean(name = "auditTypeAdminController")
@ViewScoped
public class AuditTypeAdminController extends BaseAdminController implements Serializable {
	@Setter
	@Getter
	private AuditType selectedAuditType;

	@Getter
	@Setter
	private DualListModel<BusinessProcessType> possibleBusinessTypes;

	@Getter
	@Setter
	private DualListModel<User> possibleFollowers;

	public void selectAuditType(AuditType auditType) {
		this.findingService.reattachReadOnly(auditType);
		this.selectedAuditType = auditType;
		prepareBusinessTypesDualListModel();
		adjustBusinessTypesDualListModel();
		preparePossibleFollowersDualListModel();
		adjustPossibleFollowersDualListModel();
	}

	public void deleteAuditType(AuditType auditType) {
		try {
			SearchParameters searchParameters = new SearchParameters();
			List<AuditType> auditTypeList = new ArrayList<>();
			auditTypeList.add(auditType);
			searchParameters.setAuditTypes(auditTypeList);

			List<Finding> findingList = this.findingService.searchFindings(searchParameters);

			if (findingList != null && !findingList.isEmpty()) {
				FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, "This audit type have findings", null);
			} else {
				findingService.deleteAuditType(auditType);
				this.commonDefinitionsController.refreshAuditTypes();
			}
		} catch (Exception e) {
			logger.error("deleteAuditType Exception :"+e.getMessage());
		}

	}
	private void prepareBusinessTypesDualListModel() {
		List<BusinessProcessType> businessTypeSource = Lists.newLinkedList();
		List<BusinessProcessType> businessTypeTarget = Lists.newLinkedList();
		businessTypeSource.addAll(commonDefinitionsController.getBusinessProcessTypes());
		possibleBusinessTypes = new DualListModel<BusinessProcessType>(businessTypeSource, businessTypeTarget);
		
	}
	

	private void adjustBusinessTypesDualListModel() {
		for (BusinessProcessType businessProcessType : this.selectedAuditType.getBusinessProcessTypes()) {
			if (possibleBusinessTypes.getSource().contains(businessProcessType)) {
				possibleBusinessTypes.getSource().remove(businessProcessType);
				possibleBusinessTypes.getTarget().add(businessProcessType);
			}
		}
	}

	private void preparePossibleFollowersDualListModel() {
		List<User> possibleFollowersSource = Lists.newLinkedList();
		List<User> possibleFollowersTarget = Lists.newLinkedList();
		possibleFollowersSource.addAll(commonDefinitionsController.getActiveUsers());
		possibleFollowers = new DualListModel<User>(possibleFollowersSource, possibleFollowersTarget);		
	}
	private void adjustPossibleFollowersDualListModel() {		
		for (User follower : this.selectedAuditType.getAuditTypeUpdateFollowers()) {
			if (possibleFollowers.getSource().contains(follower)) {
				possibleFollowers.getSource().remove(follower);
				possibleFollowers.getTarget().add(follower);
			}
		}	
	}

	public void addNewAuditType() {
		this.selectedAuditType = new AuditType();
		prepareBusinessTypesDualListModel();
		preparePossibleFollowersDualListModel();
	}

	public void saveAuditType() {
		try {
			this.selectedAuditType.setAdminUser(visit.getUser());
			this.selectedAuditType.setEditDate(new Date());
			this.findingService.mergeAuditType(this.getSelectedAuditType(), getPossibleBusinessTypes().getTarget(),
					possibleFollowers.getTarget());
			this.commonDefinitionsController.refreshAuditTypes();
			this.commonDefinitionsController.refreshBusinessTypes();
			FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, "Audit updated successfully", null);
		} catch (Exception exception) {
			FacesUtils.addMessage(FacesMessage.SEVERITY_INFO,
					"Can not save audit type (" + exception.getMessage() + ")", null);
		} finally {
			this.selectedAuditType = null;
		}
	}

	@Override
	protected Class getClazz() {
		return AuditTypeAdminController.class;
	}
}
