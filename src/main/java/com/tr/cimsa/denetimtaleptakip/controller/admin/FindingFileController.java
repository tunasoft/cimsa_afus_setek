package com.tr.cimsa.denetimtaleptakip.controller.admin;

import com.tr.cimsa.denetimtaleptakip.controller.util.FacesUtils;
import com.tr.cimsa.denetimtaleptakip.model.Finding;
import com.tr.cimsa.denetimtaleptakip.model.FindingFile;
import com.tr.cimsa.denetimtaleptakip.model.User;
import com.tr.cimsa.denetimtaleptakip.services.FindingService;

import lombok.*;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.*;

/**
 * User: mhazer Date: 5/21/13 Time: 9:12 AM
 */
@ManagedBean(name = "findingFileController")
@ViewScoped
public class FindingFileController implements Serializable {
	private final static Logger LOGGER = LoggerFactory.getLogger(EditFindingController.class);
	
	@Getter
	@Setter
	private List<FindingFile> findingFiles=new LinkedList<FindingFile>();
	
	@Getter
	@Setter
	private Finding finding;

	public void handleFileUpload(UploadedFile uploadedFile, Finding finding, User user) {
		try {
			FindingFile findingFile = FindingFile.createInstance(finding, user, uploadedFile);
			finding.getFindingFiles().add(findingFile);
			FacesUtils.addMessage(FacesMessage.SEVERITY_INFO,
					uploadedFile.getFileName() + " named file uploaded successfully", null);
		} catch (Exception exception) {
			exception.printStackTrace();
			LOGGER.error("error occurred during adding file to finding ({})", exception.getMessage());
			FacesUtils.addMessage(FacesMessage.SEVERITY_ERROR,
					"Error occured during  uploading file (" + exception.getMessage() + ")", null);
		}
	}

	public void removeFile(FindingFile findingFile, Finding f) {
		f.getFindingFiles().remove(findingFile);
		this.setFindingFiles(new LinkedList<FindingFile>());
		this.getFindingFiles().add(findingFile);
		this.setFinding(f);
	}

	public StreamedContent viewFile(FindingFile file) {

		try {
			InputStream stream = new ByteArrayInputStream(file.getContent());
			DefaultStreamedContent streamedContent = new DefaultStreamedContent(stream, file.getContentType(),
					file.getFileName());
			return streamedContent;
		} catch (Exception exception) {
			FacesUtils.addMessage(FacesMessage.SEVERITY_ERROR,
					"Error occured during downloading file (" + exception.getMessage() + ")", null);
		}

		return null;
	}
}
