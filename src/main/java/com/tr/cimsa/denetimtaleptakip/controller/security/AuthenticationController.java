package com.tr.cimsa.denetimtaleptakip.controller.security;

import java.io.IOException;
import java.util.Collection;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import lombok.Getter;
import lombok.Setter;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import com.google.common.base.Preconditions;
import com.tr.cimsa.denetimtaleptakip.controller.Visit;
import com.tr.cimsa.denetimtaleptakip.controller.util.FacesUtils;
import com.tr.cimsa.denetimtaleptakip.model.User;
import com.tr.cimsa.denetimtaleptakip.services.UserService;

/**
 * User: mhazer
 * Date: 8/18/12
 * Time: 6:07 PM
 */
@ManagedBean(name = "authenticationController")
@RequestScoped
public class AuthenticationController
{
    @ManagedProperty("#{authenticationManager}")
    @Setter
    private AuthenticationManager authenticationManager;

    @ManagedProperty("#{userService}")
    @Setter
    private UserService userService;

    @ManagedProperty("#{visit}")
    @Setter
    private Visit visit;

    @Getter
    @Setter
    private String username;

    @Getter
    @Setter
    private String name_surname;
    
    public String login() throws IOException
    {
        try
        {

        	Authentication request = new UsernamePasswordAuthenticationToken(this.username, this.username);
        	Authentication result = authenticationManager.authenticate(request);
        	SecurityContextHolder.getContext().setAuthentication(result);

            User user = this.userService.findUser(this.username);
            Preconditions.checkNotNull(user, "Auth successful but user can not found at the Çimsa Audit Follow Up System database");
            Preconditions.checkArgument(user.isActive(), "Auth successful but user is deactivated, contact w/ Çimsa Audit Follow Up System administrator.");
            user.isUserReponsibleOfAnyBusinessType();
            visit.setUser(user);
            redirectUserToWelcomePage();
            return null;
        	
        }
        catch (Exception exception)
        {
            FacesUtils.addMessage(FacesMessage.SEVERITY_ERROR, exception.getMessage(), exception.getMessage());
            return null;
        }
    }

    public void redirectUserToWelcomePage() throws IOException
    {
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        String defaultURL = findWelcomeUrlForRole();
        context.redirect(defaultURL);
        FacesContext.getCurrentInstance().responseComplete();
    }
    
    private String findWelcomeUrlForRole()
    {
        if (visit.getUser().hasRole("ROLE_HEAD") || visit.getUser().hasRole("ROLE_BOSS") || visit.getUser().hasRole("ROLE_ADMIN"))
        {
            return FacesUtils.getContextPath() + "/admin/summaryReport.xhtml";
        }
        return FacesUtils.getContextPath() + "/admin/searchFinding.xhtml";
    }

    public void handleAlreadyLoggedInUsers() throws IOException
    {
        if (getUserDetails() != null && !FacesContext.getCurrentInstance().isPostback())
        {
            FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, "You're already loggedin, please logut first", null);
            redirectUserToWelcomePage();
        }
    }

    public String logout() throws IOException
    {
        this.username = "";
        this.name_surname = "";
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        context.redirect(context.getRequestContextPath() + "/j_spring_security_logout");
        FacesContext.getCurrentInstance().responseComplete();
        return null;
    }

    public final boolean hasRole(String role)
    {
        boolean hasRole = false;
        UserDetails userDetails = getUserDetails();
        if (userDetails != null)
        {
            Collection<GrantedAuthority> authorities = (Collection<GrantedAuthority>) userDetails.getAuthorities();
            if (isRolePresent(authorities, role))
            {
                hasRole = true;
            }
        }
        return hasRole;
    }

    private UserDetails getUserDetails()
    {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        UserDetails userDetails = null;
        if (principal instanceof UserDetails)
        {
            userDetails = (UserDetails) principal;
        }
        return userDetails;
    }

    private boolean isRolePresent(Collection<GrantedAuthority> authorities, String role)
    {
        boolean isRolePresent = false;
        for (GrantedAuthority grantedAuthority : authorities)
        {
            isRolePresent = grantedAuthority.getAuthority().equals(role);
            if (isRolePresent) break;
        }
        return isRolePresent;
    }
    
}
