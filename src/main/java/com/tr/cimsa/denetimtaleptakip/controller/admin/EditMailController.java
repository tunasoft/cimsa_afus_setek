package com.tr.cimsa.denetimtaleptakip.controller.admin;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.tr.cimsa.denetimtaleptakip.controller.util.FacesUtils;
import com.tr.cimsa.denetimtaleptakip.model.AuditType;
import com.tr.cimsa.denetimtaleptakip.model.EmailContent;
import com.tr.cimsa.denetimtaleptakip.services.FindingService;

import lombok.Getter;
import lombok.Setter;

@ManagedBean(name = "editMailController")
@ViewScoped
public class EditMailController extends BaseAdminController implements Serializable {

	@ManagedProperty("#{findingService}")
	@Setter
	private FindingService findingService;

	@Getter
	@Setter
	private EmailContent selectedEmailContent;

	public FindingService getFindingService() {
		return findingService;
	}

	@PostConstruct
	public void init() {
		selectedEmailContent = new EmailContent();
	}

	public void createNewMailContent() {
		this.selectedEmailContent = new EmailContent();
	}

	public void saveOrUpdateSelectedEmailContent() {
		findingService.saveOrUpdateEmailContent(selectedEmailContent);
		refreshEmailContents();
	}

	public void deleteMailContent() {
		 List<AuditType> list = commonDefinitionsController.getAuditTypes();
		 String	mailDescription = selectedEmailContent.getDescription();
		 boolean isExist = false;
		 
		for (AuditType auditType : list) {
		
			try {
				if (( auditType.getOpeningEmailContent()!= null && auditType.getOpeningEmailContent().getDescription().equals(mailDescription))
						|| (auditType.getUpdateEmailContent()!= null && auditType.getUpdateEmailContent().getDescription().equals(mailDescription))
						|| (auditType.getSendWeeklyReminderMail()!= null && auditType.getSendWeeklyReminderMail().getDescription().equals(mailDescription))
						|| (auditType.getEmailStrategyContentOne()!= null &&auditType.getEmailStrategyContentOne().getDescription().equals(mailDescription))
						|| (auditType.getEmailStrategyContentTwo()!= null && auditType.getEmailStrategyContentTwo().getDescription().equals(mailDescription))
						|| (auditType.getEmailStrategyContentThree()!= null && auditType.getEmailStrategyContentThree().getDescription().equals(mailDescription))) 
				{
					FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, "This e-mail content is being used by audit types.", null);
					isExist= true;
					break;
					
				}
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
			
		}
		if (!isExist) {
			findingService.deleteEmailContent(selectedEmailContent);
			refreshEmailContents();
		}
		
	}

	private void refreshEmailContents() {
		this.commonDefinitionsController.refreshEmailContents();
	}

	@Override
	protected Class getClazz() {
		return EditMailController.class;
	}

}
