package com.tr.cimsa.denetimtaleptakip.controller.admin;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.tr.cimsa.denetimtaleptakip.model.AuditType;
import com.tr.cimsa.denetimtaleptakip.model.BusinessProcessType;
import com.tr.cimsa.denetimtaleptakip.model.BusinessSubProcessType;
import com.tr.cimsa.denetimtaleptakip.model.Company;
import com.tr.cimsa.denetimtaleptakip.model.Department;
import com.tr.cimsa.denetimtaleptakip.model.EmailReason;
import com.tr.cimsa.denetimtaleptakip.model.Finding;
import com.tr.cimsa.denetimtaleptakip.model.Priority;
import com.tr.cimsa.denetimtaleptakip.model.Status;
import com.tr.cimsa.denetimtaleptakip.model.User;

import lombok.Data;

/**
 * User: mhazer
 * Date: 8/22/12
 * Time: 11:32 AM
 */
@Data
public class SearchParameters implements Serializable
{
    private List<AuditType> auditTypes = Lists.newLinkedList();

    private List<Department> departments = Lists.newLinkedList();
    
    private List<Company> companies = Lists.newLinkedList();

    private String companyName;

    private List<BusinessProcessType> businessProcessTypes = Lists.newLinkedList();

    private List<BusinessSubProcessType> businessSubProcessTypes = Lists.newLinkedList();

    private List<Priority> priorities = Lists.newLinkedList();
    
    private List<Status> statuses = Lists.newLinkedList();

    private List<User> users = Lists.newLinkedList();

    private String description;

    private Date completionDateStart;

    private Date completionDateEnd;
    
    private Date findingEntryDate;
    
    private Date findingYear;
    
    private String findingYearStr;

    private String findingNo;
    
    private String reportName;
    
    private Integer completionPercentageStart;

    private Integer completionPercentageEnd;

    private Status status;

    private Boolean notImplemented;

    private Boolean notApproved;

    public void checkParameters()
    {
        boolean atLeastOneParameterFilled = isAtLeastOneSearchParameterFilled();
        Preconditions.checkArgument(atLeastOneParameterFilled, "In order to perform search you should fill at least one search criteria!");
    }

    public String getCompanyName()
    {
        return companyName;
    }

    public void addCompany(Company company) {
        companies.add(company);
    }

    public boolean isAtLeastOneSearchParameterFilled()
    {
        return CollectionUtils.isNotEmpty(auditTypes)
        		|| CollectionUtils.isNotEmpty(departments)
                || CollectionUtils.isNotEmpty(companies)
                || StringUtils.isNotBlank(companyName)
                || CollectionUtils.isNotEmpty(businessProcessTypes)
                || CollectionUtils.isNotEmpty(businessSubProcessTypes)
                || CollectionUtils.isNotEmpty(priorities)
                || CollectionUtils.isNotEmpty(statuses)
                || CollectionUtils.isNotEmpty(users)
                || StringUtils.isNotBlank(description)
                || completionDateStart != null
                || completionDateEnd != null
                || findingEntryDate != null
                || findingYear != null
                || StringUtils.isNotBlank(findingYearStr)
                || StringUtils.isNotBlank(findingNo)
                || StringUtils.isNotBlank(reportName)
                || completionPercentageStart != null
                || completionPercentageEnd != null
                || status != null
                || notImplemented == Boolean.TRUE
                || notApproved == Boolean.TRUE;
    }

    public void clearFields()
    {
        this.auditTypes = null;
        this.departments = null;
        this.companies = null;
        this.businessProcessTypes = null;
        this.businessSubProcessTypes = null;
        this.priorities = null;
        this.statuses = null;
        this.users = null;
        this.description = null;
        this.findingEntryDate = null;
        this.findingYear = null;
        this.completionDateStart = null;
        this.completionDateEnd = null;
        this.completionPercentageStart = null;
        this.completionPercentageEnd = null;
        this.status = null;
        this.notImplemented = Boolean.FALSE;
        this.notApproved = Boolean.FALSE;
    }

    public DetachedCriteria createSearhQueryFromParameters()
    {
        checkParameters();
        boolean findinActionAliasAdded = false;
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Finding.class, "finding");
        if (CollectionUtils.isNotEmpty(getAuditTypes()))
        {
            detachedCriteria.add(Restrictions.in("auditType", getAuditTypes()));
        }
        if (CollectionUtils.isNotEmpty(getDepartments()))
        {
            detachedCriteria.add(Restrictions.in("department", getDepartments()));
        }
        if (CollectionUtils.isNotEmpty(getCompanies()))
        {
            detachedCriteria.add(Restrictions.in("company", getCompanies()));
        }
        if (CollectionUtils.isNotEmpty(getPriorities()))
        {
            detachedCriteria.add(Restrictions.in("priority", getPriorities()));
        }
        if (CollectionUtils.isNotEmpty(getStatuses()))
        {
            detachedCriteria.add(Restrictions.in("status", getStatuses()));
        }
        if (CollectionUtils.isNotEmpty(getBusinessProcessTypes()))
        {
            detachedCriteria.add(Restrictions.in("businessProcessType", getBusinessProcessTypes()));
        }
        if (CollectionUtils.isNotEmpty(getBusinessSubProcessTypes()))
        {
            detachedCriteria.add(Restrictions.in("businessSubProcessType", getBusinessSubProcessTypes()));
        }
        if (CollectionUtils.isNotEmpty(getUsers()))
        {
            findinActionAliasAdded = true;
            detachedCriteria.createAlias("finding.findingActions", "findingAction");
            detachedCriteria.add(Restrictions.in("findingAction.actionOwner", getUsers()));
        }
        if (StringUtils.isNotBlank(description))
        {
            detachedCriteria.add(Restrictions.like("finding.description", description, MatchMode.ANYWHERE));
        }
        if (completionDateStart != null)
        {
            detachedCriteria.add(Restrictions.ge("finding.completionDate", completionDateStart));
        }
        if (completionDateEnd != null)
        {
            detachedCriteria.add(Restrictions.le("finding.completionDate", completionDateEnd));
        }
        if (findingEntryDate != null)
        {
        	long ltime = findingEntryDate.getTime()+24*60*60*1000;
        	Date findingEntryDatePlusOneDay = new Date(ltime);
            detachedCriteria.add(Restrictions.ge("finding.auditDate", findingEntryDate));
            detachedCriteria.add(Restrictions.le("finding.auditDate", findingEntryDatePlusOneDay));
        }
        if (findingYear != null)
        {
            detachedCriteria.add(Restrictions.eq("finding.findingYear", findingYear));
        }
        if (StringUtils.isNotBlank(findingNo))
        {
            detachedCriteria.add(Restrictions.like("finding.findingNumber", findingNo, MatchMode.START));
        }
        if (StringUtils.isNotBlank(reportName))
        {
            detachedCriteria.add(Restrictions.like("finding.reportName", reportName, MatchMode.START));
        }
        if(this.completionPercentageStart != null)
        {
            if(!findinActionAliasAdded)
            {
                detachedCriteria.createAlias("finding.findingActions", "findingAction");
                findinActionAliasAdded = true;
            }
            detachedCriteria.add(Restrictions.ge("findingAction.completionPercentage", completionPercentageStart));
        }
        if(this.completionPercentageEnd != null)
        {
            if(!findinActionAliasAdded)
            {
                detachedCriteria.createAlias("finding.findingActions", "findingAction");
            }
            detachedCriteria.add(Restrictions.le("findingAction.completionPercentage", completionPercentageEnd));
        }
      
        if(this.notImplemented == Boolean.TRUE)
        {
            detachedCriteria.add(Restrictions.eq("completed", Boolean.TRUE));
            /**
             * AFUS-12
             * Bulguyu edit ettiğimizde en altta "Do you want to send warning e-mails?" sorusu çıkıyor. Bunun yanındaki check box'taki tiki kaldırdığımızda sebep soruyor. Buradaki menüde "Completed" ve "Not Implemented" seçenekleri var. Biz burada "Completed", "Restructured" ve "Rejected" seçeneklerinin olmasını istiyoruz. Daha önce "Not implemented" şeklinde işaretlenmiş olanların da "Rejected"a çevrilmesini istiyoruz.
             *
             * Zekai Oğuz Özalp, 10 Ekim 2014
             */
            //detachedCriteria.add(Restrictions.eq("reasonToNotSendEmail", this.notImplemented == Boolean.TRUE ? EmailReason.NOT_IMPLEMENTED : null));
            if (this.notImplemented == Boolean.TRUE) {
                detachedCriteria.add(Restrictions.in("reasonToNotSendEmail", Arrays.asList(EmailReason.RESTRUCTURED, EmailReason.REJECTED)));
            } else {
                detachedCriteria.add(Restrictions.eq("reasonToNotSendEmail", null));
            }
        }
        if(this.notApproved == Boolean.TRUE)
        {
            detachedCriteria.add(Restrictions.eq("revised", Boolean.FALSE));
            detachedCriteria.add(Restrictions.isNotNull("revisedCompletionDate"));
        }
        
        return detachedCriteria;
    }

    public List<Finding> filterWithStatus(List<Finding> findings)
    {
        Iterable<Finding> filteredWithStatus =
                Iterables.filter(findings, new Predicate<Finding>()
                {
                    @Override
                    public boolean apply(Finding finding)
                    {
                        if (getStatus() == null || finding.getStatus() == getStatus())
                        {
                            return true;
                        }
                        return false;
                    }
                });
        return Lists.newLinkedList(filteredWithStatus);
    }

    public List<Finding> filterWithUserRole(List<Finding> findings, final User user)
    {
        if(!user.hasRole("ROLE_ADMIN") && !user.hasRole("ROLE_READONLY"))
        {
            Iterable<Finding> filteredWithStatus =
                    Iterables.filter(findings, new Predicate<Finding>()
                    {
                        @Override
                        public boolean apply(Finding finding)
                        {
                            if (finding.isFindingActionExistsForUser(user))
                            {
                                return true;
                            }
                            else
                            {
                                if(user.getResponsibleBusinessTypes().contains(finding.getBusinessProcessType())
                                        || user.getResponsibleBusinessSubTypes().contains(finding.getBusinessSubProcessType()))
                                {
                                    return true;
                                }
                            }
                            return false;
                        }
                    });
            return Lists.newLinkedList(filteredWithStatus);
        }
        return findings;
    }
    
    public List<String> getStatusDescriptions()
    {
    	List<Status> statusList = new ArrayList<Status>();
    	List<String> statusDescriptions = new ArrayList<String>();
    	Status s1 = Status.UNCONFIRMED_COMPLETED;
    	Status s2 = Status.CONFIRMED_COMPLETED;
    	Status s3 = Status.DELAYED;
    	Status s4 = Status.IN_PROGRESS;
    	Status s5 = Status.NOT_STARTED;
    	statusList.add(s1);
    	statusList.add(s2);
    	statusList.add(s3);
    	statusList.add(s4);
    	statusList.add(s5);
    	for (Status s : statusList) {
    		statusDescriptions.add(s.getDescription());
    	}
    	return statusDescriptions;
    }

	
}
