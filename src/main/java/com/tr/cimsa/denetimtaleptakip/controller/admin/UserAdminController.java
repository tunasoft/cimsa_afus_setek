package com.tr.cimsa.denetimtaleptakip.controller.admin;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.tr.cimsa.denetimtaleptakip.controller.Visit;
import com.tr.cimsa.denetimtaleptakip.controller.util.CommonDefinitionsController;
import com.tr.cimsa.denetimtaleptakip.controller.util.FacesUtils;
import com.tr.cimsa.denetimtaleptakip.model.Role;
import com.tr.cimsa.denetimtaleptakip.model.User;
import com.tr.cimsa.denetimtaleptakip.services.UserService;

import lombok.Getter;
import lombok.Setter;
import org.primefaces.model.DualListModel;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import org.springframework.security.ldap.authentication.ad.ActiveDirectoryLdapAuthenticationProvider;

/**
 * User: mhazer
 * Date: 8/28/12
 * Time: 4:49 PM
 */
@ManagedBean(name = "userAdminController")
@ViewScoped
public class UserAdminController implements Serializable
{
    @ManagedProperty("#{userService}")
    private UserService userService;

    @ManagedProperty("#{visit}")
    private Visit visit;

    @ManagedProperty("#{commonDefinitionsController}")
    private CommonDefinitionsController commonDefinitionsController;

    @Getter
    @Setter
    private User selectedUser;

    @Getter
    @Setter
    private DualListModel<Role> selectedUserRoles;

    public void selectUser(User user)
    {
        this.selectedUser = user;
        preparePossibleRolesDualListModel();
    }

    public void addNewUser()
    {
        this.selectedUser = new User();
        preparePossibleRolesDualListModel();
    }

    private void preparePossibleRolesDualListModel()
    {
        List<Role> rolesSource = Lists.newLinkedList();
        List<Role> rolesTarget = Lists.newLinkedList();
        rolesSource.addAll(commonDefinitionsController.getAllRoles());
        rolesTarget.addAll(this.selectedUser.getRoles());
        rolesSource.removeAll(this.selectedUser.getRoles());
        selectedUserRoles = new DualListModel<Role>(rolesSource, rolesTarget);
    }

    public void saveUser()
    {
        try
        {
            this.selectedUser.setAdminUser(visit.getUser());
            this.selectedUser.setEditDate(new Date());
            this.selectedUser.setRoles(Sets.newHashSet(this.selectedUserRoles.getTarget()));
            this.userService.mergeUser(this.selectedUser);
            FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, this.selectedUser.getUsername() + " added/updated successfully", null);
        }
        catch (Exception exception)
        {
            FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, "Can not save user (" + exception.getMessage() + ")", null);
        }
        finally
        {
            this.commonDefinitionsController.refreshUsers();
        }
    }

    public void setUserService(UserService userService)
    {
        this.userService = userService;
    }

    public void setCommonDefinitionsController(CommonDefinitionsController commonDefinitionsController)
    {
        this.commonDefinitionsController = commonDefinitionsController;
    }

    public void setVisit(Visit visit)
    {
        this.visit = visit;
    }

}
