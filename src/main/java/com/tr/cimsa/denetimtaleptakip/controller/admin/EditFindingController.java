package com.tr.cimsa.denetimtaleptakip.controller.admin;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.TransferEvent;
import org.primefaces.model.DualListModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.tr.cimsa.denetimtaleptakip.controller.util.FacesUtils;
import com.tr.cimsa.denetimtaleptakip.model.Department;
import com.tr.cimsa.denetimtaleptakip.model.Finding;
import com.tr.cimsa.denetimtaleptakip.model.FindingAction;
import com.tr.cimsa.denetimtaleptakip.model.FindingResponsibleAction;
import com.tr.cimsa.denetimtaleptakip.model.Status;
import com.tr.cimsa.denetimtaleptakip.model.User;
import com.tr.cimsa.denetimtaleptakip.services.FindingService;
import com.tr.cimsa.denetimtaleptakip.services.MailService;

import lombok.Getter;
import lombok.Setter;

/**
 * User: mhazer Date: 8/19/12 Time: 5:03 PM
 */
@ManagedBean(name = "editFindingController")
@ViewScoped
public class EditFindingController implements Serializable {
	private final static Logger LOGGER = LoggerFactory.getLogger(EditFindingController.class);


	
	@ManagedProperty("#{findingService}")
	@Setter
	private FindingService findingService;

	@ManagedProperty("#{mailService}")
	@Setter
	private MailService mailService;

	@ManagedProperty("#{visit.user}")
	@Setter
	private User user;

	@ManagedProperty("#{commonDefinitionsController.activeUsers}")
	@Setter
	private List<User> users;

	@ManagedProperty("#{findingFileController}")
	@Setter
	private FindingFileController findingFileController;

	@Getter
	@Setter
	private Integer findingIdToUpdate;

	@Getter
	@Setter
	private DualListModel<User> possibleActionOwners;

	@Setter
	@Getter
	private DualListModel<User> possibleReadOnlyActionOwners;

	@Getter
	@Setter
	private DualListModel<User> possibleSPOCs;

	@Getter
	private Finding finding;

	@Setter
	@Getter
	private Department department;

	@Getter
	private List<User> usersSource;

	@Getter
	private List<User> usersTarget;

	@Getter
	private List<User> readOnlyActionOwnersSource;

	@Getter
	private List<User> readOnlyActionOwnersTarget;

	private List<User> removedReadOnlyTarget;
	
	
	@ManagedProperty("#{commanFunctionsService}")
	@Setter
	private CommanFunctionsService commanFunctionsService;

	@PostConstruct
	public void init() {
		this.finding = new Finding();
		this.finding.setAuditDate(new Date());
		this.finding.setAuditUser(this.user);
		this.preparePossibleActionOwnerDualListModel();
	}

	private void preparePossibleActionOwnerDualListModel() {
		this.usersSource = Lists.newLinkedList();
		this.usersTarget = Lists.newLinkedList();
		usersSource.addAll(users);
		possibleActionOwners = new DualListModel<User>(usersSource, usersTarget);

		this.readOnlyActionOwnersSource = Lists.newLinkedList();
		this.readOnlyActionOwnersTarget = Lists.newLinkedList();
		readOnlyActionOwnersSource.addAll(users);
		possibleReadOnlyActionOwners = new DualListModel<User>(readOnlyActionOwnersSource, readOnlyActionOwnersTarget);

		List<User> spocSource = Lists.newLinkedList();
		List<User> spocTarget = Lists.newLinkedList();
		spocSource.addAll(users);
		possibleSPOCs = new DualListModel<User>(spocSource, spocTarget);
	}

	public void prepareFindingForUpdate() {
		
		if (!FacesContext.getCurrentInstance().isPostback() && findingIdToUpdate != null) {
			this.finding = this.findingService.retrieveFinding(findingIdToUpdate);
			this.department = finding.getDepartment();
			Preconditions.checkNotNull(this.finding, "Can not find finding with the given id :", findingIdToUpdate);
			adjustActionOwners();
		}
	}

	private void adjustActionOwners() {
		for (FindingAction findingAction : this.finding.getFindingActions()) {
			User findingActionUser = findingAction.getActionOwner();
			if (possibleActionOwners.getSource().contains(findingActionUser)
					|| possibleReadOnlyActionOwners.getSource().contains(findingActionUser)
					|| possibleSPOCs.getSource().contains(findingActionUser)) 
			{
				
				if (!findingAction.isReadOnly() && findingAction.isSpoc() && findingAction.isActionOwnerFlag()) {
					possibleActionOwners.getSource().remove(findingActionUser);
					possibleActionOwners.getTarget().add(findingActionUser);
					possibleSPOCs.getSource().remove(findingActionUser);
					possibleSPOCs.getTarget().add(findingActionUser);
				}else if (!findingAction.isReadOnly() && !findingAction.isSpoc()) {
					possibleActionOwners.getSource().remove(findingActionUser);
					possibleActionOwners.getTarget().add(findingActionUser);
				} else if (findingAction.isReadOnly()) {
					possibleReadOnlyActionOwners.getSource().remove(findingActionUser);
					possibleReadOnlyActionOwners.getTarget().add(findingActionUser);
				} else {
					possibleSPOCs.getSource().remove(findingActionUser);
					possibleSPOCs.getTarget().add(findingActionUser);
				}

			
			}
		}
	}

	public void changeAuditType() {
		this.finding.setBusinessProcessType(null);
		this.finding.setBusinessSubProcessType(null);
		if (this.finding.getAuditType() != null) {
			this.findingService.reattachReadOnly(this.finding.getAuditType());
		} else {
			this.finding.setBusinessProcessType(null);
			FacesUtils.addMessage(FacesMessage.SEVERITY_ERROR, "You have to choose audit type first", null);
		}
	}

	public void changeDepartment() {
		try {
			this.finding.setDepartment(department);
			if (department != null) {

				this.findingService.reattachReadOnly(this.finding.getDepartment());
			} else {

				FacesUtils.addMessage(FacesMessage.SEVERITY_ERROR, "You have to choose audit department", null);
			}
		} catch (Exception e) {
			LOGGER.debug("changing department error: "+ e);
		}
	}

	public void changeBusinessProcess() {
		this.finding.setBusinessSubProcessType(null);
		if (this.finding.getAuditType() != null) {
			this.findingService.reattachReadOnly(this.finding.getBusinessProcessType());
		} else {
			this.finding.setBusinessProcessType(null);
			FacesUtils.addMessage(FacesMessage.SEVERITY_ERROR, "You have to choose audit type first", null);
		}
	}

	public void changeSubBusinessProcess() {
		this.findingService.reattachReadOnly(this.finding.getBusinessSubProcessType());
	}

	public String mergeFinding() {
		try {
			LOGGER.debug("creating new finding");
			LOGGER.debug("finding is: {}", this.finding.getFindingNumber());
			LOGGER.debug("Selected action owners are: {}", possibleActionOwners.getTarget());
			LOGGER.debug("Selected read only action owners are: {}", possibleReadOnlyActionOwners.getTarget());
			LOGGER.debug("Selected spocs are: {}", possibleSPOCs.getTarget());
		
			if (this.finding != null && this.finding.getId() == null)
			
			{
				final Finding retrieveFinding = findingService.retrieveFinding(this.finding.getFindingNumber());
//				Preconditions.checkArgument((new Date()).before(this.finding.getCompletionDate()),
//						"Completion date must be after today");
				Preconditions.checkArgument(retrieveFinding == null, "Finding Number is duplicated!");
			}

			 finding = commanFunctionsService.updatePeriod(finding);
			
			
			if (removedReadOnlyTarget != null) {
				for (Iterator iterator = possibleReadOnlyActionOwners.getTarget().iterator(); iterator.hasNext();) {
					User user = (User) iterator.next();
					for (User removedUser : removedReadOnlyTarget) {
						if (removedUser.getNameSurname().equals(user.getNameSurname())) {
							iterator.remove();
						}
					}
				}
			}
//			possibleReadOnlyActionOwners.getTarget();
			
			this.finding.adjustFindingActionsForUsers(possibleActionOwners.getTarget(),
					possibleReadOnlyActionOwners.getTarget(), possibleSPOCs.getTarget());
			
			this.finding=this.generateActionOwnersandGeneralInfo(this.finding);
			this.finding = setFindingInfo(finding);
			this.finding = this.findingService.mergeFinding(this.finding);
			this.sendOpeningEmails(finding);
			FacesUtils.addMessage(FacesMessage.SEVERITY_INFO,
					this.finding.getAuditType().getDescription() + " finding created/updated successfully", null);
			
			return "/admin/findingAdmin.xhtml?faces-redirect=true";
		} catch (Exception exception) {
			LOGGER.error("can not save/update finding: {} ({})", finding, exception.getMessage());
			FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, "Can not save/update finding: " + exception.getMessage(),
					null);
		}
		return null;
	}

	private void sendOpeningEmails(Finding finding) {
		if (finding.getAuditType().getOpeningEmailContent() != null) {
			this.mailService.sendOpeningEmailToFindingActionOwnersAndResponsibleHeads(finding,
					finding.getAuditType().getOpeningEmailContent().getMailContent());
		}
	}

	private int flag = 0;

	public void actionOwnerPickListOnTransfer(TransferEvent event) {
		if (event.isAdd()) {
			List<User> addedUsers = (List<User>) event.getItems();

			removedReadOnlyTarget = new ArrayList<>();

			for (int i = 0; i < this.usersTarget.size(); i++) {
				for (int j = 0; j < addedUsers.size(); j++) {
					if (this.usersTarget.get(i).getNameSurname().equals(addedUsers.get(j).getNameSurname())) {
						FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, addedUsers.get(j).getNameSurname()
								+ " already is assigned as action owner! This user cannot be added as read only action owner! ",
								null);
						removedReadOnlyTarget.add(addedUsers.get(j));
						return;
					}
				}
			}
			for (int i = 0; i < this.readOnlyActionOwnersTarget.size(); i++) {
				for (int j = 0; j < addedUsers.size(); j++) {
					if (this.readOnlyActionOwnersTarget.get(i).getNameSurname()
							.equals(addedUsers.get(j).getNameSurname())) {
						FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, addedUsers.get(j).getNameSurname()
								+ " already is assigned as read only action owner! This user cannot be added as action owner! ",
								null);
						return;
					}
				}
			}
			for (User user : addedUsers) {
				if (!"readOnly".equals(event.getComponent().getAttributes().get("type"))) {
					FacesUtils.addMessage(FacesMessage.SEVERITY_INFO,
							user.getNameSurname() + " assigned as action owner", null);
				} else {
					FacesUtils.addMessage(FacesMessage.SEVERITY_INFO,
							user.getNameSurname() + " assigned as read only action owner", null);
				}
			}

		} else if (event.isRemove()) {
			List<User> removedUsers = (List<User>) event.getItems();
			for (User user : removedUsers) {
				if (!"readOnly".equals(event.getComponent().getAttributes().get("type"))) {
					FacesUtils.addMessage(FacesMessage.SEVERITY_INFO,
							user.getNameSurname() + " removed from action owners", null);
					 FindingAction findingAct = finding.getFindingAction(user);
					 findingAct.setActionOwnerFlag(false);
					usersTarget.remove(user);
					this.flag = 0;
				} else {
					FacesUtils.addMessage(FacesMessage.SEVERITY_INFO,
							user.getNameSurname() + " removed from read only action owners", null);
					finding.getFindingAction(user).setReadOnly(false);
					readOnlyActionOwnersTarget.remove(user);
					this.flag = 0;
				}
			}
		}
	}

	public void spocPickListOnTransfer(TransferEvent event) {
		if (event.isAdd()) {
			List<User> addedUsers = (List<User>) event.getItems();
			for (User user : addedUsers) {
				possibleSPOCs.getTarget().add(user);
				
				FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, user.getNameSurname() + " assigned as spoc", null);
			}
		} else if (event.isRemove()) {
			List<User> removedUsers = (List<User>) event.getItems();
			for (User user : removedUsers) {
				FindingAction findingAct = finding.getFindingAction(user);
				findingAct.setSpoc(false);
				getPossibleSPOCs().getTarget().remove(user);
				FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, user.getNameSurname() + " removed from spocs", null);
				this.flag = 0;
			}
		}
	}

//	public void validateCompletionDate(FacesContext context, UIComponent component, Object value)
//			throws ValidatorException {
//		if (this.finding != null && this.finding.getId() == null) {
//			Date today = new Date();
//			Date completionDate = (Date) value;
//			if (completionDate.before(today)) {
//				FacesMessage msg = new FacesMessage("Completion date must be after today",
//						"Completion date must be after today");
//				msg.setSeverity(FacesMessage.SEVERITY_ERROR);
//				((UIInput) component).setValid(false);
//				throw new ValidatorException(msg);
//			}
//		}
//	}

	public void handleFileUpload(FileUploadEvent fileUploadEvent) {
		findingFileController.handleFileUpload(fileUploadEvent.getFile(), this.finding, this.user);
	}
	
	public Finding generateActionOwnersandGeneralInfo(Finding f ) {
			for (FindingAction findingAction : f.findingActionsAsList()) {
				
				try {
					if (findingAction.isReadOnly()) {
						if (f.getActionOwners() != null && !f.getActionOwners().contains(findingAction.getActionOwner().getNameSurname())) {
							f.setActionOwners(f.getActionOwners() + " • " + findingAction.getActionOwner().getNameSurname()
									+ " [Read Only]\n");
						} else {
							f.setActionOwners(" • " + findingAction.getActionOwner().getNameSurname() + " [Read Only]\n");
						}
					} else if (findingAction.isSpoc() ) {
						if (f.getActionOwners() != null && !f.getActionOwners().contains(findingAction.getActionOwner().getNameSurname())) {
							
								f.setActionOwners(f.getActionOwners() + " • " + findingAction.getActionOwner().getNameSurname()
										+ " [SPOC]\n");	
						
						} else {
							f.setActionOwners(" • " + findingAction.getActionOwner().getNameSurname() + " [SPOC]\n");
						}
						
					} else {
						if (f.getActionOwners() != null && !f.getActionOwners().contains(findingAction.getActionOwner().getNameSurname())) {
							f.setActionOwners(
									f.getActionOwners() + " • " + findingAction.getActionOwner().getNameSurname() + "\n");
						} else {
							f.setActionOwners(" • " + findingAction.getActionOwner().getNameSurname() + "\n");
						}
					}				
				} catch (Exception e) {
					
					e.printStackTrace();
				}
			}

			
			f.setGeneralInfo(f.getAuditType().getDescription() + "\n" + f.getBusinessProcessType().getDescription()
					+ "\n" + f.getBusinessSubProcessType().getDescription() + "\n" + f.getReportName());
			
			setFindingInfo(f);
			return f;
		}

	private Finding setFindingInfo(Finding f) {
		f = commanFunctionsService.updatePercentageOfFinding(f);
		f = commanFunctionsService.updateStatus(f);
		f = commanFunctionsService.updatePeriod(f);
		return f;
	}
		
	
	
	

}
