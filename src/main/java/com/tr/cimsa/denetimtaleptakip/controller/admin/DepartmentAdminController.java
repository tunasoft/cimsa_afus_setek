package com.tr.cimsa.denetimtaleptakip.controller.admin;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.DualListModel;

import com.google.common.collect.Lists;
import com.tr.cimsa.denetimtaleptakip.controller.util.FacesUtils;
import com.tr.cimsa.denetimtaleptakip.model.Department;
import com.tr.cimsa.denetimtaleptakip.model.Finding;
import com.tr.cimsa.denetimtaleptakip.model.User;

import lombok.Getter;
import lombok.Setter;

/**
 * User: mhazer Date: 8/30/12 Time: 12:50 PM
 */
@ManagedBean(name = "departmentAdminController")
@ViewScoped
public class DepartmentAdminController extends BaseAdminController implements Serializable {

	@Override
	protected Class getClazz() {
		// TODO Auto-generated method stub
		return DepartmentAdminController.class;
	}

	@Setter
	@Getter
	private Department selectedDepartmentType;

	@Getter
	@Setter
	private DualListModel<User> possibleHeaders;

	public void selectDepartment(Department department) {
	
		try {
			this.findingService.reattachReadOnly(department);
			this.selectedDepartmentType = department;
			preparePossibleHeadersDualListModel();
			adjustPossibleHeadersDualListModel();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

	}

	public void deleteDepartment(Department department) {

		try {
			SearchParameters searchParameters = new SearchParameters();
			List<Department> departmentTypeList = new ArrayList<>();
			departmentTypeList.add(department);
			searchParameters.setDepartments(departmentTypeList);

			List<Finding> findingList = this.findingService.searchFindings(searchParameters);

			if (findingList != null && !findingList.isEmpty()) {
				FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, "This deparment has findings", null);
			} else {
				findingService.deleteDepartment(department);
				this.commonDefinitionsController.refreshDepartments();
			}
		} catch (Exception e) {
			logger.error(" delete department {} Exception : "+e.getMessage());
		}

	}

 	private void preparePossibleHeadersDualListModel() {
		List<User> possibleHeadersSource = Lists.newLinkedList();
		List<User> possibleHeadersTarget = Lists.newLinkedList();
		possibleHeadersSource.addAll(commonDefinitionsController.getActiveUsers());
		possibleHeaders = new DualListModel<User>(possibleHeadersSource, possibleHeadersTarget);
	}

////
	private void adjustPossibleHeadersDualListModel() {

		for (User header : this.selectedDepartmentType.getDepartmentUpdateHeaders()) {
			if (possibleHeaders.getSource().contains(header)) {
				possibleHeaders.getSource().remove(header);
				possibleHeaders.getTarget().add(header);
			}
		}
	}

	public void addNewDepartment() {
		this.selectedDepartmentType = new Department();
		preparePossibleHeadersDualListModel();
	} 

	public void saveDepartment() {
		try {
			this.selectedDepartmentType.setAdminUser(visit.getUser());
			this.selectedDepartmentType.setEditDate(new Date());
			this.findingService.mergeDepartmentType(this.selectedDepartmentType, possibleHeaders.getTarget());
			this.commonDefinitionsController.refreshDepartments();
			FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, "Department was updated successfully", null);
		} catch (Exception exception) {
			logger.debug("saving department {} Exception :"+ exception.getMessage());
			FacesUtils.addMessage(FacesMessage.SEVERITY_INFO,
					"Can not save department (" + exception.getMessage() + ")", null);
		} finally {
			this.selectedDepartmentType = null;
		}
	}

}
