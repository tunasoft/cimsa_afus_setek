package com.tr.cimsa.denetimtaleptakip.controller.admin;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.hibernate.engine.jdbc.spi.SqlExceptionHelper;
import org.primefaces.model.DualListModel;
import com.google.common.collect.Lists;
import com.tr.cimsa.denetimtaleptakip.controller.util.FacesUtils;
import com.tr.cimsa.denetimtaleptakip.model.AuditType;
import com.tr.cimsa.denetimtaleptakip.model.BusinessProcessType;
import com.tr.cimsa.denetimtaleptakip.model.BusinessSubProcessType;
import com.tr.cimsa.denetimtaleptakip.model.Finding;
import com.tr.cimsa.denetimtaleptakip.model.User;

import lombok.Getter;
import lombok.Setter;

/**
 * User: mhazer
 * Date: 8/30/12
 * Time: 12:50 PM
 */
@ManagedBean(name = "businessTypeAdminController")
@ViewScoped
public class BusinessTypeAdminController extends BaseAdminController implements Serializable
{
    @Getter
    @Setter
    private BusinessProcessType selectedBusinessProcessType;

    @Getter
    @Setter
    private DualListModel<User> possibleBusinessProcessResposibleHeads;

    @Getter
    @Setter
    private DualListModel<User> possibleBusinessSubProcessResposibleHeads;
    
    @Getter
    @Setter
    private DualListModel<User> possibleBusinessProcessFunctionHeads;
    
    @Getter
    @Setter
    private DualListModel<User> possibleBusinessSubProcessFunctionHeads;     

    @Setter
    @Getter
    private BusinessSubProcessType selectedBusinessSubProcessType;

    public void selectBusinessProcessType(BusinessProcessType businessProcessType)
    {
        this.findingService.reattachReadOnly(businessProcessType);
        this.selectedBusinessProcessType = businessProcessType;
        this.selectedBusinessProcessType.getSubProcessTypes().size();
        prepareBusinessProcessReponsibleHeadsDualListModel();
        adjustBusinessProcessReponsibleHeadsDualListModel();
    }

    private void prepareBusinessProcessReponsibleHeadsDualListModel()
    {
        List<User> businessProcResonsibleHeadsSource = Lists.newLinkedList();
        List<User> businessProcResonsibleHeadsTarget = Lists.newLinkedList();
        businessProcResonsibleHeadsSource.addAll(commonDefinitionsController.getActiveUsers());
        possibleBusinessProcessResposibleHeads = new DualListModel<User>(businessProcResonsibleHeadsSource, businessProcResonsibleHeadsTarget);
        List<User> businessProcFunctionHeadsSource = Lists.newLinkedList();
        List<User> businessProcFunctionHeadsTarget = Lists.newLinkedList();
        businessProcFunctionHeadsSource.addAll(commonDefinitionsController.getActiveUsers());
        possibleBusinessProcessFunctionHeads = new DualListModel<>(businessProcFunctionHeadsSource, businessProcFunctionHeadsTarget);
    }

    private void adjustBusinessProcessReponsibleHeadsDualListModel()
    {
        for (User user : this.selectedBusinessProcessType.getResponsibleHeads())
        {
            if (possibleBusinessProcessResposibleHeads.getSource().contains(user))
            {
                possibleBusinessProcessResposibleHeads.getSource().remove(user);
                possibleBusinessProcessResposibleHeads.getTarget().add(user);
            }
        }
        for (User user : this.selectedBusinessProcessType.getFunctionHeads())
        {
        	if (possibleBusinessProcessFunctionHeads.getSource().contains(user))
        	{
        		possibleBusinessProcessFunctionHeads.getSource().remove(user);
        		possibleBusinessProcessFunctionHeads.getTarget().add(user);
        	}
        		
        }
    }

    public void addNewBusinessType()
    {
        this.selectedBusinessProcessType = new BusinessProcessType();
        prepareBusinessProcessReponsibleHeadsDualListModel();
    }

    public void cancelBusinessProcessOperation()
    {
        this.commonDefinitionsController.refreshBusinessTypes();
    }

    public void saveBusinessType()
    {
        try
        {
            this.selectedBusinessProcessType.setAdminUser(visit.getUser());
            this.selectedBusinessProcessType.setEditDate(new Date());
            this.findingService.mergeBusinessType(selectedBusinessProcessType, this.possibleBusinessProcessResposibleHeads.getTarget(), this.possibleBusinessProcessFunctionHeads.getTarget());
            this.commonDefinitionsController.refreshBusinessTypes();
            FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, "Business Type updated successfully", null);
        }
        catch (Exception exception)
        {
        	logger.debug("saving business type {} Exception :"+ exception.getMessage());
            FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, "Can not save Business Type (" + exception.getMessage() + ")", null);
        }
        finally
        {
            this.selectedBusinessProcessType = null;
        }
    }

    public void editBusinessSubProcessType(BusinessSubProcessType businessSubProcessType)
    {
        this.findingService.reattachReadOnly(businessSubProcessType);
        this.selectedBusinessSubProcessType = businessSubProcessType;
        prepareBusinessSubProcessReponsibleHeadsDualListModel();
        adjustBusinessSubProcessReponsibleHeadsDualListModel();
    }

    private void prepareBusinessSubProcessReponsibleHeadsDualListModel()
    {
        List<User> businessSubProcResonsibleHeadsSource = Lists.newLinkedList();
        List<User> businessSubProcResonsibleHeadsTarget = Lists.newLinkedList();
        businessSubProcResonsibleHeadsSource.addAll(commonDefinitionsController.getActiveUsers());
        possibleBusinessSubProcessResposibleHeads = new DualListModel<User>(businessSubProcResonsibleHeadsSource, businessSubProcResonsibleHeadsTarget);
        List<User> businessSubProcFunctionHeadsSource = Lists.newLinkedList();
        List<User> businessSubProcFunctionHeadsTarget = Lists.newLinkedList();
        businessSubProcFunctionHeadsSource.addAll(commonDefinitionsController.getActiveUsers());
        possibleBusinessSubProcessFunctionHeads = new DualListModel<>(businessSubProcFunctionHeadsSource, businessSubProcFunctionHeadsTarget);
        
    }

    private void adjustBusinessSubProcessReponsibleHeadsDualListModel()
    {
        for (User user : this.selectedBusinessSubProcessType.getResponsibleHeads())
        {
            if (possibleBusinessSubProcessResposibleHeads.getSource().contains(user))
            {
                possibleBusinessSubProcessResposibleHeads.getSource().remove(user);
                possibleBusinessSubProcessResposibleHeads.getTarget().add(user);
            }
        }
        for (User user : this.selectedBusinessSubProcessType.getFunctionHeads())
        {
            if (possibleBusinessSubProcessFunctionHeads.getSource().contains(user))
            {
            	possibleBusinessSubProcessFunctionHeads.getSource().remove(user);
            	possibleBusinessSubProcessFunctionHeads.getTarget().add(user);
            }
        }
    }

    public void newBusinessSubProcessType()
    {
        this.selectedBusinessSubProcessType = new BusinessSubProcessType();
        prepareBusinessSubProcessReponsibleHeadsDualListModel();
    }

    public void saveNewBusinessSubType()
    {
        this.selectedBusinessSubProcessType.setAdminUser(visit.getUser());
        this.selectedBusinessSubProcessType.setEditDate(new Date());
        this.selectedBusinessSubProcessType.getResponsibleHeads().addAll(this.possibleBusinessSubProcessResposibleHeads.getTarget());
        this.selectedBusinessSubProcessType.getFunctionHeads().addAll(this.possibleBusinessSubProcessFunctionHeads.getTarget());
        if (this.selectedBusinessSubProcessType.getId() == null)
        {
            this.selectedBusinessSubProcessType.setMainProcess(this.selectedBusinessProcessType);
            this.selectedBusinessProcessType.getSubProcessTypes().add(this.selectedBusinessSubProcessType);
        }
        else
        {
            for(Iterator iterator = this.selectedBusinessSubProcessType.getResponsibleHeads().iterator(); iterator.hasNext();)
            {
                if(!this.possibleBusinessSubProcessResposibleHeads.getTarget().contains(iterator.next()))
                {
                    iterator.remove();
                }
            }
            for(Iterator iterator = this.selectedBusinessSubProcessType.getFunctionHeads().iterator(); iterator.hasNext();)
            {
                if(!this.possibleBusinessSubProcessFunctionHeads.getTarget().contains(iterator.next()))
                {
                    iterator.remove();
                }
            }
        }
    }

	public void deleteBusinessProcessType(BusinessProcessType businessProcessType) {
		try {
			
			boolean isPermissionDelete = true;
			SearchParameters searchParameters = new SearchParameters();
			List<BusinessProcessType> businessProcessTypesList = new ArrayList<>();
			businessProcessTypesList.add(businessProcessType);
			searchParameters.setBusinessProcessTypes(businessProcessTypesList);

			List<Finding> findingList = this.findingService.searchFindings(searchParameters);

			if (findingList != null && !findingList.isEmpty()) {
				FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, "This main process type has findings.", null);
				isPermissionDelete = false;
			} else {
				
				List<AuditType> auditTypeList =  findingService.retrieveAllActiveAuditTypes();
				for (AuditType at : auditTypeList) {

					for (BusinessProcessType bp : at.getBusinessProcessTypes()) {
						if (bp.getDescription().equals(businessProcessType.getDescription())) {
							FacesUtils.addMessage(FacesMessage.SEVERITY_INFO,
									"This main process type using by audit type (" + at.getDescription() + ")", null);
							isPermissionDelete = false;
							break;
						}
					}
				}
			}
			
			if (isPermissionDelete) {
				findingService.deleteBusinessProcessType(businessProcessType);
				this.commonDefinitionsController.refreshBusinessTypes();
			}

		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	public void deleteBusinessSubProcessType(BusinessSubProcessType businessSubProcessType) {
		try {
			SearchParameters searchParameters = new SearchParameters();
			List<BusinessSubProcessType> businessSubProcessTypesList = new ArrayList<>();
			businessSubProcessTypesList.add(businessSubProcessType);
			searchParameters.setBusinessSubProcessTypes(businessSubProcessTypesList);

			List<Finding> findingList = this.findingService.searchFindings(searchParameters);

			if (findingList != null && !findingList.isEmpty()) {
				FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, "This subprocess type have findings.", null);
			} else {
				findingService.deleteBusinessSubProcessType(businessSubProcessType);
				commonDefinitionsController.refreshBusinessTypes();
				selectedBusinessProcessType.getSubProcessTypes().remove(businessSubProcessType);
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

    @Override
    protected Class getClazz()
    {
        return BusinessTypeAdminController.class;
    }
}
