package com.tr.cimsa.denetimtaleptakip.controller;

import lombok.Getter;
import lombok.Setter;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.tr.cimsa.denetimtaleptakip.model.User;

import java.io.Serializable;

/**
 * User: mhazer
 * Date: 8/20/12
 * Time: 12:16 PM
 */
@ManagedBean(name = "visit")
@SessionScoped
public class Visit implements Serializable
{
	
    @Getter
    @Setter
    private User user;
}
