package com.tr.cimsa.denetimtaleptakip.controller.util;

import com.tr.cimsa.denetimtaleptakip.model.Finding;

import lombok.Data;

/**
 * Created by burak.acikgoz on 5/22/14.
 */
@Data
public class FindingImportWrapper {

    private int id;

    private Finding finding;

    private boolean valid;

    private String errorDetail;

}
