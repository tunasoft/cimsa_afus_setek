package com.tr.cimsa.denetimtaleptakip.controller.user;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.LazyDataModel;

import com.tr.cimsa.denetimtaleptakip.controller.Visit;
import com.tr.cimsa.denetimtaleptakip.model.Finding;
import com.tr.cimsa.denetimtaleptakip.services.FindingService;

import lombok.Getter;
import lombok.Setter;

/**
 * User: mhazer
 * Date: 8/21/12
 * Time: 1:05 PM
 */
@ManagedBean(name = "departmentFindingsController")
@ViewScoped
public class DepartmentFindingsController implements Serializable
{
    @ManagedProperty("#{findingService}")
    @Setter
    private FindingService findingService;

    @ManagedProperty("#{visit}")
    @Setter
    private Visit visit;
    
    @Getter
	private List<Finding> findings;
    
   
	
	@Getter
	@Setter
	private List<Finding> filteredFindings;
	
	@Getter 
	private LazyDataModel<Finding> departmentFindingLazyDataModel; 


	@PostConstruct
	public void init() {
		
		Long start = System.currentTimeMillis();
		findings = new ArrayList<Finding>();
		
		
		departmentFindingLazyDataModel =  new DepartmentFindingsLazyModel(findingService, visit);

		
//		for (Finding finding : this.findings)
//			finding.getFindingFiles().size();

    	

	}
}
