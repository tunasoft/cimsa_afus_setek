package com.tr.cimsa.denetimtaleptakip.controller.user;

import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.hibernate.type.StringClobType;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.chart.PieChartModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;
import com.tr.cimsa.denetimtaleptakip.controller.Visit;
import com.tr.cimsa.denetimtaleptakip.controller.admin.FindingAdminController;
import com.tr.cimsa.denetimtaleptakip.controller.admin.FindingFileController;
import com.tr.cimsa.denetimtaleptakip.controller.admin.NumberOfFindingsPerStatusGraphicalReportController;
import com.tr.cimsa.denetimtaleptakip.controller.util.FacesUtils;
import com.tr.cimsa.denetimtaleptakip.model.Finding;
import com.tr.cimsa.denetimtaleptakip.model.FindingAction;
import com.tr.cimsa.denetimtaleptakip.model.FindingResponsibleAction;
import com.tr.cimsa.denetimtaleptakip.model.Role;
import com.tr.cimsa.denetimtaleptakip.model.Status;
import com.tr.cimsa.denetimtaleptakip.model.User;
import com.tr.cimsa.denetimtaleptakip.services.FindingExcelService;
import com.tr.cimsa.denetimtaleptakip.services.FindingService;
import com.tr.cimsa.denetimtaleptakip.services.MailService;

import lombok.Getter;
import lombok.Setter;

/**
 * User: mhazer Date: 8/21/12 Time: 1:05 PM
 */
@ManagedBean(name = "userTasksController")
@ViewScoped
public class UserTasksController implements Serializable {
	private final static Logger LOGGER = LoggerFactory.getLogger(UserTasksController.class);

	@ManagedProperty("#{findingService}")
	private FindingService findingService;

	@ManagedProperty("#{findingExcelService}")
	private FindingExcelService findingExcelService;

	@ManagedProperty("#{mailService}")
	@Setter
	private MailService mailService;

	@Getter
	private PieChartModel pieChartModel;

	@ManagedProperty("#{visit}")
	private Visit visit;
	@ManagedProperty("#{visit.user}")
	@Setter
	private User user;
	@ManagedProperty("#{findingFileController}")
	@Setter
	private FindingFileController findingFileController;

	@Getter
	@Setter
	private List<User> currentActionOwners;

	@Getter
	@Setter
	private List<User> SPOCActionOwners;

	@Getter
	private FindingAction selectedFindingAction;

	@Getter
	public Finding selectedFinding;

	@Getter
	public boolean isEditAsUser;

	@Getter
	@Setter
	private String[] selectedActionOwners;

	@Getter
	@Setter
	private FindingResponsibleAction selectedFindingResponsibleAction;

	@Getter
	private List<FindingAction> findingActions;

	@Getter
	@Setter
	private List<FindingAction> selectedFindingActions;

	@Getter
	private List<Finding> findings;

	@Getter
	@Setter
	private List<Finding> filteredFindingActions;

	@ManagedProperty("#{findingAdminController}")
	@Setter
	private FindingAdminController findingAdminController;

	private boolean userHasEditForSpoc;

	private Finding currentFinding;

	@Getter 
	private FindingActionsLazyDataModel findingActionsLazyDataModel; 
	
	@PostConstruct
	public void init() {
			
			findingActionsLazyDataModel = new FindingActionsLazyDataModel(findingService, visit);
	}

	public void selectFindingAction(FindingAction findingAction) {
		this.isEditAsUser = false;
		this.selectedFindingAction = this.findingService.retrieveFindingActionWithHistory(findingAction.getId());
		this.selectedFinding = this.selectedFindingAction.getFinding();
this.selectedFinding = this.selectedFindingAction.getFinding(); 



	}

	public void action() throws IOException {
		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		ec.redirect(((HttpServletRequest) ec.getRequest()).getRequestURI());
	}

	public void selectFindingForEditUserActions(FindingAction findingAction) {
		userHasEditForSpoc = findingAction.isSpoc();
		Finding findingOrj = findingAction.getFinding();
		this.isEditAsUser = true;
		currentFinding = findingService.retrieveFinding(findingOrj.getId());
		this.selectedFinding = currentFinding;
		this.selectedFindingAction = new FindingAction();
		this.selectedFindingAction.setActionTaken("");
		this.selectedFindingAction.setFinding(currentFinding);

		this.selectedFindingActions = currentFinding.findingActionsAsList();

		List<FindingAction> list = currentFinding.getFindingActions();
		currentActionOwners = new ArrayList<>();
		SPOCActionOwners = new ArrayList<>();

		for (FindingAction myFindAct : list) {
			if (myFindAct.isActionOwnerFlag()) {
				if (!this.visit.getUser().getNameSurname().equals(myFindAct.getActionOwner().getNameSurname())) {
					currentActionOwners.add(myFindAct.getActionOwner());
				}
			} else if (myFindAct.isSpoc()) {
				if (this.visit.getUser().getNameSurname().equals(myFindAct.getActionOwner().getNameSurname())) {
					SPOCActionOwners.add(myFindAct.getActionOwner());
				}
			}
		}
	}

//	public void updateFindingActions() {
//		int flag;
//		boolean atLeastOneActionOwnerFilled = !isAtLeastOneActionOwnerSelected();
//		try {
//			Preconditions.checkArgument(atLeastOneActionOwnerFilled,
//					"In order to perform update you should select at least one action owner!");
//		} catch (IllegalArgumentException e) {
//			LOGGER.debug(e.getMessage());
//			FacesUtils.addMessage(FacesMessage.SEVERITY_ERROR,
//					"In order to perform update you should select at least one action owner!", null);
//			e.printStackTrace();
//			return;
//		}
//		flag = 0;
//		for (Role r : this.user.getRoles()) {
//			if (r.getName().equals("ROLE_ADMIN"))
//				flag = 1;
//		}
//
//		if (flag == 0 && !userHasEditForSpoc) {
//			LOGGER.debug("You do not have permission to do this!");
//			FacesUtils.addMessage(FacesMessage.SEVERITY_ERROR, "You do not have permission to do this!", null);
//			return;
//		}
//		try {
//			List<FindingAction> sendEmailList = new ArrayList<>();
//			for (FindingAction action : this.selectedFindingActions) {
//				for (String myUser : selectedActionOwners) {
//
//					if ((!action.isReadOnly()) && (myUser.equalsIgnoreCase(action.getActionOwner().getNameSurname()))) {
//						String maximumRevisedDateinString = selectedFinding.getAuditType().getMaximumRevisedDate();
//						String tokens[] = maximumRevisedDateinString.split(" ");
//						if (tokens[0].contains("+")) {
//
//							sendEmailList.add(action);
//							FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, "Finding status is updated successfully",
//									null);
//						} else {
//
//							int maximumRevised = Integer.parseInt(tokens[0]);
//							Calendar calendar = Calendar.getInstance();
//							calendar.setTime(action.getFinding().getCompletionDate());
//							calendar.add(Calendar.MONTH, maximumRevised);
//							Date date = calendar.getTime();
//							if (this.selectedFindingAction.getRevisedCompletionDate() != null
//									&& this.selectedFindingAction.getRevisedCompletionDate().compareTo(date) == 1) {
//								FacesUtils.addMessage(FacesMessage.SEVERITY_INFO,
//										"Unsuccess because Maximum revised date time is " + maximumRevised + " months",
//										null);
//								break;
//							} else {
//
//								action.setActionTaken("[SPOC :" + user.getNameSurname() + " tarafından] "
//										+ this.selectedFindingAction.getActionTaken());
//								action.setCompletionPercentage(this.selectedFindingAction.getCompletionPercentage());
//								action.setExplanationForDelay(this.selectedFindingAction.getExplanationForDelay());
//
//								action.setRevisedCompletionDate(this.selectedFindingAction.getRevisedCompletionDate());
//
//								if (action.getRevisedCompletionDate() != null
//										&& action.getRevisedCompletionDate().compareTo(date) == 1) {
//									FacesUtils.addMessage(FacesMessage.SEVERITY_INFO,
//											"Unsuccess because Maximum revised date time is " + maximumRevised
//													+ " months",
//											null);
//									break;
//								} else {
//
//									action.setChangeDate(new Date());
//									sendEmailList.add(action);
//								}
//								
//								if (selectedFindingAction.getRevisedCompletionDate() != null) {
//									selectedFinding.setRevised(false);
//									selectedFinding.setRevisedCompletionDate(selectedFindingAction.getRevisedCompletionDate());
//								}
//
//								selectedFinding = updatePertenceOfFinding(this.selectedFinding);
//								selectedFinding.setCompleted(selectedFinding.getCompletionPercentage() == 100 ? true : false);
//								selectedFinding = updateStatus(selectedFinding);
//
//								findingService.mergeFinding(selectedFinding);
//
//								for (FindingAction findingAction : sendEmailList) {
//									this.sendUpdateEmails(findingAction);
//								}
//
//								FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, "Finding status is updated successfully", null);
//
//							}
//						}
//					}
//				}
//			}		
//			if (!findingFileController.getFindingFiles().isEmpty() && findingFileController.getFinding() != null) {
//				findingService.removeFindingFile(findingFileController.getFindingFiles(),
//						findingFileController.getFinding());
//			}
//		} catch (Exception exception) {
//			exception.printStackTrace();
//			FacesUtils.addMessage(FacesMessage.SEVERITY_ERROR,
//					"Finding status update is failed: (" + exception.getMessage() + ")", null);
//		} finally {
//			this.selectedFindingAction = null;
//			this.selectedFinding = null;
//			this.init();
//		}
//	}
///*	
	public void updateFindingActions() {
		int flag;
		boolean atLeastOneActionOwnerFilled = !isAtLeastOneActionOwnerSelected();
		try {
			Preconditions.checkArgument(atLeastOneActionOwnerFilled,
					"In order to perform update you should select at least one action owner!");
		} catch (IllegalArgumentException e) {
			LOGGER.debug(e.getMessage());
			FacesUtils.addMessage(FacesMessage.SEVERITY_ERROR,
					"In order to perform update you should select at least one action owner!", null);
			e.printStackTrace();
			return;
		}
		flag = 0;
		for (Role r : this.user.getRoles()) {
			if (r.getName().equals("ROLE_ADMIN"))
				flag = 1;
		}

		if (flag == 0 && !userHasEditForSpoc) {
			LOGGER.debug("You do not have permission to do this!");
			FacesUtils.addMessage(FacesMessage.SEVERITY_ERROR, "You do not have permission to do this!", null);
			return;
		}
		
		try {

			String maximumRevisedDateInString = selectedFinding.getAuditType().getMaximumRevisedDate();
			String tokens[] = maximumRevisedDateInString.split(" ");
			int maximumRevised = Integer.parseInt(tokens[0]);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(this.selectedFinding.getCompletionDate());
			calendar.add(Calendar.MONTH, maximumRevised);
			Date maximumRevisedDate = calendar.getTime();
			if (this.selectedFindingAction.getRevisedCompletionDate() != null
					&& this.selectedFindingAction.getRevisedCompletionDate().after(maximumRevisedDate)) {
				FacesUtils.addMessage(FacesMessage.SEVERITY_INFO,
						"Unsuccess because Maximum revised date time is " + maximumRevised + " months", null);
				return;
			}

			List<FindingAction> sendEmailList = new ArrayList<>();
			for (FindingAction action : this.selectedFindingActions) {
				for (String myUser : selectedActionOwners) {

					if ((!action.isReadOnly()) && (myUser.equalsIgnoreCase(action.getActionOwner().getNameSurname()))) {

						action.setActionTaken("[SPOC :" + user.getNameSurname() + " tarafından] "
								+ this.selectedFindingAction.getActionTaken());
						action.setCompletionPercentage(this.selectedFindingAction.getCompletionPercentage());
						action.setExplanationForDelay(this.selectedFindingAction.getExplanationForDelay());

						action.setRevisedCompletionDate(this.selectedFindingAction.getRevisedCompletionDate());

						action.setChangeDate(new Date());
						sendEmailList.add(action);
					}
				}
			}

			if (selectedFindingAction.getRevisedCompletionDate() != null) {
				selectedFinding.setRevised(false);
				selectedFinding.setRevisedCompletionDate(selectedFindingAction.getRevisedCompletionDate());
			}

			selectedFinding = updatePertenceOfFinding(this.selectedFinding);
			selectedFinding.setCompleted(selectedFinding.getCompletionPercentage() == 100 ? true : false);
			selectedFinding = updateStatus(selectedFinding);

			findingService.mergeFinding(selectedFinding);

			for (FindingAction findingAction : sendEmailList) {
				this.sendUpdateEmails(findingAction);
			}



			if (!findingFileController.getFindingFiles().isEmpty() && findingFileController.getFinding() != null) {
				findingService.removeFindingFile(findingFileController.getFindingFiles(),
						findingFileController.getFinding());
			}
			
			FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, "Finding status is updated successfully", null);
		} catch (Exception exception) {
			exception.printStackTrace();
			FacesUtils.addMessage(FacesMessage.SEVERITY_ERROR,
					"Finding status update is failed: (" + exception.getMessage() + ")", null);
		} finally {
			this.selectedFindingAction = null;
			this.selectedFinding = null;
			this.init();
		}
	}

//*/	

	public void updateFindingAction(String maximumRevisedDate) {
		LOGGER.debug("UserTasksController.updateFindingAction()");


		try {
//				checkFindingActionValues();
			if (selectedFindingAction.getFinding().getCompleted()
					&& selectedFindingAction.getFinding().getConfirmed()) {
				FacesUtils.addMessage(FacesMessage.SEVERITY_WARN,
						"Finding is already confirmed/completed. Finding status can not be updated.", null);
			} else {
				selectedFindingAction.setChangeDate(new Date());
				String maximumRevisedDateinString = this.selectedFinding.getAuditType().getMaximumRevisedDate();
				String tokens[] = maximumRevisedDateinString.split(" ");
				if (tokens[0].contains("+")) {
					
					
					if(selectedFindingAction.getRevisedCompletionDate() != null){
						selectedFinding.setRevised(false);
			            selectedFinding.setRevisedCompletionDate(selectedFindingAction.getRevisedCompletionDate());
			        }
					
					selectedFinding = updatePertenceOfFinding(this.selectedFinding);
					selectedFinding.setCompleted( selectedFinding.getCompletionPercentage() == 100 ? true : false);
					selectedFinding	= updateStatus(selectedFinding);
					
					findingService.mergeFinding(selectedFinding);
					
					this.sendUpdateEmails(selectedFindingAction);
					FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, "Finding status is updated successfully", null);
				} else {

					int maximumRevised = Integer.parseInt(tokens[0]);
					Calendar calendar = Calendar.getInstance();
					calendar.setTime(selectedFindingAction.getFinding().getCompletionDate());
					calendar.add(Calendar.MONTH, maximumRevised);
					Date maxRevisedDate = calendar.getTime();

					if (this.selectedFindingAction.getRevisedCompletionDate() != null
							&& this.selectedFindingAction.getRevisedCompletionDate().after(maxRevisedDate)){
						FacesUtils.addMessage(FacesMessage.SEVERITY_INFO,
								"Unsuccess because Maximum revised date time is " + maximumRevised + " months", null);
					}else if( this.selectedFindingAction.getRevisedCompletionDate() != null
							&& this.selectedFindingAction.getRevisedCompletionDate().before(selectedFindingAction.getFinding().getAuditDate())) {
						FacesUtils.addMessage(FacesMessage.SEVERITY_INFO,
								"You can not get revised date before entry date. Entry Date : " + new SimpleDateFormat("yyyy-MM-dd").format(selectedFindingAction.getFinding().getAuditDate()) , null);
					}
					else {

						
						if(selectedFindingAction.getRevisedCompletionDate() != null){
							selectedFinding.setRevised(false);
				            selectedFinding.setRevisedCompletionDate(selectedFindingAction.getRevisedCompletionDate());
				        }
						
						selectedFinding = updatePertenceOfFinding(this.selectedFinding);
						selectedFinding.setCompleted( selectedFinding.getCompletionPercentage() == 100 ? true : false);
						selectedFinding	= updateStatus(selectedFinding);
						
						findingService.mergeFinding(selectedFinding);
						
						this.sendUpdateEmails(selectedFindingAction);
						FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, "Finding status is updated successfully",
								null);
					}
				}
				if (!findingFileController.getFindingFiles().isEmpty() && findingFileController.getFinding() != null) {
					findingService.removeFindingFile(findingFileController.getFindingFiles(),
							findingFileController.getFinding());
				}
			}
			
		} catch (Exception exception) {
			exception.printStackTrace();
			FacesUtils.addMessage(FacesMessage.SEVERITY_ERROR,
					"Finding status update is failed: (" + exception.getMessage() + ")", null);
		} finally {
			this.selectedFindingAction = null;
			this.selectedFinding = null;
			this.init();
		}
	}

	public boolean isAtLeastOneActionOwnerSelected() {
		return CollectionUtils.sizeIsEmpty(selectedActionOwners);
	}

	public Finding updatePertenceOfFinding(Finding finding) {

		// her finding için yüzde hesaplıyor ve f set action owners spoc read only set
		// edilmiş
		int numberOfRealActOwners = 0;
		int sumOfFindingActionCompletionPercentages = 0;

		for (FindingAction findingAction : finding.findingActionsAsList()) {

			if (findingAction.isActionOwnerFlag()) {
				numberOfRealActOwners++;
				sumOfFindingActionCompletionPercentages = sumOfFindingActionCompletionPercentages
						+ findingAction.getCompletionPercentage();
			}

		}

		try {
			finding.setCompPercentage(
					Integer.toString(sumOfFindingActionCompletionPercentages / numberOfRealActOwners));
		} catch (ArithmeticException e) {
			finding.setCompPercentage("0");
		}

		return finding;

	}
	
	public Finding updateStatus(Finding f) {
		Date today = new Date();
		if (Integer.parseInt(f.getCompPercentage()) == 100) {
			if (f.getConfirmed())
				f.setStatus(Status.CONFIRMED_COMPLETED);
			else
				f.setStatus(Status.UNCONFIRMED_COMPLETED);
		} else if (Integer.parseInt(f.getCompPercentage()) > 0 && Integer.parseInt(f.getCompPercentage()) < 100) {
			if (today.before(f.getCompletionDate()))
				f.setStatus(Status.IN_PROGRESS);
			else
				f.setStatus(Status.DELAYED);
		} else if (Integer.parseInt(f.getCompPercentage()) == 0) {
			if (today.before(f.getCompletionDate()))
				f.setStatus(Status.NOT_STARTED);
			else
				f.setStatus(Status.DELAYED);
		}
//		f.getStatus();
		return f;
	}
	

	

	public void updateFindingComments() {
		try {
			final Finding finding = this.selectedFindingAction.getFinding();

			if (finding != null && finding.getId() != null) {
				LOGGER.debug("finding is: {}", finding.getFindingNumber());
				this.findingService.mergeFinding(finding);
				FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, "Finding comments are updated successfully", null);
			} else {
				FacesUtils.addMessage(FacesMessage.SEVERITY_ERROR, "Finding is not found.", null);
			}
		} catch (Exception exception) {
			exception.printStackTrace();
			FacesUtils.addMessage(FacesMessage.SEVERITY_ERROR,
					"Finding comments update is failed: (" + exception.getMessage() + ")", null);
		} finally {
			this.selectedFindingAction = null;
			this.selectedFinding = null;
			this.init();
		}
	}

	private void sendUpdateEmails(FindingAction findingAction) {
		this.mailService.sendFindingActionUpdateEmail(findingAction);
	}

//	private void checkFindingActionValues() {
//		if (this.selectedFindingAction.getCompletionPercentage() != 100) {
//			if (this.selectedFindingAction.getRevisedCompletionDate() != null
//					&& this.selectedFindingAction.getRevisedCompletionDate().before(new Date())) {
//				// throw new IllegalArgumentException("You need to choose revised date after
//				// today");
//			}
//		}
//		/**
//		 * 15.10.2018 Durhasan kapatıldı if(
//		 * this.selectedFindingAction.getCompletionPercentage() == 100 &&
//		 * this.selectedFindingAction.getRevisedCompletionDate() != null ) { throw new
//		 * IllegalArgumentException("Bulgu tamamlanma yüzdesi %100 olduğundan revize
//		 * tamamlanma tarihine ihtiyaç yoktur."); }
//		 * 
//		 * if( this.selectedFinding.getCompletionPercentage()>0 &&
//		 * this.selectedFinding.getCompletionPercentage()<100 &&
//		 * this.selectedFindingAction.getRevisedCompletionDate() == null) { throw new
//		 * IllegalArgumentException("Lütfen Revize Tamamlanma Tarihi Giriniz."); }
//		 **/
//
//	}

	public void handleFileUpload(FileUploadEvent fileUploadEvent) {
		findingFileController.handleFileUpload(fileUploadEvent.getFile(), this.selectedFindingAction.getFinding(),
				this.visit.getUser());
	}

	public void setFindingService(FindingService findingService) {
		this.findingService = findingService;
	}

	public void setFindingExcelService(FindingExcelService findingExcelService) {
		this.findingExcelService = findingExcelService;
	}

	public void setVisit(Visit visit) {
		this.visit = visit;
	}

	public void exportFindingsToExcel(List<Finding> findings) {
		Workbook workbook = this.findingExcelService.prepareExcelReportOfTheFindings(findings, this.visit.getUser());
		LOGGER.debug("workbook size is {}", workbook.getSheet("Findings").getLastRowNum());
		sendFileToBrowser(workbook);
	}

	public String sendFileToBrowser(Workbook workbook) {
		try {
			String filename = "findings.xls";

			FacesContext facesContext = FacesContext.getCurrentInstance();
			ExternalContext externalContext = facesContext.getExternalContext();
			externalContext.setResponseContentType("application/vnd.ms-excel");

			// http://stackoverflow.com/questions/3592058/how-to-send-byte-as-pdf-to-browser-in-java-web-application
			// attachment (pops up a "Save As" dialogue) or inline (let the web browser
			// handle the display itself)
			externalContext.setResponseHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");

			workbook.write(externalContext.getResponseOutputStream()); // get workbook
			facesContext.responseComplete(); // if I don't call responseComplete() => IllegalStateException

			return null; // remain on same page
		} catch (Exception e) {
			// handle exception...
			return null; // remain on same page
		}
	}

	public void preparePieChart(List<Finding> findings) {
		LOGGER.debug("preparing pie chart");
		try {
			pieChartModel = NumberOfFindingsPerStatusGraphicalReportController.preparePieChart(findings);
		} catch (Exception exception) {
			exception.printStackTrace();
			FacesUtils.addMessage(FacesMessage.SEVERITY_ERROR,
					"We couldn't prepare the graph of findings" + exception.getMessage(), null);
		}
	}
	
	public void selectFindingResponsibleAction(FindingResponsibleAction findingResponsibleAction) {
		findingResponsibleAction.setHistoryOfFindingResponsibleAction(
				this.findingService.retrieveHistoryOfFindingResponsibleAction(findingResponsibleAction.getId()));
		this.selectedFindingResponsibleAction = findingResponsibleAction;
	}
}
