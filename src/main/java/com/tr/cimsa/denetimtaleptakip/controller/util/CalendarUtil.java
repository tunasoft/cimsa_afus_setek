package com.tr.cimsa.denetimtaleptakip.controller.util;

import com.google.common.base.Preconditions;
import com.sun.org.apache.xerces.internal.jaxp.datatype.XMLGregorianCalendarImpl;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.Minutes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author mbeytorun
 *
 */
public class CalendarUtil {

    private static final Logger logger = LoggerFactory.getLogger(CalendarUtil.class);

		
	public static boolean stringToDatesCompare(String firstDate,String secondDate){
		DateTimeFormatter format = DateTimeFormat.forPattern("yyyy-MM-dd' 'HH:mm:ss.SSSSSSSS").withZone(DateTimeZone.UTC);
		DateTime fDate = format.parseDateTime(firstDate);
		DateTime sDate = format.parseDateTime(secondDate);
		return fDate.isAfter(sDate);	
	}
	
	public static boolean dateToStringDateCompare(Date firstDate,String secondDate){
		DateTimeFormatter format = DateTimeFormat.forPattern("yyyy-MM-dd' 'HH:mm:ss.SSSSSSSS").withZone(DateTimeZone.UTC);
		DateTime sDate = format.parseDateTime(secondDate);
		return firstDate.after(sDate.toDate());
	}
	
	public static Date stringToDate(String date){
		return stringToDate(date, "dd/MM/yyyy");
	}
	
	public static Date stringToDate(String date, String pattern){
		Preconditions.checkArgument(StringUtils.isNotEmpty(date),"Tarih boş olamaz");
		Preconditions.checkNotNull(pattern, "pattern null olamaz");
		DateTimeFormatter formatter = DateTimeFormat.forPattern(pattern);
		return formatter.parseDateTime(date).toDate();	
	}
	
    public static Date stringToDateUsingDateFormat(String date, String pattern)
    {
        Preconditions.checkArgument(StringUtils.isNotEmpty(date), "Tarih boş olamaz");
        Preconditions.checkNotNull(pattern, "pattern null olamaz");
        DateFormat dateFormat = new SimpleDateFormat(pattern);
        Date d = null;
        try
        {
            d = dateFormat.parse(date);
        }
        catch (ParseException e)
        {
            logger.error("Sistemde beklenmeyen hata olustu:",e);
        }
        return d;
    }

	public static String dateToString(Date date){
		if(date!=null){
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		   	return df.format(date);
		}else{
			return StringUtils.EMPTY;
		}
	}
	
	public static String dateToStringWithPattern(Date date, String pattern){
		if(date!=null){
			DateFormat df = new SimpleDateFormat(pattern);
		   	return df.format(date);
		}else{
			return StringUtils.EMPTY;
		}
	}
	
	public static String bankaContactHistoryLogFormatWithSeconds(Date tarih){
		Preconditions.checkNotNull(tarih,"Tarih boş olamaz");
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss.SSS");
	   	return df.format(tarih);
	}
	
	public static String bankaContactHistoryLogFormat(Date tarih){
		Preconditions.checkNotNull(tarih,"Tarih boş olamaz");
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH.mm.ss");
	   	return df.format(tarih);
	}
	
	public static Date dateTimeStringToDate(String date){
		Preconditions.checkArgument(StringUtils.isNotEmpty(date),"Tarih boş olamaz");
		DateTime tarih = null;
		try {
			tarih = DateTimeFormat.forPattern("yyyy-MM-dd' 'HH:mm:ss.SSSSSSSS").withZone(DateTimeZone.UTC).parseDateTime(date);
		}catch(Exception e){
			logger.error("Sistemde beklenmeyen hata olustu:" ,e);
		}
		return tarih.toDate();
	}
	
	public static LocalDate stringToLocalDate(String date, String pattern){
		Preconditions.checkNotNull(date, "tarih null olamaz");
		Preconditions.checkNotNull(pattern, "pattern null olamaz");
		DateTimeFormatter formatter = DateTimeFormat.forPattern(pattern).withZone(DateTimeZone.UTC);
		return formatter.parseDateTime(date).toLocalDate();		
	}
	
    public static LocalDate dateToLocalDateWithDefaultPattern(Date date)
    {
        Preconditions.checkNotNull(date, "tarih null olamaz");
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy");
        return formatter.parseDateTime(sdf.format(date)).toLocalDate();
    }
	
	public static LocalDate stringToLocalDateWithDefaultPattern(String date){
		Preconditions.checkNotNull(date, "tarih null olamaz");	
		DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy");
		return formatter.parseDateTime(date).toLocalDate();		
	}
	
	public static int ikiTarihArasindakiGunFarki(String ilkTarih, String ikinciTarih){
		LocalDate _ilkTarih = stringToLocalDateWithDefaultPattern(ilkTarih);
		LocalDate _ikinciTarih = stringToLocalDateWithDefaultPattern(ikinciTarih);
		return ikiTarihArasindakiGunFarki(_ilkTarih, _ikinciTarih);
	}

    public static int ikiTarihArasindakiGunFarki(LocalDate ilkTarih, LocalDate ikinciTarih){
        Preconditions.checkNotNull(ilkTarih, "ilk tarih null olamaz");
        Preconditions.checkNotNull(ikinciTarih, "ikinci tarih null olamaz");
        return Days.daysBetween(ilkTarih, ikinciTarih).getDays();
    }

    public static int ikiTarihArasindakiAyFarki(Date ilkTarih, Date ikinciTarih){
        Preconditions.checkNotNull(ilkTarih, "ilk tarih null olamaz");
        Preconditions.checkNotNull(ikinciTarih, "ikinci tarih null olamaz");
        LocalDate _ilkTarih = new LocalDate(ilkTarih);
        LocalDate _ikinciTarih = new LocalDate(ikinciTarih);
        Period monthWeekPeriod = new Period(_ilkTarih, _ikinciTarih, PeriodType.yearMonthDay().withYearsRemoved());
        int result = Math.abs(monthWeekPeriod.getMonths());
        return monthWeekPeriod.getDays() != 0 ? result + 1 : result; 
    }
	
	public static int sistemTarihiyleArasindakiGunFarki(String tarih){
		Preconditions.checkNotNull(tarih, "tarih null olamaz");		
		return ikiTarihArasindakiGunFarki(new LocalDate().toString("dd/MM/yyyy"), tarih);		
	}

	public static int sistemTarihiyleArasindakiGunFarki(Date tarih){
		Preconditions.checkNotNull(tarih, "tarih null olamaz");
		return ikiTarihArasindakiGunFarki(new LocalDate(tarih), new LocalDate());
	}

	public static String xmlDateToStringWithDefaultPattern(XMLGregorianCalendar date){
		Preconditions.checkNotNull(date, "tarih null olamaz");
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		return sdf.format(date.toGregorianCalendar().getTime());						
	}
	
	public static XMLGregorianCalendar localDateToXMLGregorianCalendar(LocalDate date) throws DatatypeConfigurationException{		
		DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();
		return datatypeFactory.newXMLGregorianCalendar(date.toDateTimeAtStartOfDay().toGregorianCalendar());
	}
	
	public static LocalDate xmlDateToLocalDate(XMLGregorianCalendar date){		
		Preconditions.checkNotNull(date, "tarih null olamaz");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return new LocalDate(sdf.format(date.toGregorianCalendar().getTime()));
	}
	
    public static Date xmlGregorianCalendarToDate(XMLGregorianCalendar date)
    {
        Preconditions.checkNotNull(date, "tarih null olamaz");
        return date.toGregorianCalendar().getTime();
    }

    public static String xmlGregorianCalendarToString(XMLGregorianCalendar date)
    {
        Preconditions.checkNotNull(date, "tarih null olamaz");
        return dateToString(date.toGregorianCalendar().getTime());
    }
	
	public static XMLGregorianCalendar dateToXMLGregorianCalendarWithDefaultPattern(Date date) throws DatatypeConfigurationException{
		return dateToXMLGregorianCalendar(date, "yyyy-MM-dd");
	}
	
	public static XMLGregorianCalendar dateToXMLGregorianCalendar(Date date, String pattern) throws DatatypeConfigurationException{
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();
		return datatypeFactory.newXMLGregorianCalendar(simpleDateFormat.format(date));
	}
	
	public static XMLGregorianCalendar dateToXMLGregorianCalendar(Date date) throws DatatypeConfigurationException{
		LocalDate _date = new LocalDate(date);
		return localDateToXMLGregorianCalendar(_date);
	}
	
	public static XMLGregorianCalendar stringToXMLGregorianCalendar(String date, String inputPattern, String outputPattern) throws DatatypeConfigurationException{
		LocalDate _tarih = stringToLocalDate(date, inputPattern);		
		return dateToXMLGregorianCalendarWithDefaultPattern(_tarih.toDateMidnight().toDate());
	}
	
    public static  DateTime stringToDateTime(String date)
    {
        DateTime tescilTarihi;
        try
        {
            tescilTarihi = DateTimeFormat.forPattern("dd/MM/yyyy").parseDateTime(date);
        }
        catch (Exception e)
        {
            tescilTarihi = DateTimeFormat.forPattern("ddMMyyyy").parseDateTime(date);
            logger.error("Sistemde beklenmeyen hata olustu:",e);
        }
        return tescilTarihi;
    }
    
	public static boolean ikiTarihArasindaTamBirYilMiVar(LocalDate baslangicTarihi, LocalDate bitisTarihi) 
	{
		Preconditions.checkNotNull(baslangicTarihi, "başlangıç tarihi null olamaz");
		Preconditions.checkNotNull(bitisTarihi, "bitiş tarihi null olamaz");
		return "P1Y".equals((new Period(baslangicTarihi, bitisTarihi)).toString());
	}
	
    public static Long tarihlerArasiSaatFarkiniHesapla(Date bitisTarihi, Date baslangicTarihi)
    {
        LocalDateTime ilkTarih = new LocalDateTime(bitisTarihi);
        LocalDateTime ikinciTarih = new LocalDateTime(baslangicTarihi);
        return Long.valueOf(Hours.hoursBetween(ikinciTarih, ilkTarih).getHours());
    }

    public static Long tarihlerArasiDakikaFarkiniHesapla(Date bitisTarihi, Date baslangicTarihi)
    {
        LocalDateTime ilkTarih = new LocalDateTime(bitisTarihi);
        LocalDateTime ikinciTarih = new LocalDateTime(baslangicTarihi);
        return Long.valueOf(Minutes.minutesBetween(ikinciTarih, ilkTarih).getMinutes());
    }
    

    public static Date minusDaysExcludingWeekends(Date date, int dayCount)
    {
        LocalDateTime localDateTime = new LocalDateTime(date.getTime());
        localDateTime = minusWeekends(localDateTime);
        for (int i = dayCount; i > 0; i--)
        {
            localDateTime = localDateTime.minusDays(1);
            localDateTime = minusWeekends(localDateTime);
        }
        return localDateTime.toDateTime().toDate();
    }

    private static LocalDateTime minusWeekends(LocalDateTime time)
    {
        final int SATURDAY = 6;
        final int SUNDAY = 7;
        while (time.getDayOfWeek() == SATURDAY || time.getDayOfWeek() == SUNDAY)
        {
            time = time.minusDays(1);
        }
        return time;
    }

    public static Date saatBilgisiniSifirla(Date date)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }
    
    /**
     * @author atifcan
     * @param String date
     * @param String pattern
     * 
     * Verilen tarih bilgisi verilen patterne uygynsa true doner.<br>
     * <br>
     * E.g: <br>
     *      isLegalDate("00000000","yyyyMMdd") -> false<br>
     *      isLegalDate("21/15/2001", "dd/MM/yyyy") -> false<br>
     *      isLegalDate("","dd/MM/yyyy") -> false<br>
     *      isLegalDate(null,"dd/MM/yyyy") -> false<br>
     *      isLegalDate("20121005", "yyyyMMdd") -> true<br>
     * */
    public static boolean isLegalDate(String date, String pattern){
            SimpleDateFormat sdf = new SimpleDateFormat(pattern);
            sdf.setLenient(false);
            return sdf.parse(date, new ParsePosition(0)) != null;
    }
    
    public static String getCurrentLocalDateTimeWithPattern(String pattern){
        Preconditions.checkNotNull(pattern, "pattern null olamaz");
        LocalDateTime localDateTime = new LocalDateTime();
        return localDateTime.toString(pattern);
    }
    
    public static String extractYearFromDate(Date date)
    {
        LocalDate localDate = convertDateToLocalDate(date);
        return String.valueOf(localDate.getYear());
    }

    public static String extractMonthFromDate(Date date)
    {
        LocalDate localDate = convertDateToLocalDate(date);
        return StringUtils.leftPad(String.valueOf(localDate.getMonthOfYear()), 2, "0");
    }

    private static LocalDate convertDateToLocalDate(Date date)
    {
        Preconditions.checkNotNull(date, "tarih bilgisi boş olamaz");
        return new LocalDate(date);
    }
    
    public static int getCurrentYear()
    {
        return new LocalDate().getYear();
    }
    
    public static String getNowMinusDaysAsFormattedString(int day, String dateFormat)
    {
        LocalDate now = new LocalDate();
        Date tarih  = now.minusDays(day).toDateMidnight().toDate();
        DateTimeFormatter fmt = DateTimeFormat.forPattern(dateFormat);
        return fmt.print(tarih.getTime());
    }

    public static String getNowMinusMonthsAsFormattedString(int month, String dateFormat)
    {
        LocalDate now = new LocalDate();
        Date tarih  = now.minusMonths(month).toDateMidnight().toDate();
        DateTimeFormatter fmt = DateTimeFormat.forPattern(dateFormat);
        return fmt.print(tarih.getTime());
    }
    
    public static Date addDays(Date date, int daysToAdd)
    {
        Preconditions.checkNotNull(date, "Tarih boş olamaz");
        return new LocalDateTime(date).plusDays(daysToAdd).toDateTime().toDate();
    }

    public static Date getStartOfDay(Date date)
    {
        return new LocalDate(date).toDateTimeAtStartOfDay().toDate();
    }

    public static Date getEndOfDay(Date date)
    {
        return getEndOfDayForDateTime(date).toDate();
    }

    public static DateTime getEndOfDayForDateTime(Date date)
    {
        return new LocalDate(date).toDateTimeAtCurrentTime().withTime(23, 59, 59, 999);
    }    

    public static boolean tarihMuteakipAyinSonGunundenSonraMi(LocalDate tarih)
    {
        Preconditions.checkNotNull(tarih,"Lütfen geçerli bir tarih giriniz");
        LocalDate today = new LocalDate();
        LocalDate lastDayOfNextMonth = today.plusMonths(2).withDayOfMonth(1).minusDays(1);
        return tarih.isAfter(lastDayOfNextMonth);
    }

    public static boolean tarihMuteakipAyinIlkGunundenSonraMi(LocalDate tarih)
    {
        Preconditions.checkNotNull(tarih,"Lütfen geçerli bir tarih giriniz");
        LocalDate today = new LocalDate();
        LocalDate lastDayOfThisMonth = today.plusMonths(1).withDayOfMonth(1).minusDays(1);
        return tarih.isAfter(lastDayOfThisMonth);
    }

    public static boolean tarihCariAyinIlkGunundenOnceMi(LocalDate tarih)
    {
        Preconditions.checkNotNull(tarih,"Lütfen geçerli bir tarih giriniz");
        LocalDate today = new LocalDate();
        LocalDate firstDayOfCurrentMonth = today.withDayOfMonth(1);
        return tarih.isBefore(firstDayOfCurrentMonth);
    }

    /**
     * <p>Checks if two objects are on the same month ignoring day and time.</p>
     */
    public static boolean isSameMonth(LocalDate date1, LocalDate date2)
    {
        Preconditions.checkNotNull(date1, "Lütfen geçerli bir tarih giriniz");
        Preconditions.checkNotNull(date2, "Lütfen geçerli bir tarih giriniz");
        return isSameMonth(date1.toDateMidnight().toDate(), date2.toDateMidnight().toDate());
    }

    /**
     * <p>Checks if two objects are on the same month ignoring day and time.</p>
     */
    public static boolean isSameMonth(Date date1, Date date2)
    {
        Preconditions.checkNotNull(date1, "Lütfen geçerli bir tarih giriniz");
        Preconditions.checkNotNull(date2, "Lütfen geçerli bir tarih giriniz");
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        return cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
                        cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                        cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH);
    }

    public static Date saatBilgisiEkle(Date date, int saat, int dakika)
    {
        saatBilgisiniSifirla(date);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, saat);
        calendar.set(Calendar.MINUTE, dakika);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    public static DateTime minusMinutes(Date date, int minutes)
    {
        Preconditions.checkNotNull(date, "Tarih boş olamaz");
        return new LocalDateTime(date).minusMinutes(minutes).toDateTime();
    }

    public static Date localDateToDate(LocalDate localDate)
    {
        return localDate == null ? null : localDate.toDateMidnight().toDate();
    }

    public static String timestampToStringWithPattern(long timestamp, String pattern)
    {
        return new SimpleDateFormat(pattern).format(new Date(timestamp));
    }
    
    public static Date getDayWithMaxHour(Date date) 
    {
    	Calendar calendar = Calendar.getInstance();
    	calendar.setTime(date);
    	calendar.set(calendar.get(Calendar.YEAR), 
            		 calendar.get(Calendar.MONTH), 
                     calendar.get(Calendar.DAY_OF_MONTH), 
                     calendar.getActualMaximum(Calendar.HOUR), 
                     calendar.getActualMaximum(Calendar.MINUTE), 
                     calendar.getActualMaximum(Calendar.SECOND)
            );
    	calendar.set(Calendar.MILLISECOND, calendar.getActualMaximum(Calendar.MILLISECOND));
    	return calendar.getTime();
    }

    public static boolean haftaSonuMu()
    {
        int day = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
        return day == Calendar.SATURDAY || day == Calendar.SUNDAY;
    }

    public static Integer ikiZamanArasiDakikaFarkiniHesapla(Date lastActivityTime, Date date)
    {
        LocalDateTime activityTime = new LocalDateTime(lastActivityTime);
        LocalDateTime now = new LocalDateTime(date);
        return Minutes.minutesBetween(now, activityTime).getMinutes();
    }
}
