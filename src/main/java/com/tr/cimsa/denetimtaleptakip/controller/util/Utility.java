package com.tr.cimsa.denetimtaleptakip.controller.util;

import com.google.common.base.Preconditions;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Properties;

/**
 * User: mhazer
 * Date: 1/11/13
 * Time: 5:28 PM
 */
public class Utility
{

    public static String generateRandom(int size) { //32 olabilir size
        SecureRandom secureRandom;
        try
        {
            secureRandom = SecureRandom.getInstance("SHA1PRNG");
            String secureToken = new BigInteger(100, secureRandom).toString(size);
            return secureToken;
        } catch (NoSuchAlgorithmException e)
        {
            throw new RuntimeException(e.getMessage());
        }
    }

    public static String convertStackTraceToString(Exception exception)
    {
        Preconditions.checkNotNull(exception, "Stack trace oluşturulması için gerekli exception'a oluşamadı");
        StringWriter sw = new StringWriter();
        exception.printStackTrace(new PrintWriter(sw));
        return sw.toString();
    }

}
