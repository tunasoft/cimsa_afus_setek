package com.tr.cimsa.denetimtaleptakip.controller.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import com.tr.cimsa.denetimtaleptakip.model.AuditType;
import com.tr.cimsa.denetimtaleptakip.model.BusinessProcessType;
import com.tr.cimsa.denetimtaleptakip.model.BusinessSubProcessType;
import com.tr.cimsa.denetimtaleptakip.model.Company;
import com.tr.cimsa.denetimtaleptakip.model.Department;
import com.tr.cimsa.denetimtaleptakip.model.EmailContent;
import com.tr.cimsa.denetimtaleptakip.model.EmailSendingStrategy;
import com.tr.cimsa.denetimtaleptakip.model.Priority;
import com.tr.cimsa.denetimtaleptakip.model.Role;
import com.tr.cimsa.denetimtaleptakip.model.Status;
import com.tr.cimsa.denetimtaleptakip.model.User;
import com.tr.cimsa.denetimtaleptakip.services.FindingService;
import com.tr.cimsa.denetimtaleptakip.services.UserService;

import lombok.Getter;

/**
 * User: mhazer
 * Date: 8/21/12
 * Time: 11:35 PM
 * @param <statuses>
 */
@ManagedBean(name = "commonDefinitionsController")
@SessionScoped
public class CommonDefinitionsController<statuses> implements Serializable
{
    @ManagedProperty("#{findingService}")
    private FindingService findingService;

    @ManagedProperty("#{userService}")
    private UserService userService;

    @Getter
    private List<AuditType> auditTypes;
    
    @Getter
    private List<Company> companies;

    @Getter
    private List<AuditType> activeAuditTypes;
    
    @Getter
    private List<Company> activeCompanies;
    
    @Getter
    private List<Department> activeDepartments;
    
    @Getter
    private List<String> maximumRevisedDates;

    @Getter
    private List<BusinessProcessType> businessProcessTypes;

    @Getter
    private List<BusinessSubProcessType> businessSubProcessTypes;

    @Getter
    private List<Priority> priorities;

    @Getter
    private List<Status> statuses;

    @Getter
    private List<User> activeUsers;

    @Getter
    private List<User> allUsers;

    @Getter
    private List<Role> allRoles;

    @Getter
    private List<EmailSendingStrategy> emailSendingStrategies;

    @Getter
    private List<EmailContent> emailContents;
    
    @Getter
    private List<Department> departments;

    @PostConstruct
    public void init()
    {
        this.priorities = Arrays.asList(Priority.values());
        this.statuses = Arrays.asList(Status.values());
        refreshAuditTypes();
        refreshCompanies();
        refreshDepartments();
        refreshBusinessTypes();
        refreshMaximumRevisedDates();
        refreshUsers();
        refreshRoles();
        refreshEmailSendingStrategies();
        refreshEmailContents();
    }

    public void refreshUsers()
    {
        this.activeUsers = this.userService.retrieveActiveUsers();
        if (activeUsers.size() > 0) {
			Collections.sort(activeUsers, new Comparator<User>() {
			    @Override
			    public int compare(final User object1, final User object2) {
			        return object1.getNameSurname().compareTo(object2.getNameSurname());
			    }
			});
        }
        this.allUsers = this.userService.retrieveAllUsers();
        if (allUsers.size() > 0) {
			Collections.sort(allUsers, new Comparator<User>() {
			    @Override
			    public int compare(final User object1, final User object2) {
			        return object1.getNameSurname().compareTo(object2.getNameSurname());
			    }
			});
        }
    }

    public void refreshAuditTypes()
    {
        this.auditTypes = this.findingService.retrieveAuditTypes();
        if (auditTypes.size() > 0) {
			Collections.sort(auditTypes, new Comparator<AuditType>() {
			    @Override
			    public int compare(final AuditType object1, final AuditType object2) {
			        return object1.getDescription().compareTo(object2.getDescription());
			    }
			});
        }
        this.activeAuditTypes = this.findingService.retrieveAllActiveAuditTypes();
        if (activeAuditTypes.size() > 0) {
			Collections.sort(activeAuditTypes, new Comparator<AuditType>() {
			    @Override
			    public int compare(final AuditType object1, final AuditType object2) {
			        return object1.getDescription().compareTo(object2.getDescription());
			    }
			});
        }
    }
    
    public void refreshCompanies()
    {
        this.companies = this.findingService.retrieveCompanies();
        if (companies.size() > 0) {
			Collections.sort(companies, new Comparator<Company>() {
			    @Override
			    public int compare(final Company object1, final Company object2) {
			        return object1.getDescription().compareTo(object2.getDescription());
			    }
			});
        }
        this.activeCompanies = this.findingService.retrieveAllActiveCompanies();
        if (activeCompanies.size() > 0) {
			Collections.sort(activeCompanies, new Comparator<Company>() {
			    @Override
			    public int compare(final Company object1, final Company object2) {
			        return object1.getDescription().compareTo(object2.getDescription());
			    }
			});
        }
    }
    
    public void refreshDepartments()
    {
        this.departments = this.findingService.retrieveDepartments();
        if (departments.size() > 0) {
			Collections.sort(departments, new Comparator<Department>() {
			    @Override
			    public int compare(final Department object1, final Department object2) {
			        return object1.getDescription().compareTo(object2.getDescription());
			    }
			});
        }
        this.activeDepartments = this.findingService.retrieveAllActiveDepartments();
        if (activeDepartments.size() > 0) {
			Collections.sort(activeDepartments, new Comparator<Department>() {
			    @Override
			    public int compare(final Department object1, final Department object2) {
			        return object1.getDescription().compareTo(object2.getDescription());
			    }
			});
        }
    }
    
    public void refreshMaximumRevisedDates()
    {	
    	this.maximumRevisedDates=new ArrayList<>();
        this.maximumRevisedDates.add("3 ay");
        this.maximumRevisedDates.add("6 ay");
        this.maximumRevisedDates.add("9 ay");
        this.maximumRevisedDates.add("12 ay");
        this.maximumRevisedDates.add("12+ ay");
    }
    
    public void refreshBusinessTypes()
    {
    	this.businessProcessTypes = this.findingService.retrieveBusinessProcesses();
        if (businessProcessTypes.size() > 0) {
             Collections.sort(businessProcessTypes, new Comparator<BusinessProcessType>() {
                 @Override
                 public int compare(final BusinessProcessType object1, final BusinessProcessType object2) {
                     return object1.getDescription().compareTo(object2.getDescription());
                 }
             });
        }
        this.businessSubProcessTypes = this.findingService.retrieveBusinessSubProcesses();
        if (businessSubProcessTypes.size() > 0) {
			Collections.sort(businessSubProcessTypes, new Comparator<BusinessSubProcessType>() {
			    @Override
			    public int compare(final BusinessSubProcessType object1, final BusinessSubProcessType object2) {
			        return object1.getDescription().compareTo(object2.getDescription());
			    }
			});
        }
        
    }

    private void refreshRoles()
    {
        this.allRoles = this.userService.retrieveAllRoles();
    }

    private void refreshEmailSendingStrategies()
    {
        this.emailSendingStrategies = this.findingService.retrieveAllEmailStrategies();
    }

    public void refreshEmailContents()
    {
        this.emailContents = this.findingService.retrieveAllEmailContents();
    }

    public void setFindingService(FindingService findingService)
    {
        this.findingService = findingService;
    }

    public void setUserService(UserService userService)
    {
        this.userService = userService;
    }
    
}
