package com.tr.cimsa.denetimtaleptakip.controller.user;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tr.cimsa.denetimtaleptakip.controller.Visit;
import com.tr.cimsa.denetimtaleptakip.controller.admin.FindingAdminController;
import com.tr.cimsa.denetimtaleptakip.model.Finding;
import com.tr.cimsa.denetimtaleptakip.services.FindingService;

import lombok.Getter;

public class DepartmentFindingsLazyModel extends LazyDataModel<Finding> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final  Logger logger = LoggerFactory.getLogger(DepartmentFindingsLazyModel.class);

	private FindingService findingService;
	private Visit visit;

	@Getter
	private List<String> actionOwnersOfFindings;

	@Getter
	private List<String> responsibleOwnersOfFindings;

	public DepartmentFindingsLazyModel(FindingService findingService, Visit visit) {
		this.findingService = findingService;
		this.visit = visit;
	}

	@Override
	public List<Finding> load(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) {

		long start = System.currentTimeMillis();
		
		int totalDataSize = 0;
		
		List<String> list1 = findingService.getActionOwnersOfMyDepartmentFindingsForSubBusinesProc(Integer.toString(visit.getUser().getId()));
		List<String> list2 = findingService.getActionOwnersOfMyDepartmentFindingsForBusinessProc(Integer.toString(visit.getUser().getId()));
		
		HashSet<String> hset = new HashSet<String>();
		hset.addAll(list1);
		hset.addAll(list2);
		
		actionOwnersOfFindings = new ArrayList<>();
		
		actionOwnersOfFindings.addAll(hset);
		
		Collections.sort(actionOwnersOfFindings);
		
		responsibleOwnersOfFindings = findingService.getResponsibleOwnersOfFindings();

		
		if (filters != null && !filters.isEmpty()) {
			logger.debug("DepartmentFindingsLazyModel.load() filter size " + filters.size());

			Map<String, Object> paramMaps = new LinkedHashMap<>();

			for (String key : filters.keySet()) {

				logger.debug(key + " ->  value : " + filters.get(key).toString());

				Object value = filters.get(key);

				key = key.equals("auditDate") ? "AUDIT_DATE" : key;
				key = key.equals("findingYear") ? "FINDING_YEAR" : key;
				key = key.equals("revisedCompletionDate") ? "REVISED_COMP_DATE" : key;
				key = key.equals("completionDate") ? "COMPLETION_DATE" : key;

				if (key.equals("getStatus().getDescription()")) {

					value = value.equals("Overdue") ? "DELAYED" : value;
					value = value.equals("Unconfirmed-Completed") ? "UNCONFIRMED_COMPLETED" : value;
					value = value.equals("Confirmed-Completed") ? "CONFIRMED_COMPLETED" : value;
					value = value.equals("Not Started") ? "NOT_STARTED" : value;
					value = value.equals("In Progress") ? "IN_PROGRESS" : value;

					paramMaps.put("status", value);
				} else {
					paramMaps.put(key, value);
				}

			}

			List<Finding> filteredList = new ArrayList<Finding>();
			filteredList = findingService.retrieveUserResponsibleDepartmentsFindingActionsByParam(paramMaps, first,
					pageSize, sortField, sortOrder, visit.getUser());

			totalDataSize = findingService
					.retrieveUserResponsibleDepartmentsFindingActionsCountByParam(paramMaps, visit.getUser())
					.intValue();
			this.setRowCount(totalDataSize);

			return filteredList;

		}

		totalDataSize = findingService.retrieveUserResponsibleDepartmentsFindingActionsCount(visit.getUser())
				.intValue();
		setRowCount(totalDataSize);

		List<Finding> list = findingService.retrieveUserResponsibleDepartmentsFindingActions(first, pageSize, sortField,
				sortOrder, visit.getUser());

		return list;

	}

}
