package com.tr.cimsa.denetimtaleptakip.controller.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.tr.cimsa.denetimtaleptakip.model.Role;

import java.util.List;

/**
 * User: mhazer
 * Date: 8/20/12
 * Time: 8:43 AM
 */
@FacesConverter(value = "roleConverter")
public class RoleConverter implements Converter
{
    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String id)
    {
        List<Role> roles = (List<Role>) uiComponent.getAttributes().get("roleEntities");
        if (roles != null)
        {
            for (Role role : roles)
            {
                if (role.getId().toString().equals(id))
                {
                    return role;
                }
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o)
    {
        Role role = (Role) o;
        return role.getId().toString();
    }
}
