package com.tr.cimsa.denetimtaleptakip.controller.admin;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.tr.cimsa.denetimtaleptakip.controller.util.FacesUtils;
import com.tr.cimsa.denetimtaleptakip.model.Finding;
import com.tr.cimsa.denetimtaleptakip.model.FindingAction;
import com.tr.cimsa.denetimtaleptakip.model.User;
import com.tr.cimsa.denetimtaleptakip.services.FindingService;

import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.List;

/**
 * @author murathazer
 */
@ManagedBean(name = "reassignFindingController")
@ViewScoped
public class ReassignFindingController implements Serializable
{
    private final static Logger LOGGER = LoggerFactory.getLogger(ReassignFindingController.class);

    @ManagedProperty("#{findingService}")
    @Setter
    private FindingService findingService;

    @ManagedProperty("#{commonDefinitionsController.activeUsers}")
    @Setter
    private List<User> users;

    @Getter
    @Setter
    private Integer findingIdToUpdate;

    @Getter
    private Finding finding;

    @Getter
    private List<User> currentActionOwners;

    @Getter
    @Setter
    private List<User> selectedOldActionOwners;

    @Getter
    @Setter
    private User selectedNewActionOwner;

    public void prepareFindingForReassign()
    {
        if (!FacesContext.getCurrentInstance().isPostback() && findingIdToUpdate != null)
        {
            this.finding = this.findingService.retrieveFinding(findingIdToUpdate);
            Preconditions.checkNotNull(this.finding, "Can not find finding with the given id :", findingIdToUpdate);
            adjustActionOwners();
        }
    }

    protected void adjustActionOwners()
    {
        currentActionOwners = Lists.newArrayList();
        selectedOldActionOwners = Lists.newArrayList();
        for (FindingAction findingAction : this.finding.getFindingActions())
        {
            User findingActionUser = findingAction.getActionOwner();
            currentActionOwners.add(findingActionUser);
        }
    }
    
	public Finding updatePercentageOfFinding(Finding finding) {

		int numberOfRealActOwners = 0;
		int sumOfFindingActionCompletionPercentages = 0;

		for (FindingAction findingAction : finding.findingActionsAsList()) {

			if (findingAction.isActionOwnerFlag()) {
				numberOfRealActOwners++;
				sumOfFindingActionCompletionPercentages = sumOfFindingActionCompletionPercentages
						+ findingAction.getCompletionPercentage();
			}

		}

		try {
			finding.setCompPercentage(
					Integer.toString(sumOfFindingActionCompletionPercentages / numberOfRealActOwners));
		} catch (ArithmeticException e) {
			finding.setCompPercentage("0");
		}
		return finding;
	}

    public void reassignFinding()
    {
        try
        {
            LOGGER.debug("finding is: {}", this.finding.getId());
            this.findingService.reassignFinding(this.finding, this.currentActionOwners , this.selectedOldActionOwners, this.selectedNewActionOwner);
            this.finding =  findingService.retrieveFinding(this.finding.getId());
            this.finding = updatePercentageOfFinding(this.finding);
            this.finding =  this.findingService.mergeFinding(this.finding);
            FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, this.finding.getAuditType().getDescription() + " finding reassigned successfully", null);
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
            LOGGER.error("can not reassign finding: {} ({})", finding.getId(), exception.getMessage());
            FacesUtils.addMessage(FacesMessage.SEVERITY_INFO, "Can not reassign finding: " + exception.getMessage(), null);
        }
        finally
        {
        	
//            this.finding = this.findingService.retrieveFinding(this.finding.getId());
            this.selectedNewActionOwner = null;
            adjustActionOwners();
        }
    }
}
