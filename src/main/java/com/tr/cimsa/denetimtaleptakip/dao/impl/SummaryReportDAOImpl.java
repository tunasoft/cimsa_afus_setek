package com.tr.cimsa.denetimtaleptakip.dao.impl;

import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.google.common.collect.Lists;
import com.tr.cimsa.denetimtaleptakip.dao.SummaryReportDAO;
import com.tr.cimsa.denetimtaleptakip.model.*;


@Repository("summaryReportDAO")
public class SummaryReportDAOImpl extends GenericDAOImpl implements SummaryReportDAO
{
    public List<Finding> getFindingForSummaryReport(AuditType auditType, User user)
    {
        Criteria criteria = getSummaryReportCriteria(auditType, user);
        return criteria.list();
    }

    @Override
    public List<Finding> getFindingForSummaryReport(AuditType auditType, User user, String priority, String status)
    {
        List<Finding> selectedFindings = Lists.newArrayList();

        Criteria criteria = getSummaryReportCriteria(auditType, user);
        criteria.add(Restrictions.eq("priority", Priority.findPriorityFromDescription(priority)));
        for (Finding finding : (List<Finding>)criteria.list())
        {
            if (Status.findStatusFromDescription(status).equals(finding.getStatus()))
            {
                selectedFindings.add(finding);
            }
        }
        return selectedFindings;
    }

    private Criteria getSummaryReportCriteria(AuditType auditType, User user)
    {
        List<Integer> procIds = getIds(user.getResponsibleBusinessTypes());
        List<Integer> subProcIds = getIds(user.getResponsibleBusinessSubTypes());
        Criteria criteria = getSession().createCriteria(Finding.class, "finding");

        if (auditType != null)
        {
            criteria.add(Restrictions.eq("auditType", auditType));
        }
        if (!user.hasRole("ROLE_ADMIN"))
        {
            if (CollectionUtils.isNotEmpty(procIds) && CollectionUtils.isNotEmpty(subProcIds))
            {
                criteria.add(Restrictions.disjunction()
                		.add(Restrictions.in("businessProcessType", procIds))
                    .add(Restrictions.in("businessSubProcessType", subProcIds)));
            }
            else if (CollectionUtils.isNotEmpty(procIds))
            {
                criteria.add(Restrictions.in("businessProcessType", procIds));
            }
            else if (CollectionUtils.isNotEmpty(subProcIds))
            {
                criteria.add(Restrictions.in("businessSubProcessType", subProcIds));
            }
        }
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return criteria;
    }

    /**
    @Override
    public BigDecimal findKpiRateFromTable(BigDecimal higherOverdueKPIRate, BigDecimal lowerOverdueKPIRate)
    {
        return (BigDecimal)getSession().createSQLQuery(
            "select kpi.KPI_RATE from SG.DT_RT_KPI_RATE kpi where kpi.VH_H_MIN <= :higherOverdueKPIRate and kpi.VH_H_MAX >= :higherOverdueKPIRate and kpi.M_L_MIN <= :lowerOverdueKPIRate and kpi.M_L_MAX >= :lowerOverdueKPIRate")
            .setParameter("higherOverdueKPIRate", higherOverdueKPIRate)
            .setParameter("lowerOverdueKPIRate", lowerOverdueKPIRate)
            .setMaxResults(1)
            .uniqueResult();
    }**/
}
