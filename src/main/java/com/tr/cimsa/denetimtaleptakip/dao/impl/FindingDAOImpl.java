package com.tr.cimsa.denetimtaleptakip.dao.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.query.AuditEntity;
import org.hibernate.envers.query.AuditQuery;
import org.hibernate.transform.Transformers;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.tr.cimsa.denetimtaleptakip.controller.admin.SearchParameters;
import com.tr.cimsa.denetimtaleptakip.controller.user.DepartmentFindingsLazyModel;
import com.tr.cimsa.denetimtaleptakip.dao.FindingDAO;
import com.tr.cimsa.denetimtaleptakip.model.AuditType;
import com.tr.cimsa.denetimtaleptakip.model.BusinessProcessType;
import com.tr.cimsa.denetimtaleptakip.model.BusinessSubProcessType;
import com.tr.cimsa.denetimtaleptakip.model.Company;
import com.tr.cimsa.denetimtaleptakip.model.Department;
import com.tr.cimsa.denetimtaleptakip.model.EmailContent;
import com.tr.cimsa.denetimtaleptakip.model.Finding;
import com.tr.cimsa.denetimtaleptakip.model.FindingAction;
import com.tr.cimsa.denetimtaleptakip.model.FindingFile;
import com.tr.cimsa.denetimtaleptakip.model.FindingResponsibleAction;
import com.tr.cimsa.denetimtaleptakip.model.User;

/**
 * User: mhazer Date: 8/19/12 Time: 5:07 PM
 */
@Repository("findingDAO")
public class FindingDAOImpl extends GenericDAOImpl implements FindingDAO {
	private final  Logger logger = LoggerFactory.getLogger(GenericDAOImpl.class);

	@Override
	public List<Finding> retrieveAllFindings() {
		return getSession().createCriteria(Finding.class).addOrder(Order.asc("completionDate")).list();
	}

	@Override
	public List<FindingAction> retrieveUserFindingActions(User user) {
		try {
			return getSession().createCriteria(FindingAction.class).add(Restrictions.eq("actionOwner", user))
					.addOrder(Order.asc("completionPercentage")).list();
		} catch (Exception e) {
			logger.debug("retrieveUserFindingActions Exception :" +e.getMessage());
		}
		return new ArrayList<FindingAction>();
	}

	@Override
	public List<BusinessSubProcessType> retrieveBusinessSubProcesses() {
		return getSession().createCriteria(BusinessSubProcessType.class).list();
	}

	@Override
	public List<BusinessProcessType> retrieveBusinessProcesses() {
		return getSession().createCriteria(BusinessProcessType.class).list();
	}
	
	
	@Override
	public List<Finding> searchFindings(SearchParameters searchParameters) {
		DetachedCriteria detachedCriteria = searchParameters.createSearhQueryFromParameters();
		Criteria criteria = detachedCriteria.getExecutableCriteria(getSession());
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return criteria.list();
	}

	@Override
	public FindingAction retrieveFindingActionWithHistory(Integer findingActionId) {
		FindingAction findingAction = findById(FindingAction.class, findingActionId);
		Preconditions.checkNotNull(findingAction, "Can not find action with given id %s", findingActionId);
		findingAction.setHistoryOfFindingAction(retrieveHistoryOfFindingAction(findingActionId));
		return findingAction;
	}

	@Override
	public List<FindingAction> retrieveHistoryOfFindingAction(Integer findingActionId) {
		AuditQuery auditQuery = AuditReaderFactory.get(getSession()).createQuery()
				.forRevisionsOfEntity(FindingAction.class, true, false).add(AuditEntity.id().eq(findingActionId));
		return auditQuery.getResultList();
	}

	@Override
	public Long findNotCompletedFindingActionCount(Finding finding) {
		return (Long) getSession().createCriteria(FindingAction.class).add(Restrictions.eq("finding", finding))
				.add(Restrictions.lt("completionPercentage", 100)).setProjection(Projections.rowCount()).uniqueResult();
	}

	@Override
	public List<AuditType> retrieveAllAuditTypes() {
		return getSession().createCriteria(AuditType.class).addOrder(Order.asc("description")).list();
	}

	@Override
	public List<Company> retrieveAllCompanies() {
		return getSession().createCriteria(Company.class).addOrder(Order.asc("description")).list();
	}

	@Override
	public AuditType retrieveAuditType(String description) {
		return findByProperty(AuditType.class, "description", description);
	}

	@Override
	public Company retrieveCompany(String description) {
		return findByProperty(Company.class, "description", description);
	}

	@Override
	public Department retrieveDepartment(String description) {
		return findByProperty(Department.class, "description", description);
	}

	@Override
	public List<AuditType> retrieveAllActiveAuditTypes() {
		return getSession().createCriteria(AuditType.class).add(Restrictions.eq("active", true))
				.addOrder(Order.asc("description")).list();
	}

	@Override
	public List<Company> retrieveAllActiveCompanies() {
		return getSession().createCriteria(Company.class).add(Restrictions.eq("active", true))
				.addOrder(Order.asc("description")).list();
	}

	@Override
	public List<Department> retrieveAllActiveDepartments() {
		return getSession().createCriteria(Department.class).add(Restrictions.eq("active", true))
				.addOrder(Order.asc("description")).list();
	}

	@Override
	public List<Finding> retrieveUserResponsibleBusinessTypeFindings(
			Collection<BusinessProcessType> responsibleBusinessTypes) {
		return getSession().createCriteria(Finding.class)
				.add(Restrictions.in("businessProcessType", responsibleBusinessTypes)).list();
	}

	@Override
	public List<Finding> retrieveUserResponsibleBusinessSubTypeFindings(
			Collection<BusinessSubProcessType> responsibleBusinessSubTypes) {
		return getSession().createCriteria(Finding.class)
				.add(Restrictions.in("businessSubProcessType", responsibleBusinessSubTypes)).list();
	}
	
	


	@Override
	public void deleteFindingActionsOfUsers(Finding finding, List<User> selectedOldActionOwners) {
		if (CollectionUtils.isNotEmpty(selectedOldActionOwners)) {
			getSession().getNamedQuery("finding.deleteFindingActions").setParameter("finding", finding)
					.setParameterList("oldUsers", selectedOldActionOwners).executeUpdate();
		}
	}

	@Override
	public List<FindingResponsibleAction> retrieveHistoryOfFindingResponsibleAction(
			Integer findingResponsibleActionId) {
		AuditQuery auditQuery = AuditReaderFactory.get(getSession()).createQuery()
				.forRevisionsOfEntity(FindingResponsibleAction.class, true, false)
				.add(AuditEntity.id().eq(findingResponsibleActionId));
		return auditQuery.getResultList();
	}

	@Override
	public BusinessProcessType retrieveBusinessProcessType(String description) {
		return findByProperty(BusinessProcessType.class, "description", description);
	}

	@Override
	public Finding retrieveFinding(String findingNumber) {
		return findByProperty(Finding.class, "findingNumber", findingNumber);
	}

	@Override
	public Finding retrieveFinding2(int id) {
		return findByProperty(Finding.class, "id", id);
	}

	@Override
	public List<Finding> retrieveAllFindings(int first, int pageSize, String sortField, SortOrder sortOrder) {
		Criteria criteria = getSession().createCriteria(Finding.class);
		criteria.createAlias("businessProcessType", "businessProcessType");

		if (first >= 0) {
			criteria.setFirstResult(first);
		}
		if (pageSize >= 0) {
			criteria.setMaxResults(pageSize);
		}

		if (sortField == null) {
			criteria.addOrder(Order.asc("completionDate"));
		} else if (sortOrder.equals(SortOrder.ASCENDING)) {
			criteria.addOrder(Order.asc(sortField));
		} else if (sortOrder.equals(SortOrder.DESCENDING)) {
			criteria.addOrder(Order.desc(sortField));
		}
		logger.debug("size " + criteria.list().size());
		return criteria.list();
	}

	@Override
	public List<Finding> retrieveFindingsByParam(Map<String, Object> paramterMaps, int first, int pageSize,
			String sortField, SortOrder sortOrder) {

		Criteria criteria = getSession().createCriteria(Finding.class);

		criteria.createAlias("businessProcessType", "businessProcessType");

		for (String key : paramterMaps.keySet()) {
			Object value = paramterMaps.get(key);

			logger.debug("FindingDAOImpl.retrieveFindingsByParam() key : " + key + "  value -> " + value);

			if (key.equals("AUDIT_DATE") || key.equals("COMPLETION_DATE") || key.equals("REVISED_COMP_DATE")) {
				criteria.add(Restrictions.sqlRestriction(" convert(varchar(10)," + key + ", 126 ) like '%" + value + "%'"));
			} else if (key.equals("FINDING_YEAR")) {
				criteria.add(Restrictions.sqlRestriction(" convert(varchar(4)," + key + ", 126 ) like '%" + value + "%'"));
			}  else if (key.equals("priority") || key.equals("status")) {
				criteria.add(Restrictions.eq(key, value));
			} else  if (key.equals("compareDates")) {
				criteria.add(Restrictions.ne("compPercentage", "100"));
				criteria.add(Restrictions.like(key, "%" + value + "%"));
			} else {
				criteria.add(Restrictions.like(key, "%" + value + "%"));
			}
		}

		if (sortField == null) {
			criteria.addOrder(Order.asc("completionDate"));
		} else if (sortOrder.equals(SortOrder.ASCENDING)) {
			criteria.addOrder(Order.asc(sortField));
		} else if (sortOrder.equals(SortOrder.DESCENDING)) {
			criteria.addOrder(Order.desc(sortField));
		}

		if (first >= 0) {
			criteria.setFirstResult(first);
		}
		if (pageSize >= 0) {
			criteria.setMaxResults(pageSize);
		}

		List<Finding> list = criteria.list();

		return list;
	}

	@Override
	public Long retrieveFindingsCountByParam(Map<String, Object> paramterMaps) {

		Criteria criteria = getSession().createCriteria(Finding.class);

		for (String key : paramterMaps.keySet()) {
			Object value = paramterMaps.get(key);

			if (key.equals("AUDIT_DATE") || key.equals("REVISED_COMP_DATE") || key.equals("COMPLETION_DATE")) {

				criteria.add(
						Restrictions.sqlRestriction(" convert(varchar(10)," + key + ", 126 ) like '%" + value + "%'"));

			} else if (key.equals("FINDING_YEAR")) {
				criteria.add(
						Restrictions.sqlRestriction(" convert(varchar(4)," + key + ", 126 ) like '%" + value + "%'"));
			}  else if (key.equals("priority") || key.equals("status")) {
				criteria.add(Restrictions.eq(key, value));
			} else  if (key.equals("compareDates")) {
				criteria.add(Restrictions.ne("compPercentage", "100"));
				criteria.add(Restrictions.like(key, "%" + value + "%"));
			} 
			else {
				criteria.add(Restrictions.like(key, "%" + value + "%"));
			}

		}

		criteria.setProjection(Projections.rowCount());
		final Long result = (Long) criteria.uniqueResult();
		return result;

	}

	@Override
	public List<String> getActionOwnersOfFindings() {

		List<String> data = getSession().createSQLQuery(
				"select distinct ut.name_surname from dt_id_users_table ut inner join DT_RT_FINDING_ACTIONS_TABLE fa \n"
						+ "  on ut.id = fa.ACTION_OWNER_ID\n" + "  inner join DT_RT_FINDINGS_TABLE ft \n"
						+ "  on fa.FINDING_ID = ft.id  order by ut.name_surname asc")
				.list();

		return data;
	}

	@Override
	public List<String> getResponsibleOwnersOfFindings() {

		List<String> data = getSession().createSQLQuery(
				"Select distinct ut.name_surname  from dt_id_users_table ut inner join DT_RT_FINDING_RESP_ACTIONS_TABLE fra  on ut.id = fra.ACTION_OWNER_ID\n"
						+ " inner join  DT_RT_FINDINGS_TABLE ft  on fra.FINDING_ID = ft.id")
				.list();

		return data;

	}

	@Override
	public List<String> getActionOwnersOfMyDepartmentFindingsForSubBusinesProc(String userId) {

		List<String> data = getSession().createSQLQuery(
				" select  distinct mut.name_surname  from ( Select  ft.FINDING_NUMBER,ft.id  from    DT_RT_FINDINGS_TABLE  ft\n"
						+ "	 \n"
						+ "	   inner join DT_ID_SUBPROC_RESP_HEADS srh on  ft.BUSINESS_SUB_PROC = srh.SUB_PROC_ID\n"
						+ "	   inner join dt_id_users_table ut on  ut.id = srh.USER_ID  where srh.USER_ID=" + userId
						+ ") \n" + "	 \n" + "	  fintable \n"
						+ "	 	 inner join DT_RT_FINDING_ACTIONS_TABLE fa  on fa.FINDING_ID = fintable.id\n"
						+ "		 inner join  dt_id_users_table mut on mut.id = fa.ACTION_OWNER_ID ")
				.list();

		return data;

	}

	public List<String> getActionOwnersOfMyDepartmentFindingsForBusinessProc(String userId) {

		List<String> data = getSession().createSQLQuery(
				"select  distinct mut.name_surname  from ( Select  ft.FINDING_NUMBER,ft.id  from    DT_RT_FINDINGS_TABLE  ft\n"
						+ "	 \n"
						+ "	   inner join DT_ID_PROC_RESPONSIBLE_HEADS prh on  ft.BUSINESS_PROC = prh.PROC_ID\n"
						+ "	   inner join dt_id_users_table ut on  ut.id = prh.USER_ID  where prh.USER_ID= " + userId
						+ " ) \n" + "	 \n" + "	  fintable \n"
						+ "	 	 inner join DT_RT_FINDING_ACTIONS_TABLE fa  on fa.FINDING_ID = fintable.id\n"
						+ "		 inner join  dt_id_users_table mut on mut.id = fa.ACTION_OWNER_ID ;")
				.list();

		return data;

	}
	
	@Override
	public FindingResponsibleAction getFindingResponsibleOfLastChange(Integer findingId) {
		FindingResponsibleAction data = (FindingResponsibleAction) getSession().createSQLQuery(
				"SELECT TOP 1 * FROM DT_RT_FINDING_RESP_ACTIONS_TABLE where FINDING_ID = "+findingId+" ORDER BY CHANGE_DATE DESC").addEntity(FindingResponsibleAction.class).list().get(0);
		return data;
	}
	
	@Override
	public FindingAction getFindingActionOfLastChange(Integer findingId) {
		FindingAction data = (FindingAction) getSession().createSQLQuery(
				"SELECT TOP 1 * FROM DT_RT_FINDING_ACTIONS_TABLE where FINDING_ID = "+findingId+" ORDER BY CHANGE_DATE DESC").addEntity(FindingAction.class).list().get(0);
		return data;
	}

	@Override
	public Long retrieveAllFindingsCount() {
		final Criteria criteria = getSession().createCriteria(Finding.class);
		criteria.setProjection(Projections.rowCount());
		final Long result = (Long) criteria.uniqueResult();
		return result;
	}
	
	@Override
	public Long retrieveFindingsStatusNullCount() {
		final Criteria criteria = getSession().createCriteria(Finding.class);
		criteria.setProjection(Projections.rowCount());
		criteria.add(Restrictions.isNull("status"));
		final Long result = (Long) criteria.uniqueResult();
		return result;
	}
	
	@Override
	public List<Finding> retrieveFindingsStatusNull() {
		final Criteria criteria = getSession().createCriteria(Finding.class);
		criteria.add(Restrictions.isNull("status"));
	
		return criteria.list();
		
	}

	@Override
	public List<Finding> retrieveUserResponsibleDepartmentsFindingActions(int first, int pageSize, String sortField,
			SortOrder sortOrder, User user) {

		Criteria criteria = getSession().createCriteria(Finding.class);

		criteria.createAlias("businessProcessType", "businessProcessType");

		if (CollectionUtils.isNotEmpty(user.getResponsibleBusinessTypes())
				&& CollectionUtils.isNotEmpty(user.getResponsibleBusinessSubTypes())) {
			Criterion businessProcessTypeCriterion = Restrictions.in("businessProcessType",
					user.getResponsibleBusinessTypes());
			Criterion businessSubProcessTypeCriterion = Restrictions.in("businessSubProcessType",
					user.getResponsibleBusinessSubTypes());
			criteria.add(Restrictions.or(businessProcessTypeCriterion, businessSubProcessTypeCriterion));
		} else if (CollectionUtils.isNotEmpty(user.getResponsibleBusinessTypes())) {
			criteria.add(Restrictions.in("businessProcessType", user.getResponsibleBusinessTypes()));
		} else if (CollectionUtils.isNotEmpty(user.getResponsibleBusinessSubTypes())) {
			criteria.add(Restrictions.in("businessSubProcessType", user.getResponsibleBusinessSubTypes()));
		} else {
			return Lists.newLinkedList();
		}

		if (pageSize >= 0) {
			criteria.setMaxResults(pageSize);
		}
		if (first >= 0) {
			criteria.setFirstResult(first);
		}
		if (sortField == null) {
			criteria.addOrder(Order.asc("completionDate"));
		} else if (sortOrder.equals(SortOrder.ASCENDING)) {
			criteria.addOrder(Order.asc(sortField));
		} else if (sortOrder.equals(SortOrder.DESCENDING)) {
			criteria.addOrder(Order.desc(sortField));
		}
		return criteria.list();
	}

	@Override
	public Long retrieveUserResponsibleDepartmentsFindingActionsCount(User user) {
		Criteria criteria = getSession().createCriteria(Finding.class);

		if (CollectionUtils.isNotEmpty(user.getResponsibleBusinessTypes())
				&& CollectionUtils.isNotEmpty(user.getResponsibleBusinessSubTypes())) {
			Criterion businessProcessTypeCriterion = Restrictions.in("businessProcessType",
					user.getResponsibleBusinessTypes());
			Criterion businessSubProcessTypeCriterion = Restrictions.in("businessSubProcessType",
					user.getResponsibleBusinessSubTypes());
			criteria.add(Restrictions.or(businessProcessTypeCriterion, businessSubProcessTypeCriterion));
		} else if (CollectionUtils.isNotEmpty(user.getResponsibleBusinessTypes())) {
			criteria.add(Restrictions.in("businessProcessType", user.getResponsibleBusinessTypes()));
		} else if (CollectionUtils.isNotEmpty(user.getResponsibleBusinessSubTypes())) {
			criteria.add(Restrictions.in("businessSubProcessType", user.getResponsibleBusinessSubTypes()));
		} else {
			return (long) 0;
		}

		criteria.setProjection(Projections.rowCount());
		return (Long) criteria.uniqueResult();
	}

	@Override
	public List<Finding> retrieveUserResponsibleDepartmentsFindingActionsByParam(Map<String, Object> paramterMaps,
			int first, int pageSize, String sortField, SortOrder sortOrder, User user) {

		Criteria criteria = getSession().createCriteria(Finding.class);

		criteria.createAlias("businessProcessType", "businessProcessType");

		if (CollectionUtils.isNotEmpty(user.getResponsibleBusinessTypes())
				&& CollectionUtils.isNotEmpty(user.getResponsibleBusinessSubTypes())) {
			Criterion businessProcessTypeCriterion = Restrictions.in("businessProcessType",
					user.getResponsibleBusinessTypes());
			Criterion businessSubProcessTypeCriterion = Restrictions.in("businessSubProcessType",
					user.getResponsibleBusinessSubTypes());
			criteria.add(Restrictions.or(businessProcessTypeCriterion, businessSubProcessTypeCriterion));
		} else if (CollectionUtils.isNotEmpty(user.getResponsibleBusinessTypes())) {
			criteria.add(Restrictions.in("businessProcessType", user.getResponsibleBusinessTypes()));
		} else if (CollectionUtils.isNotEmpty(user.getResponsibleBusinessSubTypes())) {
			criteria.add(Restrictions.in("businessSubProcessType", user.getResponsibleBusinessSubTypes()));
		} else {
			return Lists.newLinkedList();
		}

		for (String key : paramterMaps.keySet()) {
			Object value = paramterMaps.get(key);

			if (key.equals("AUDIT_DATE") || key.equals("COMPLETION_DATE") || key.equals("REVISED_COMP_DATE")) {
				criteria.add(Restrictions.sqlRestriction(" convert(varchar(10)," + key + ", 126 ) like '%" + value + "%'"));
			} else if (key.equals("FINDING_YEAR")) {
				criteria.add(Restrictions.sqlRestriction(" convert(varchar(4)," + key + ", 126 ) like '%" + value + "%'"));
			}  else if (key.equals("priority")|| key.equals("status")) {
				criteria.add(Restrictions.eq(key, value));
			} else  if (key.equals("compareDates")) {
				criteria.add(Restrictions.ne("compPercentage", "100"));
				criteria.add(Restrictions.like(key, "%" + value + "%"));
			} 
			else {
				criteria.add(Restrictions.like(key, "%" + value + "%"));
			}

		}

		if (pageSize >= 0) {
			criteria.setMaxResults(pageSize);
		}
		if (first >= 0) {
			criteria.setFirstResult(first);
		}
		if (sortField == null) {
			criteria.addOrder(Order.asc("completionDate"));
		} else if (sortOrder.equals(SortOrder.ASCENDING)) {
			criteria.addOrder(Order.asc(sortField));
		} else if (sortOrder.equals(SortOrder.DESCENDING)) {
			criteria.addOrder(Order.desc(sortField));
		}
		return criteria.list();
	}

	@Override
	public Long retrieveUserResponsibleDepartmentsFindingActionsCountByParam(Map<String, Object> paramterMaps,
			User user) {
		Criteria criteria = getSession().createCriteria(Finding.class);

		if (CollectionUtils.isNotEmpty(user.getResponsibleBusinessTypes())
				&& CollectionUtils.isNotEmpty(user.getResponsibleBusinessSubTypes())) {
			Criterion businessProcessTypeCriterion = Restrictions.in("businessProcessType",
					user.getResponsibleBusinessTypes());
			Criterion businessSubProcessTypeCriterion = Restrictions.in("businessSubProcessType",
					user.getResponsibleBusinessSubTypes());
			criteria.add(Restrictions.or(businessProcessTypeCriterion, businessSubProcessTypeCriterion));
		} else if (CollectionUtils.isNotEmpty(user.getResponsibleBusinessTypes())) {
			criteria.add(Restrictions.in("businessProcessType", user.getResponsibleBusinessTypes()));
		} else if (CollectionUtils.isNotEmpty(user.getResponsibleBusinessSubTypes())) {
			criteria.add(Restrictions.in("businessSubProcessType", user.getResponsibleBusinessSubTypes()));
		} else {
			return (long) 0;
		}

		for (String key : paramterMaps.keySet()) {
			Object value = paramterMaps.get(key);

			if (key.equals("AUDIT_DATE") || key.equals("COMPLETION_DATE") || key.equals("REVISED_COMP_DATE")) {
				criteria.add(
						Restrictions.sqlRestriction(" convert(varchar(10)," + key + ", 126 ) like '%" + value + "%'"));
			} else if (key.equals("FINDING_YEAR")) {
				criteria.add(
						Restrictions.sqlRestriction(" convert(varchar(4)," + key + ", 126 ) like '%" + value + "%'"));
			} else if (key.equals("priority")  || key.equals("status")) {
				criteria.add(Restrictions.eq(key, value));
			} else  if (key.equals("compareDates")) {
				criteria.add(Restrictions.ne("compPercentage", "100"));
				criteria.add(Restrictions.like(key, "%" + value + "%"));
			} 
			else {
				criteria.add(Restrictions.like(key, "%" + value + "%"));
			}
		}

		criteria.setProjection(Projections.rowCount());
		return (Long) criteria.uniqueResult();
	}

	@Override
	public Long retrieveUserFindingActionsCount(User user) {
		Criteria criteria = getSession().createCriteria(FindingAction.class);
		criteria.add(Restrictions.eq("actionOwner", user));
		criteria.setProjection(Projections.rowCount());
		return (Long) criteria.uniqueResult();
	}

	@Override
	public List<FindingAction> retrieveUserFindingActions(int first, int pageSize, String sortField,
			SortOrder sortOrder, User user) {

		try {
			final Criteria criteria = getSession().createCriteria(FindingAction.class);
			criteria.add(Restrictions.eq("actionOwner", user));
			criteria.createAlias("finding", "finding");

			if (pageSize >= 0) {
				criteria.setMaxResults(pageSize);
			}
			if (first >= 0) {
				criteria.setFirstResult(first);
			}
			if (sortField == null) {
				criteria.addOrder(Order.asc("completionPercentage"));
			} else if (sortField.equals("custom")) {
				criteria.addOrder(Order.asc("finding.completed"));
				criteria.addOrder(Order.asc("finding.confirmed"));
				criteria.addOrder(Order.asc("finding.completionDate"));
				criteria.addOrder(Order.asc("completionPercentage"));
			} else if (sortOrder.equals(SortOrder.ASCENDING)) {
				criteria.addOrder(Order.asc(sortField));
			} else if (sortOrder.equals(SortOrder.DESCENDING)) {
				criteria.addOrder(Order.desc(sortField));
			}

			return criteria.list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}

	@Override
	public Long retrieveUserFindingActionsCountByParam(Map<String, Object> paramterMaps, User user) {

		final Criteria criteria = getSession().createCriteria(FindingAction.class, "findingAction");
		criteria.createAlias("finding", "finding");
		criteria.add(Restrictions.eq("actionOwner", user));

		for (String key : paramterMaps.keySet()) {
			Object value = paramterMaps.get(key);

			if (key.equals("REVISED_COMP_DATE")) {
				criteria.add(Restrictions.sqlRestriction(" convert(varchar(10)," + key + ", 126 ) like '%" + value + "%'"));
			} else if (key.equals("FINDING_YEAR")) {
				criteria.add(Restrictions.sqlRestriction(" convert(varchar(4)," + key + ", 126 ) like '%" + value + "%'"));
			} else if (key.equals("finding.priority") || key.equals("finding.status") ) {
				criteria.add(Restrictions.eq(key, value));
			} else {
				criteria.add(Restrictions.like(key, "%" + value + "%"));
			}

		}

		criteria.setProjection(Projections.rowCount());
		return (Long) criteria.uniqueResult();
	}

	@Override
	public List<FindingAction> retrieveUserFindingActionsByParam(Map<String, Object> paramterMaps, int first,
			int pageSize, String sortField, SortOrder sortOrder, User user) {

		try {
			final Criteria criteria = getSession().createCriteria(FindingAction.class, "findingAction");
			criteria.createAlias("finding", "finding");
			criteria.add(Restrictions.eq("actionOwner", user));

			if (pageSize >= 0) {
				criteria.setMaxResults(pageSize);
			}
			if (first >= 0) {
				criteria.setFirstResult(first);
			}
			if (sortField == null) {
				criteria.addOrder(Order.asc("completionPercentage"));
			} else if (sortField.equals("custom")) {
				criteria.addOrder(Order.asc("finding.completed"));
				criteria.addOrder(Order.asc("finding.confirmed"));
				criteria.addOrder(Order.asc("finding.completionDate"));
				criteria.addOrder(Order.asc("completionPercentage"));
			} else if (sortOrder.equals(SortOrder.ASCENDING)) {
				criteria.addOrder(Order.asc(sortField));
			} else if (sortOrder.equals(SortOrder.DESCENDING)) {
				criteria.addOrder(Order.desc(sortField));
			}

			for (String key : paramterMaps.keySet()) {
				Object value = paramterMaps.get(key);

				if (key.equals("REVISED_COMP_DATE")) {
					criteria.add(Restrictions.sqlRestriction(" convert(varchar(10)," + key + ", 126 ) like '%" + value + "%'"));
				} else if (key.equals("FINDING_YEAR")) {
					criteria.add(Restrictions.sqlRestriction(" convert(varchar(4)," + key + ", 126 ) like '%" + value + "%'"));
				} else if (key.equals("finding.priority")  || key.equals("finding.status")) {
					criteria.add(Restrictions.eq(key, value));
				} else {
					criteria.add(Restrictions.like(key, "%" + value + "%"));
				}

			}

			return criteria.list();
		} catch (Exception e) {
			logger.debug("retrieveUserFindingActionsByParam Exception"+e.getMessage());
		}
		return null;

	}

	@Override
	public void deleteFinding(Finding finding) {
		getSession().beginTransaction();
		getSession().delete(finding);
		getSession().flush();
		getSession().getTransaction().commit();
	}

	@Override
	public void deleteAuditType(AuditType auditType) {
		getSession().beginTransaction();
		getSession().delete(auditType);
		getSession().flush();
		getSession().getTransaction().commit();
	}

	@Override
	public void deleteDepartment(Department department) {
		getSession().beginTransaction();
		getSession().delete(department);
		getSession().flush();
		getSession().getTransaction().commit();
	}

	@Override
	public void deleteBusinessProcessType(BusinessProcessType businessProcessType) {
		getSession().beginTransaction();
		getSession().delete(businessProcessType);
		getSession().flush();
		getSession().getTransaction().commit();

	}

	@Override
	public void deleteBusinessSubProcessType(BusinessSubProcessType businessSubProcessType) {
		getSession().beginTransaction();
		getSession().delete(businessSubProcessType);
		getSession().flush();
		getSession().getTransaction().commit();
	}

	@Override
	public List<Department> retrieveAllDepartments() {
		return getSession().createCriteria(Department.class).addOrder(Order.asc("description")).list();
	}

	@Override
	public void saveFindingAction(FindingAction selectedFindingAction) {
		getSession().beginTransaction();
		getSession().saveOrUpdate(selectedFindingAction);
		getSession().flush();
		getSession().getTransaction().commit();
	}

	@Override
	public void saveFinding(Finding selectedFinding) {
		getSession().beginTransaction();
		getSession().saveOrUpdate(selectedFinding);
		getSession().flush();
		getSession().getTransaction().commit();
	}

	@Override
	public void deleteEmailContent(EmailContent emailContent) {
		getSession().beginTransaction();
		getSession().delete(emailContent);
		getSession().flush();
		getSession().getTransaction().commit();
	}

	@Override
	public void saveOrUpdateEmailContent(EmailContent emailContent) {
		getSession().beginTransaction();
		getSession().saveOrUpdate(emailContent);
		getSession().flush();
		getSession().getTransaction().commit();
	}

	public void removeFindingFile(List<FindingFile> findingFiles, Finding finding) {
		if (CollectionUtils.isNotEmpty(findingFiles)) {
			getSession().getNamedQuery("finding.removeFindingFile").setParameter("finding", finding)
					.setParameterList("findingFiles", findingFiles).executeUpdate();
		}
	}
	
	@Override
	public List<Finding> findNotComplatedFindings(){
		return getSession().createCriteria(Finding.class).add(Restrictions.ne("status", "CONFIRMED_COMPLETED")).add(Restrictions.ne("status", "UNCONFIRMED_COMPLETED")).list();
	}

}
