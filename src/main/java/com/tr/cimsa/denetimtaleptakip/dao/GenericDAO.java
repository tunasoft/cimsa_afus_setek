package com.tr.cimsa.denetimtaleptakip.dao;

import org.hibernate.Session;

import com.tr.cimsa.denetimtaleptakip.model.User;

import java.io.Serializable;
import java.util.List;

/**
 * bütün daolar için ortak olan işlemler
 * 
 * @author mhazer
 * */
public interface GenericDAO
{

    public <T, PK> PK save(T newInstance);

    public <T> void update(T transientObject);

    public <T> void saveOrUpdate(T transientObject);

    @SuppressWarnings("unchecked")
    public <T, PK extends Serializable> T findById(Class type, PK id);

    @SuppressWarnings("unchecked")
    public <T> List<T> findAll(Class clazz);

    @SuppressWarnings("unchecked")
    public <T> List<T> findAllByProperty(Class clazz, String propertyName, Object value);

    public <T> T findByProperty(Class clazz, String propertyName, Object value);

    public <T> void reattachUsingLock(T transientObject);

    public void flushSessionChanges();

    public <T> T merge(T transientObject);

    public Session getSession();

}
