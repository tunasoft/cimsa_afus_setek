package com.tr.cimsa.denetimtaleptakip.dao.impl;

import org.hibernate.LockOptions;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

import com.google.common.collect.Lists;
import com.tr.cimsa.denetimtaleptakip.model.BaseEntity;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

public abstract class GenericDAOImpl
{

    private SessionFactory sessionFactory;

    @Resource(name = "sessionFactory")
    public void setSessionFactory(SessionFactory sessionFactory)
    {
        this.sessionFactory = sessionFactory;
    }

    public Session getSession()
    {
        return this.sessionFactory.getCurrentSession();
    }

    protected SessionFactory getSessionFactory()
    {
        return this.sessionFactory;
    }

    @SuppressWarnings("unchecked")
    protected DetachedCriteria createDetachedCriteria(Class clazz)
    {
        return DetachedCriteria.forClass(clazz);
    }

    @SuppressWarnings("unchecked")
    public <T> List<T> findAll(Class clazz)
    {
        DetachedCriteria criteria = createDetachedCriteria(clazz);
        return criteria.getExecutableCriteria(getSession()).list();
    }

    @SuppressWarnings("unchecked")
    public <T> List<T> findAllByProperty(Class clazz, String propertyName, Object value)
    {
        DetachedCriteria criteria = createDetachedCriteria(clazz);
        criteria.add(Restrictions.eq(propertyName, value));
        return criteria.getExecutableCriteria(getSession()).list();
    }

    public <T> T findByProperty(Class clazz, String propertyName, Object value)
    {
        DetachedCriteria criteria = createDetachedCriteria(clazz);
        criteria.add(Restrictions.eq(propertyName, value));
        return (T) criteria.getExecutableCriteria(getSession()).uniqueResult();
    }

    @SuppressWarnings("unchecked")
    public <T, PK extends Serializable> T findById(Class type, PK id)
    {
        return (T) getSession().get(type, id);
    }

    @SuppressWarnings("unchecked")
    public <T, PK> PK save(T newInstance)
    {
        return (PK) getSession().save(newInstance);
    }

    public <T> void saveOrUpdate(T transientObject)
    {
        getSession().saveOrUpdate(transientObject);
    }

    public <T> void update(T transientObject)
    {
        getSession().update(transientObject);
    }

    public <T> void reattachUsingLock(T transientObject)
    {
        getSession().buildLockRequest(LockOptions.NONE).lock(transientObject);
    }

    public void flushSessionChanges()
    {
        getSession().flush();
    }

    public boolean isSessionContainsObject(Object detachedObject)
    {
        return getSession().contains(detachedObject);
    }

    @SuppressWarnings("unchecked")
    public <T> T merge(T transientObject)
    {
        transientObject = (T) getSession().merge(transientObject);
        return transientObject;
    }
    
    public <T extends BaseEntity> List<Integer> getIds(Collection<T> processList)
    {
        List<Integer> ids = Lists.newArrayList();
        for (T entity : processList)
        {
            ids.add(entity.getId());
        }
        return ids;
    }

}
