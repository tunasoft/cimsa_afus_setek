package com.tr.cimsa.denetimtaleptakip.dao;

import com.tr.cimsa.denetimtaleptakip.controller.admin.SearchParameters;
import com.tr.cimsa.denetimtaleptakip.model.*;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.primefaces.model.SortOrder;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * User: mhazer
 * Date: 8/19/12
 * Time: 5:07 PM
 */
public interface FindingDAO extends GenericDAO
{
    public List<Finding> retrieveAllFindings();

    public List<FindingAction> retrieveUserFindingActions(User user);

    public List<BusinessSubProcessType> retrieveBusinessSubProcesses();

    public List<BusinessProcessType> retrieveBusinessProcesses();

    public List<Finding> searchFindings(SearchParameters searchParameters);

    public FindingAction retrieveFindingActionWithHistory(Integer findingActionId);

    public List<FindingAction> retrieveHistoryOfFindingAction(Integer findingActionId);

    public Long findNotCompletedFindingActionCount(Finding finding);

    public List<Company> retrieveAllCompanies();
    
    public List<Department> retrieveAllDepartments();
    
    public void saveFindingAction(FindingAction selectedFindingAction);
    
    public void saveFinding(Finding selectedFinding);
    
    public List<AuditType> retrieveAllAuditTypes();

    public Company retrieveCompany(String description);
    
    public Department retrieveDepartment(String description);
    
    public AuditType retrieveAuditType(String description);

    public List<Finding> retrieveUserResponsibleBusinessTypeFindings(Collection<BusinessProcessType> responsibleBusinessTypes);

    public List<Finding> retrieveUserResponsibleBusinessSubTypeFindings(Collection<BusinessSubProcessType> responsibleBusinessSubTypes);

    List<Company> retrieveAllActiveCompanies();
    
    List<AuditType> retrieveAllActiveAuditTypes();

    public void deleteFindingActionsOfUsers(Finding finding, List<User> selectedOldActionOwners);

    List<FindingResponsibleAction> retrieveHistoryOfFindingResponsibleAction(Integer findingResponsibleActionId);

    public BusinessProcessType retrieveBusinessProcessType(String description);

    public Finding retrieveFinding(String findingNumber);

    List<Finding> retrieveAllFindings(int first, int pageSize, String sortField, SortOrder sortOrder);
    
    public List<Finding> retrieveFindingsByParam(Map<String,Object> paramterMaps ,int first, int pageSize, String sortField, SortOrder sortOrder);
    
    public Long retrieveFindingsCountByParam(Map<String, Object> paramterMaps);
    
    public List<String> getActionOwnersOfFindings();
    
    public List<String> getResponsibleOwnersOfFindings();
    
    public List<String> getActionOwnersOfMyDepartmentFindingsForSubBusinesProc(String userId);
    
    public List<String> getActionOwnersOfMyDepartmentFindingsForBusinessProc(String userId);
    
    Long retrieveAllFindingsCount();

    List<Finding> retrieveUserResponsibleDepartmentsFindingActions(int first, int pageSize, String sortField, SortOrder sortOrder, User user);

    Long retrieveUserResponsibleDepartmentsFindingActionsCount(User user);
    
    public Long retrieveUserResponsibleDepartmentsFindingActionsCountByParam(Map<String, Object> paramterMaps,User user);
    
    public List<Finding> retrieveUserResponsibleDepartmentsFindingActionsByParam(Map<String, Object> paramterMaps,int first, int pageSize, String sortField, SortOrder sortOrder, User user);

    Long retrieveUserFindingActionsCount(User user);
    
    public Long retrieveUserFindingActionsCountByParam(Map<String, Object> paramterMaps,User user);

    List<FindingAction> retrieveUserFindingActions(int first, int pageSize, String sortField, SortOrder sortOrder, User user);
    
    public List<FindingAction> retrieveUserFindingActionsByParam(Map<String, Object> paramterMaps ,int first, int pageSize, String sortField, SortOrder sortOrder, User user);
    
    public void deleteFinding(Finding finding);
    
    public void deleteAuditType(AuditType auditType);
    
    public void deleteBusinessProcessType(BusinessProcessType businessProcessType);
    
    public void deleteBusinessSubProcessType(BusinessSubProcessType businessSubProcessType);
    
    public void deleteEmailContent(EmailContent emailContent);
    
    public void saveOrUpdateEmailContent(EmailContent emailContent);

	Finding retrieveFinding2(int id);

	List<Department> retrieveAllActiveDepartments();

	public void deleteDepartment(Department department);

	public void removeFindingFile(List<FindingFile> findingFiles, Finding finding);
	
	public Long retrieveFindingsStatusNullCount();
	
	public List<Finding> retrieveFindingsStatusNull();
	
	public FindingResponsibleAction getFindingResponsibleOfLastChange(Integer findingId);
	
	public FindingAction getFindingActionOfLastChange(Integer findingId);
	
	public List<Finding> findNotComplatedFindings();

}
