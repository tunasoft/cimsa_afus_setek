package com.tr.cimsa.denetimtaleptakip.dao;

import java.util.List;

import com.tr.cimsa.denetimtaleptakip.model.User;

/**
 * User: mhazer
 * Date: 8/19/12
 * Time: 5:07 PM
 */
public interface UserDAO extends GenericDAO
{
    public List<User> retrieveAllUsers();

    public List<User> retrieveActiveUsers();
    
	public List<User> findByUsername(String username);
}
