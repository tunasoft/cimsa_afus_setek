package com.tr.cimsa.denetimtaleptakip.dao.impl;

import com.google.common.collect.Lists;
import com.tr.cimsa.denetimtaleptakip.dao.UserDAO;
import com.tr.cimsa.denetimtaleptakip.model.User;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Locale;

/**
 * User: mhazer
 * Date: 8/19/12
 * Time: 5:07 PM
 */
@Repository("userDAO")
public class UserDAOImpl extends GenericDAOImpl implements UserDAO
{
    @Override
    public List<User> retrieveAllUsers()
    {
        List<User> users = 
                getSession()
                        .createCriteria(User.class)
                        .addOrder(Order.asc("username"))
                        .list();
        return Lists.newLinkedList(users);
    }

    @Override
    public List<User> retrieveActiveUsers()
    {
        List<User> users =
                getSession()
                        .createCriteria(User.class)
                        .add(Restrictions.eq("active", Boolean.TRUE))
                        .addOrder(Order.asc("username"))
                        .list();
        return Lists.newLinkedList(users);
    }
    
    @Override
    public List<User> findByUsername(String username)
    {
    	Criteria cri = getSession().createCriteria(User.class);
    	cri.add(Restrictions.or(Restrictions.eq("username", username.toLowerCase(Locale.ENGLISH)), Restrictions.eq("username", username.toUpperCase(Locale.ENGLISH))));
        List<User> users = cri.list();
        
        return Lists.newLinkedList(users);
    }
}
