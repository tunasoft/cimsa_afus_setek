package com.tr.cimsa.denetimtaleptakip.model;

import java.util.Map;

import com.google.common.collect.Maps;

/**
 * User: mhazer
 * Date: 8/20/12
 * Time: 8:12 AM
 */
public enum Priority
{
    VERY_HIGH("Very High", "#ff0000"),
    HIGH("High", "#ffa500"),
    MODERATE("Moderate", "#ffff00"),
    LOW("Low", "#9acd32");
    
    private static final Map<String, Priority> _map = Maps.newHashMap();

    static
    {
        for(Priority priority : Priority.values())
        {
            _map.put(priority.getDescription(), priority);
        }
    }

    private final String description;
    private final String color;

    private Priority(String description, String color)
    {
        this.description = description;
        this.color = color;
    }

    public String getDescription()
    {
        return description;
    }

    public String getColor()
    {
        return color;
    }
    
    public static Priority findPriorityFromDescription(String description)
    {
        return _map.get(description);
    }
}
