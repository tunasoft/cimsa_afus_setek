package com.tr.cimsa.denetimtaleptakip.model;

import org.apache.commons.lang.StringUtils;

public class AfusEnum
{
    public enum AuditType
    {
        INTERNAL_AUDIT("Surec_Denetimi"),
        EXTERNAL_AUDIT("Holding_Ortak_Denetim");
        
        private final String description;

        private AuditType(String description)
        {
            this.description = description;
        }

        public String getDescription()
        {
            return description;
        }
    }
    
    
    
    public enum Answer
    {
        NO("HAYIR"),
        YES("EVET");
        
        private final String turkishLabel;
        
        private Answer(String turkishLabel)
        {
            this.turkishLabel = turkishLabel;
        }
        
        public String getTurkishLabel()
        {
            return turkishLabel;
        }
    }

    public enum ChartSeriesStatus
    {
        IN_PROGRESS("IN PROGRESS"),
        OVERDUE("OVERDUE"),
        COMPLETED("COMPLETED");

        private final String description;

        private ChartSeriesStatus(String description)
        {
            this.description = description;
        }

        public String getDescription()
        {
            return description;
        }
    }

}
