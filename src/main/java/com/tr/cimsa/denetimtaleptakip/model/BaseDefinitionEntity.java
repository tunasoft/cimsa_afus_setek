package com.tr.cimsa.denetimtaleptakip.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Type;

import java.io.Serializable;
import java.util.Date;

/**
 * User: mhazer
 * Date: 8/19/12
 * Time: 6:24 PM
 */
@Data
@MappedSuperclass
@EqualsAndHashCode(callSuper = true, exclude = {"adminUser"})
public class BaseDefinitionEntity extends BaseEntity implements Serializable
{
    protected String description;
    @Column(name = "ACTIVE", columnDefinition = "BIT", length = 1)
    protected boolean active = true;
    @ManyToOne
    @JoinColumn(name = "ADMIN_USER_ID")
    protected User adminUser;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "EDIT_DATE")
    protected Date editDate;
}
