package com.tr.cimsa.denetimtaleptakip.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;
import org.primefaces.model.UploadedFile;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * User: mhazer
 * Date: 5/17/13
 * Time: 06:07 AM
 */
@Entity
@Table(name = "DT_RT_FINDING_FILE_TABLE")
@Data
@ToString(includeFieldNames = true, callSuper = true, exclude = {"finding", "user", "content"})
@EqualsAndHashCode(callSuper = false, of = {"fileName"})
@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
@AuditTable("DT_RT_FINDING_FILE_TABLE_AUD")
public class FindingFile extends BaseEntity implements Serializable
{
    @ManyToOne(optional = false)
    @JoinColumn(name = "FINDING_ID")
    private Finding finding;

    @ManyToOne(optional = false)
    @JoinColumn(name = "USER_ID")
    private User user;

    @Column(name = "FILE_NAME", nullable = false)
    private String fileName;

    @Column(name = "CONTENT_TYPE", nullable = false)
    private String contentType;

    @Column(name = "CHANGE_DATE", nullable = false)
    private Date changeDate;

    @NotAudited
    @Lob
    @Column(name = "CONTENT", nullable = false)
    private byte[] content;

    public static FindingFile createInstance(Finding finding, User user, UploadedFile file)
    {
        FindingFile findingFile = new FindingFile();
        findingFile.setFinding(finding);
        findingFile.setUser(user);
        findingFile.setFileName(file.getFileName());
        findingFile.setContent(file.getContents());
        findingFile.setContentType(file.getContentType());
        findingFile.setChangeDate(new Date());
        return findingFile;
    }
}
