package com.tr.cimsa.denetimtaleptakip.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * User: mhazer
 * Date: 8/19/12
 * Time: 9:07 PM
 */
@Entity
@Table(name = "DT_ID_EMAIL_CONTENTS")
@ToString(includeFieldNames = true, callSuper = true)
@EqualsAndHashCode(callSuper = true, exclude = "mailContent")
public class EmailContent extends BaseDefinitionEntity implements Serializable
{
    @Column(name = "MAIL_CONTENT")
    private String mailContent;

	public String getMailContent() {
		return mailContent;
	}

	public void setMailContent(String mailContent) {
		this.mailContent = mailContent;
	}
    
    
}
