package com.tr.cimsa.denetimtaleptakip.model;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * User: mhazer
 * Date: 8/19/12
 * Time: 4:58 PM
 */
@Entity
@Table(name = "DT_ID_BUSINESS_SUB_PROC_TABLE")
@Data
@ToString(includeFieldNames = true, callSuper = true , exclude = {"responsibleHeads", "functionHeads"})
@EqualsAndHashCode(exclude={"mainProcess", "responsibleHeads", "functionHeads"}, callSuper = true)
public class BusinessSubProcessType extends BaseDefinitionEntity implements Serializable
{
    @Column(name = "SUB_PROC_ID")
    private Integer subProcessId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MAIN_PROC_ID", referencedColumnName = "id")
    private BusinessProcessType mainProcess;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name="DT_ID_SUBPROC_RESP_HEADS",
            joinColumns =
                    @JoinColumn(name = "SUB_PROC_ID" , referencedColumnName = "id"),
            inverseJoinColumns =
                    @JoinColumn(name = "USER_ID", referencedColumnName = "id")
    )
    private Set<User> responsibleHeads = Sets.newHashSet();

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name="DT_ID_SUBPROC_FUNCTION_HEADS",
            joinColumns =
                    @JoinColumn(name = "SUB_PROC_ID" , referencedColumnName = "id"),
            inverseJoinColumns =
                    @JoinColumn(name = "USER_ID", referencedColumnName = "id")
    )
    private Set<User> functionHeads = Sets.newHashSet();    
    
    
    @Transient
    public List<User> getResponsibleHeadsAsList()
    {
        return Lists.newLinkedList(getResponsibleHeads());
    }
}
