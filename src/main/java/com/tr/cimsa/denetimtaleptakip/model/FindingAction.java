package com.tr.cimsa.denetimtaleptakip.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;
import org.joda.time.LocalDate;

import com.google.common.collect.Lists;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * User: mhazer
 * Date: 8/21/12
 * Time: 11:32 AM
 */
@Entity
@Table(name = "DT_RT_FINDING_ACTIONS_TABLE")
@Data
@ToString(includeFieldNames = true, callSuper = true, exclude = {"finding", "actionOwner"})
@EqualsAndHashCode(callSuper = true, of = {"finding", "actionOwner", "historyOfFindingAction"})
@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
@AuditTable("DT_RT_FINDING_ACTIONS_TBL_AUD")
public class FindingAction extends BaseEntity implements Serializable
{
    @ManyToOne
    @JoinColumn(name = "FINDING_ID")
    private Finding finding;

    @ManyToOne
    @JoinColumn(name = "ACTION_OWNER_ID")
    private User actionOwner;
    
    @Column(name = "ACTION_TAKEN")
    private String actionTaken;

    @Column(name = "COMPLETION_PERCENTAGE")
    private int completionPercentage = 0;
    

    @Column(name = "EXPLANATION_FOR_DELAY")
    private String explanationForDelay;

    @Column(name = "REVISED_COMPLETION_DATE")
    private Date revisedCompletionDate;

    @Column(name = "CHANGE_DATE")
    private Date changeDate;
    
    @Column(name = "ACTION_OWNER_FLAG", columnDefinition = "BIT", length = 1)
    private boolean actionOwnerFlag;

    @Column(name = "READ_ONLY", columnDefinition = "BIT", length = 1)
    private boolean readOnly;
    
    @Column(name = "IS_SPOC", columnDefinition = "BIT default 0", length = 1, nullable = false)
    private boolean spoc;
    
    @Transient
    private List<FindingAction> historyOfFindingAction = Lists.newLinkedList();

    public void setHistoryOfFindingAction(List<FindingAction> findingActionHistory)
    {
        if (CollectionUtils.isNotEmpty(findingActionHistory))
        {
            this.historyOfFindingAction = findingActionHistory;
        }
    }

    @Transient
    private LocalDate defineDayToFindDelayedStatus()
    {
        LocalDate dayToDefineDelayStatus = new LocalDate();
        if(getRevisedCompletionDate() != null)
        {
            dayToDefineDelayStatus = new LocalDate(getRevisedCompletionDate());
        }
        return dayToDefineDelayStatus;
    }
}
