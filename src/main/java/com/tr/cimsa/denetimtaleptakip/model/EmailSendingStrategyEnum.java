package com.tr.cimsa.denetimtaleptakip.model;

import lombok.Getter;

/**
 * User: mhazer
 * Date: 7/7/13
 * Time: 5:14 PM
 */
public enum EmailSendingStrategyEnum
{
    THREE_MONTH_STRATEGY(new Integer("1")),
    ONE_DAY_BEFORE_STRATEGY(new Integer("2")),
    ENERJISA_STRATEGY(new Integer("3"));

    @Getter
    private Integer code;

    private EmailSendingStrategyEnum(Integer code)
    {
        this.code = code;
    }
}
