package com.tr.cimsa.denetimtaleptakip.model;

import com.google.common.collect.Maps;

import java.util.Map;

/**
 * User: mhazer
 * Date: 8/25/12
 * Time: 9:58 PM
 */
public enum Status
{
    UNCONFIRMED_COMPLETED("Unconfirmed-Completed", "orange"),
    CONFIRMED_COMPLETED("Confirmed-Completed", "green"),
    DELAYED("Overdue", "red"),
    IN_PROGRESS("In Progress", "blue"),
    NOT_STARTED("Not Started", "red");

    private static final Map<String, Status> _map = Maps.newHashMap();
    
    static
    {
        for(Status status : Status.values())
        {
            _map.put(status.getDescription(), status);
        }
    }
    
    private final String description;

    private final String color;

    private Status(String description, String color)
    {
        this.description = description;
        this.color = color;

    }

    public String getDescription()
    {
        return description;
    }

    public String getColor()
    {
        return color;
    }
    
    public static Status findStatusFromDescription(String description)
    {
        return _map.get(description);
    }
}
