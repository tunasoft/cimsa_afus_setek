package com.tr.cimsa.denetimtaleptakip.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;

@Data
@MappedSuperclass
@EqualsAndHashCode(exclude = {"id", "version"}, callSuper = false)
public class BaseEntity implements Serializable
{
    @Id
    @Column(name="id")
    @GeneratedValue(strategy= GenerationType.AUTO)
    protected Integer id;
    @Version
    protected Integer version;
}