package com.tr.cimsa.denetimtaleptakip.model;

/**
 * 
 * @author mhazer
 * 
 * */
public class StatusUserType extends EnumUserType<Status> {

	public StatusUserType() {
		super(Status.class);
	}
}
