package com.tr.cimsa.denetimtaleptakip.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * User: zekai.ozalp Date: 10/30/2014 Time: 11:40 AM
 */
@Entity
@Table(name = "DT_ID_COMPANIES_TABLE")
@Data
@ToString(includeFieldNames = true, callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class Company extends BaseDefinitionEntity implements Serializable {

}
