package com.tr.cimsa.denetimtaleptakip.model;

/**
 * User: mhazer
 * Date: 8/26/12
 * Time: 9:40 PM
 */
public enum EmailReason
{
    COMPLETED("Completed"),
    /**
     * AFUS-12
     * Bulguyu edit ettiğimizde en altta "Do you want to send warning e-mails?" sorusu çıkıyor. Bunun yanındaki check box'taki tiki kaldırdığımızda sebep soruyor. Buradaki menüde "Completed" ve "Not Implemented" seçenekleri var. Biz burada "Completed", "Restructured" ve "Rejected" seçeneklerinin olmasını istiyoruz. Daha önce "Not implemented" şeklinde işaretlenmiş olanların da "Rejected"a çevrilmesini istiyoruz.
     *
     * Zekai Oğuz Özalp, 10 Ekim 2014
     */
    //NOT_IMPLEMENTED("Not implemented");
    RESTRUCTURED("Restructured"),
    REJECTED("Rejected");

    
    private final String description;

    private EmailReason(String description)
    {
        this.description = description;
    }

    public String getDescription()
    {
        return description;
    }
}
