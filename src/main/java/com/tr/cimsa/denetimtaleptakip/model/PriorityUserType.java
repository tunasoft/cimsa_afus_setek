package com.tr.cimsa.denetimtaleptakip.model;

/**
 * 
 * @author mhazer
 * 
 * */
public class PriorityUserType extends EnumUserType<Priority> {

	public PriorityUserType() {
		super(Priority.class);
	}
}
