package com.tr.cimsa.denetimtaleptakip.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Entity
@Table(name = "DT_ID_DEPARTMENT_TABLE")
@Data
@ToString(includeFieldNames = true, callSuper = true, exclude = { "departmentUpdateHeaders" })
@EqualsAndHashCode(callSuper = true, exclude = { "departmentUpdateHeaders", "departmentEmailContent"})
public class Department extends BaseDefinitionEntity implements Serializable {

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "DT_ID_DEPARTMENT_HEAD_TABLE", joinColumns = @JoinColumn(name = "DEPARTMENT_ID", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "USER_ID", referencedColumnName = "id"))
	@NotFound(action = NotFoundAction.IGNORE)
	private Set<User> departmentUpdateHeaders = new LinkedHashSet<User>();

	@Column(name = "EMAIL_SUBJECT", nullable = false)
	private String emailSubject;

	
	  @ManyToOne
	  @JoinColumn(name = "DEPARTMENT_EMAIL_CONTENT")
	  @NotFound(action = NotFoundAction.IGNORE)
	private EmailContent departmentEmailContent;
	
}
