package com.tr.cimsa.denetimtaleptakip.model;

import com.google.common.collect.Lists;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * User: mhazer
 * Date: 8/21/12
 * Time: 11:32 AM
 */
@Entity
@Table(name = "DT_RT_FINDING_RESP_ACTIONS_TABLE")
@Data
@ToString(includeFieldNames = true, callSuper = true, exclude = {"finding", "actionOwner"})
@EqualsAndHashCode(callSuper = true, of = {"finding", "actionOwner", "historyOfFindingResponsibleAction"})
@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
@AuditTable("DT_RT_FINDING_RESP_ACTIONS_TBL_AUD")
public class FindingResponsibleAction extends BaseEntity implements Serializable
{
    @ManyToOne
    @JoinColumn(name = "FINDING_ID")
    private Finding finding;

    @ManyToOne(optional = false)
    @JoinColumn(name = "ACTION_OWNER_ID")
    private User actionOwner;

    @Column(name = "ACTION_TAKEN")
    private String actionTaken;

    @Column(name = "COMPLETION_PERCENTAGE")
    private int completionPercentage = 0;

    @Column(name = "EXPLANATION_FOR_DELAY")
    private String explanationForDelay;

    @Column(name = "REVISED_COMPLETION_DATE")
    private Date revisedCompletionDate;

    @Column(name = "CHANGE_DATE")
    private Date changeDate;

    @Transient
    private List<FindingResponsibleAction> historyOfFindingResponsibleAction = Lists.newLinkedList();

    public void setHistoryOfFindingResponsibleAction(List<FindingResponsibleAction> findingResponsbileActionHistory)
    {
        if (CollectionUtils.isNotEmpty(findingResponsbileActionHistory))
        {
            this.historyOfFindingResponsibleAction = findingResponsbileActionHistory;
        }
    }

	public Date getRevisedCompletionDate() {
		return revisedCompletionDate;
	}

}
