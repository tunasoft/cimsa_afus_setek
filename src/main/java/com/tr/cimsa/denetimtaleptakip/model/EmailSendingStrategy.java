package com.tr.cimsa.denetimtaleptakip.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * User: mhazer
 * Date: 8/19/12
 * Time: 9:07 PM
 */
@Entity
@Table(name = "DT_ID_EMAIL_SENDING_STRATEGY")
@Data
@ToString(includeFieldNames = true, callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class EmailSendingStrategy extends BaseDefinitionEntity implements Serializable
{
}
