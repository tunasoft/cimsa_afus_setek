package com.tr.cimsa.denetimtaleptakip.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * User: mhazer
 * Date: 8/19/12
 * Time: 9:07 PM
 */
@Entity
@Table(name = "DT_ID_ROLES_TABLE")
@Data
@ToString(includeFieldNames = true, callSuper = true)
@EqualsAndHashCode(callSuper = false, of = {"name"})
public class Role extends BaseEntity implements Serializable
{
    private String name;
}
