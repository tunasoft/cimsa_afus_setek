package com.tr.cimsa.denetimtaleptakip.model;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * User: mhazer
 * Date: 8/19/12
 * Time: 4:58 PM
 */
@Entity
@Table(name = "DT_ID_BUSINESS_PROC_TABLE")
@Data
@ToString(includeFieldNames = true, callSuper = true, exclude = {"subProcessTypes", "responsibleHeads", "functionHeads"})
@EqualsAndHashCode(exclude = {"subProcessTypes", "responsibleHeads", "functionHeads"}, callSuper = true)
public class BusinessProcessType extends BaseDefinitionEntity implements Serializable
{
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "mainProcess", cascade = CascadeType.ALL)
    @OrderBy("description")
    private Set<BusinessSubProcessType> subProcessTypes = Sets.newHashSet();

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "DT_ID_PROC_RESPONSIBLE_HEADS",
            joinColumns =
            @JoinColumn(name = "PROC_ID", referencedColumnName = "id"),
            inverseJoinColumns =
            @JoinColumn(name = "USER_ID", referencedColumnName = "id")
    )
    private Set<User> responsibleHeads = Sets.newHashSet();
    
    
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "DT_ID_PROC_FUNCTION_HEADS",
            joinColumns =
            @JoinColumn(name = "PROC_ID", referencedColumnName = "id"),
            inverseJoinColumns =
            @JoinColumn(name = "USER_ID", referencedColumnName = "id")
    )
    private Set<User> functionHeads = Sets.newHashSet();


    @Transient
    public List<BusinessSubProcessType> subprocessTypesAsList()
    {
        return Lists.newLinkedList(getSubProcessTypes());
    }
    
    @Transient
    public List<User> getResponsibleHeadsAsList()
    {
        return Lists.newLinkedList(getResponsibleHeads());
    }

}
