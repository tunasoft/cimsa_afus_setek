package com.tr.cimsa.denetimtaleptakip.model;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * User: dpinar
 * Date: 3/8/17
 * Time: 3:27 PM
 */

@Entity
@Table(name = "dt_id_users_table")
@Data
@ToString(includeFieldNames = true, callSuper = true, exclude = {"findings", "adminUser", "responsibleBusinessTypes", "responsibleBusinessSubTypes"})
@EqualsAndHashCode(callSuper = true, of = {"username"})
@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
public class User extends BaseEntity implements Serializable
{ 
    @Column(name = "username")
    private String username;
    @Column(name = "email")
    private String email;
    @Column(name = "name_surname")
    private String nameSurname;
    @Column(name = "job_title")
    private String jobTitle;
    @Column(name = "ACTIVE", columnDefinition = "BIT", length = 1)
    private boolean active = true;
    @ManyToOne
    @JoinColumn(name = "ADMIN_USER_ID")
    private User adminUser;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "EDIT_DATE")
    private Date editDate;

    @OneToMany(mappedBy = "actionOwner")
    private Set<FindingAction> findings = Sets.newHashSet();

    @ManyToMany(mappedBy = "responsibleHeads", fetch = FetchType.LAZY)
    private Set<BusinessProcessType> responsibleBusinessTypes = Sets.newHashSet();

    @ManyToMany(mappedBy = "responsibleHeads", fetch = FetchType.LAZY)
    private Set<BusinessSubProcessType> responsibleBusinessSubTypes = Sets.newHashSet();

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(
            name = "DT_ID_USER_ROLES_TABLE",
            joinColumns =
            @JoinColumn(name = "USER_ID", referencedColumnName = "id"),
            inverseJoinColumns =
            @JoinColumn(name = "ROLE_ID", referencedColumnName = "id")
    )
    @Fetch(FetchMode.SUBSELECT)
    @NotAudited
    private Set<Role> roles = Sets.newHashSet();

    @Transient
    public static List<String> usersEmailAddresses(Collection<User> users)
    {
        List<String> emailAddresses = Lists.newArrayList();
        for (User user : users)
        {
        	if(user.isActive())	// kullanıcı aktif ise mail listesine ekle 
        		emailAddresses.add(user.getEmail());
        }
        return emailAddresses;
    }

    @Transient
    public static List<String> usersNamesAndSurnames(Collection<User> users)
    {
        List<String> namesAndSurname = Lists.newArrayList();
        for (User user : users)
        {
            namesAndSurname.add(user.getNameSurname());
        }
        return namesAndSurname;
    }

    @Transient
    public boolean isUserReponsibleOfAnyBusinessType()
    {
        return CollectionUtils.isNotEmpty(getResponsibleBusinessTypes()) || CollectionUtils.isNotEmpty(getResponsibleBusinessSubTypes());
    }

    @Transient
    public boolean hasRole(String roleName)
    {
        for (Role role : getRoles())
        {
            if (role.getName().equals(roleName))
            {
                return true;
            }
        }
        return false;
    }

    @Transient
    public List<Role> getRolesAsList()
    {
        return Lists.newLinkedList(getRoles());
    }

}
