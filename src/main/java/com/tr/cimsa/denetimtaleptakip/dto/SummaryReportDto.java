package com.tr.cimsa.denetimtaleptakip.dto;

import lombok.Data;

import java.util.Date;

import com.tr.cimsa.denetimtaleptakip.model.Priority;

/**
 * Created by gkisakol on 4/4/2016.
 */
@Data
public class SummaryReportDto
{
    private String findingNumber;
    private String description;
    private Priority priority;private Date completionDate;

}
