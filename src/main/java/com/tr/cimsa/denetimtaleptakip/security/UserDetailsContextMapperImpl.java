package com.tr.cimsa.denetimtaleptakip.security;

import com.google.common.collect.Lists;
import com.tr.cimsa.denetimtaleptakip.model.Role;
import com.tr.cimsa.denetimtaleptakip.model.User;
import com.tr.cimsa.denetimtaleptakip.services.UserService;

import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.ldap.userdetails.UserDetailsContextMapper;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * User: mhazer
 * Date: 1/5/13
 * Time: 2:54 PM
 */
public class UserDetailsContextMapperImpl implements UserDetailsContextMapper, Serializable
{
    private UserService userService;

    @Override
    public UserDetails mapUserFromContext(DirContextOperations ctx, String username, Collection<? extends GrantedAuthority> authorities)
    {
        User user = findUser(username);
        List<GrantedAuthority> mappedAuthorities = prepareAuthorities(user);
        return new org.springframework.security.core.userdetails.User(username, "", true, true, true, true, mappedAuthorities);
    }

    private User findUser(String username)
    {
        User user = this.userService.findUser(username);
        if (user == null)
        {
            throw new UsernameNotFoundException("User can not found at the Çimsa Audit Follow Up System database");
        }
        return user;
    }

    private List<GrantedAuthority> prepareAuthorities(User user)
    {
        List<GrantedAuthority> mappedAuthorities = Lists.newArrayList();
        for (Role role : user.getRoles())
        {
            GrantedAuthority authority = new SimpleGrantedAuthority(role.getName());
            mappedAuthorities.add(authority);
        }
        return mappedAuthorities;
    }

    @Override
    public void mapUserToContext(UserDetails user, DirContextAdapter ctx)
    {
    }

    public void setUserService(UserService userService)
    {
        this.userService = userService;
    }
}
